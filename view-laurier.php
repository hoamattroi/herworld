<?php

/** Sets up WordPress vars and included files. */
require_once('wp-load.php'); 
require "php-export-data.class.php";
//get initial data
global $wpdb;
//$sql = "SELECT ID FROM wp_posts WHERE post_type='product'";
//$postArr = $wpdb->get_results($sql);
$id = $_REQUEST['id'];
	$user 			= get_user_by('id', $id);
	$id 			= $user->ID;
	$email 			= $user->user_email;
	$nickname 		= get_user_meta( $id, 'nickname', true );
	$phone	 		= get_user_meta( $id, 'phone', true );
	$date_modify	= get_user_meta( $id, 'date_modify', true );
	$cmnd 			= get_user_meta( $id, 'cmnd', true );
	$job 			= get_user_meta( $id, 'job', true );
	$description 	= get_user_meta( $id, 'description', true );
	$fbURL 			= $user->data->user_url; 
	$file_tpl 		= get_user_meta( $id, 'user_img_tpl', true );
	$file_ori 		= get_user_meta( $id, 'user_img_ori', true );
    $file_tpl 		= str_replace("//herworldvietnam.vn", "//backend.herworldvietnam.vn" , $file_tpl);
    $file_ori 		= str_replace("//herworldvietnam.vn", "//backend.herworldvietnam.vn" , $file_ori);
?>

<!DOCTYPE html>
<html lang="en">
<head>
  <meta charset="utf-8">
  <script src="https://ajax.googleapis.com/ajax/libs/jquery/1.12.4/jquery.min.js"></script>
  <!-- My Style -->
  <link rel="stylesheet" href="<?php echo site_url(); ?>/wp-content/themes/herworld/library/asset/css/bootstrap.css" type="text/css">
  <link rel="stylesheet" href="<?php echo site_url(); ?>/wp-content/themes/herworld/library/asset/css/sync-style.css" type="text/css">
</head>
<body class="single single-cgqn">
<div class="laurier-wrap laurier-share">
  <!-- Begin Cotnent -->
  <div id="laurier-content">
    <section class="sections">
      <div class="laurier-container">

        <!-- maincol -->
        <div class="maincol">
        	<div class="box01 over-header">
		        <div class="box01-heading">
		            <h1 class="title-general small">ĐI TÌM GƯƠNG MẶT</h1>
		            <img src="<?php echo URL_IMAGE; ?>/co-gai-quyen-nang-small.png" class="text-title" alt="Cô gái quyền năng">
		        </div>
		        <div class="box01-body">
		            <div class="share-template finish">
                    	<p><?php echo $id; ?></p>
                    	<p><?php echo $date_modify; ?></p>
	                    <div class="choose">
	                        <?php if(!empty($file_tpl)) { ?>      
	                            <img src="<?php echo $file_tpl ?>" class="img-responsive" alt="<?php echo $nickname; ?>"/>  
	                        <?php } else { ?>
	                            <img src="<?php echo URL_IMAGE; ?>/temp-1-big.jpg" class="img-responsive" alt="<?php echo $nickname; ?>"/>
	                        <?php } ?>
	                    </div>
	                    <div class="instock">
	                        <div class="text-center profile">
	                        	<p><?php echo $nickname; ?></p>
	                        	<p><?php echo $email; ?></p>
	                        	<p><?php echo $job; ?></p>
	                        	<p><?php echo $phone; ?></p>
	                        	<p><?php echo $cmnd; ?></p>
	                            <div class="description"><?php echo $description; ?></div>
	                            <a href="<?php echo $fbURL; ?>" style="text-decoration: underline;" target="_blank" >Facebook<br></a>
	                            <a href="<?php echo $file_ori; ?>" style="text-decoration: underline;" target="_blank" >Avatar Origin</a>
	                        </div>
	                    </div>

		            </div>
		        </div>
		    </div><!-- /.box01 -->
			
			<?php if ( current_user_can('edit_posts') ) { ?>
		    <div style="height: 30px;"></div>

			<form id="form-update-avatar" action="" method="post">
				<input type="text" name="user_id" id="form-update-user-id" value="<?php echo $id;?>" hidden>
				<div class="box01 over-header">
				  <div class="box01-body">
					<div class="share-template">
					  <div class="choose">
				          <div class="show-template show01">
				            <img src="<?php echo URL_IMAGE; ?>/front/logo.png" alt="" class="img-responsive img-logo">
				            <div class="sentence-temp">
				              <h2><?php echo $description; ?></h2>
				              <p class="author"><?php echo $nickname; ?> <br class="sp">- <?php echo $job; ?></p>
				            </div>
				            <figure class="imgUpload" id="imgUpload">
				              <img class="img-responsive" src="<?php echo $file_ori; ?>" alt="">
				            </figure>
				          </div><!-- /show-template -->
				      </div>
					  <div class="instock">
						<h2>Chọn mẫu trang trí</h2>
						<ul>
						  <li class="active"><a href="show01" href="javascript:;" data-src="<?php echo URL_IMAGE; ?>/template/template-01.png"><img src="<?php echo URL_IMAGE; ?>/temp-1.jpg" class="img-reponsive" alt="" /></a></li>
						  <li><a  href="show02" href="javascript:;" data-src="<?php echo URL_IMAGE; ?>/template/template-02.png"><img src="<?php echo URL_IMAGE; ?>/temp-2.jpg" alt="" class="img-reponsive" /></a></li>
						  <li><a  href="show03" href="javascript:;" data-src="<?php echo URL_IMAGE; ?>/template/template-03.png"><img src="<?php echo URL_IMAGE; ?>/temp-3.jpg" alt="" class="img-reponsive" /></a></li>
						  <li><a  href="show04" href="javascript:;" data-src="<?php echo URL_IMAGE; ?>/template/template-04.png"><img src="<?php echo URL_IMAGE; ?>/temp-4.jpg" alt="" class="img-reponsive" /></a></li>
						</ul>
						<div class="row">
						<div class="col-md-12 text-center">
							<a class="btn btn-default" onclick="moveTopImage()">Top</a>
						</div>
						<div class="col-md-6 text-center">
							<a class="btn btn-default" onclick="moveLeftImage()">Left</a>
						</div>
						<div class="col-md-6 text-center">
							<a class="btn btn-default" onclick="moveRightImage()">Right</a>
						</div>
						<div class="col-md-12 text-center">
							<a class="btn btn-default" onclick="moveBottomImage()">Bottom</a>
						</div>
						<div class="col-md-6 text-center">
							<a class="btn btn-default" onclick="zoomInImage()">Zoom in</a>
						</div>
						<div class="col-md-6 text-center">
							<a class="btn btn-default" onclick="zoomOutImage()">Zoom out</a>
						</div>

						</div>
					  </div>
					</div>
					<div class="button-footer">
					  <button id="save-info" class="gbtn gbtn-continue"></button>
					</div>
				  </div>              
				</div><!-- /.box01 -->
			</form>
			<?php }else{ ?>
				<a class="btn btn-default" href="<?php site_url(); ?>/wp-login.php">Login with administrator</a>
			<?php } ?>
        </div>
        <!-- /maincol -->
      </div>
    </section>
  </div>  
  <!-- End Content -->
  
  <!-- Script -->
  <script type="text/javascript" src="<?php echo site_url(); ?>/wp-content/themes/herworld/library/asset/js/plugins.js"></script>
  <script type="text/javascript" src="<?php echo site_url(); ?>/wp-content/themes/herworld/library/asset/js/jquery.validate.js"></script>
  <script type="text/javascript" src="<?php echo site_url(); ?>/wp-content/themes/herworld/library/asset/js/html2canvas.min.js"></script>
  <script type="text/javascript" src="<?php echo site_url(); ?>/wp-content/themes/herworld/library/asset/js/sync-custom.js"></script>

</div>
</body>
</html>

<?php
// echo "<table>";
// 	echo "<tr>";
// 		echo "<td style='width: 100px;'>ID</td>";
// 		echo "<td style='width: 100px;'>Fullname</td>";
// 		echo "<td style='width: 100px;'>Email</td>";
// 		echo "<td style='width: 80px;'>Phone</td>";
// 		echo "<td style='width: 80px;'>CMND</td>";
// 		echo "<td style='width: 200px;'>Job</td>";
// 		echo "<td style='width: 100px;'>Description</td>";
// 		echo "<td >Facebook URL</td>";
// 		echo "<td>Link Image</td>";
// 		echo "<td>Link Original</td>"; 
// 	echo "</tr>"; 
// 	echo "<tr>";
// 		echo "<td>" . $id . "</td>";
// 		echo  "<td>" . $nickname . "</td>";
// 		echo  "<td>" . $email . "</td>";
// 		echo  "<td>" . $phone . "</td>";
// 		echo  "<td>" . $cmnd . "</td>";
// 		echo  "<td>" . $job . "</td>";
// 		echo  "<td>" . $description . "</td>";
// 		echo  "<td>" . $fbURL . "</td>";
// 		echo  "<td>" . $file_tpl . "</td>";
// 		echo  "<td>" . $file_ori . "</td>"; 
// 	echo "</tr>"; 
// echo "</table>";