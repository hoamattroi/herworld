<?php

/** Sets up WordPress vars and included files. */
require_once('wp-load.php'); 
require "php-export-data.class.php";
//get initial data
global $wpdb;
//$sql = "SELECT ID FROM wp_posts WHERE post_type='product'";
//$postArr = $wpdb->get_results($sql);
$blogusers = get_users( array( 'role__in' => 'thamgia', 'orderby' =>'ID', 'order'=> 'DESC' ) );
 
$excel = new ExportDataExcel('browser');
$excel->filename = "data-" . date("Y-m-d H:i:s") . ".xls";
$excel->initialize();
//Tên, dt, CMND, TK FB, bài du thi (hình có khung), tổng like, share
$headline = array("ID","Fullname", "Email", "Phone", "CMND", "Job", "Description", "Facebook URL", "Link Image", "Link Original", "Total");
$excel->addRow($headline);
if(!empty($blogusers)) {
	// Array of WP_User objects.
	foreach ( $blogusers as $user ) { 
		$row = array();
		$id = $user->ID; 
		$nickname = get_user_meta( $id, 'nickname', true );
		$email = $user->user_email;
		$phone = get_user_meta( $id, 'phone', true );
		$cmnd = get_user_meta( $id, 'cmnd', true );
		$job = get_user_meta( $id, 'job', true );
		$description = get_user_meta( $id, 'description', true );
		$fbURL = $user->data->user_url; 
		$file_tpl = get_user_meta( $id, 'user_img_tpl', true );
		$file_ori = get_user_meta( $id, 'user_img_ori', true );
		$url = "http://herworldvietnam.vn/co-gai-quyen-nang/bai-du-thi-ca-nhan/". $id; 
		$row = array($id, $nickname, $email, $phone, $cmnd, $job, $description, $fbURL, $file_tpl, $file_ori, 0 );
		//print_r($row);
		$excel->addRow($row);
	}
}   
$excel->finalize();
