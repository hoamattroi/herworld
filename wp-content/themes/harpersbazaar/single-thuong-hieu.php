<?php get_header(); 

require_once('library/facebook/facebook.php');
require_once('library/facebook/fb_config.php');

global $halfpage;

?>

<div id="content">
	<div class="container">
		<div id="main" class="clearfix" role="main">

			<div class="article-pre">
				<?php if ( function_exists('yoast_breadcrumb') ) {
					yoast_breadcrumb('<p class="breadcrumbs">','</p>');
				} ?>
			</div>

			<div class="page-wrapper">
				<?php if (have_posts()) : while (have_posts()) : the_post(); ?>

				<div class="article-wrapper brand-wrapper">
				
					<article class="article-primary article-trend article-brand" id="post-<?php the_ID(); ?>" <?php post_class('clearfix'); ?> role="article" itemscope itemtype="http://schema.org/NewsArticle">

						<header class="article-header">
						<h1 class="category-title barred-heading">
							<span itemprop="headline"><?php the_title(); ?></span>
						</h1>
						</header> <!-- end article header -->

						<section class="entry-content clearfix" itemprop="articleBody">

							<?php 
								if(get_field('brand_information')) {
									get_template_part("parts/brand-tabbed");
								} 
							?>

						</section> <!-- end article section -->

					</article> <!-- end article -->
				</div>

				<?php endwhile; ?>

				<?php else : ?>

					<article id="post-not-found" class="hentry clearfix">
						<header class="article-header">
							<h2><?php _e("Không tìm thấy bài viết!", "harpersbazaar"); ?></h2>
						</header>
					</article>
				<?php endif; ?>

			</div> <!-- end .page-wrapper -->
			<section class="feed_brands">
				<section class="feed_wrapper">
					<section class="news_feed">
						<h2 class="category-title barred-heading">
							<span><?php the_title(); ?> <?php _e('trên Harper\'s Bazaar', 'harpersbazaar'); ?></span>
						</h2>
						<?php 
						
						
						$tag_id = get_term_by( 'name', get_the_title(), 'post_tag' );
						$paged = (get_query_var('paged')) ? get_query_var('paged') : 1;
						$args = array(
							'tag__in' => array( $tag_id->term_id ),
							'posts_per_page' => 6,
							'paged' => $paged
						);
						
						echo "<div class=\"inner_news_feed\">";		
						echo "<div class=\"internal_news_row\">";				
							$wp_query = new WP_Query( $args );
							$count_posts = 1;
							if ($wp_query->have_posts()) : while ($wp_query->have_posts()) : $wp_query->the_post(); ?>
							<article class="brand_internal_news" id="post-<?php the_ID(); ?>" <?php post_class('clearfix'); ?> role="article">
								<div class="image_wrapper"><a href="<?php the_permalink() ?>"><?php the_post_thumbnail('thumb-brand-people'); ?></a></div>
								<header class="article-header">
									<h2 class="h2"><a href="<?php the_permalink() ?>" rel="bookmark" title="<?php the_title_attribute(); ?>" itemprop="headline"><?php the_title(); ?></a></h2>
									<div class="article-meta">
										<p class="byline vcard">
											<?php printf(__('trong %1$s.', 'harpersbazaar'), get_the_category_list(', ')); ?>
										</p>
									</div>
								</header>
	
							</article> <!-- end article -->
							<?php if($count_posts%3==0){
								echo "</div><div class=\"internal_news_row\">";
							} 
							$count_posts++;
							?>
							<?php endwhile; ?>
						</div>
					</section>
						
						<?php else : ?>
							<div id="post-not-found" class="hentry clearfix">
									<p><?php _e("Chưa có bài viết trong chuyên mục này.", "harpersbazaar"); ?></p>
							</div>
	
						<?php endif; 
						wp_reset_query();
						?>
				</section>
				<aside class="sidebar">
				<?php echo $halfpage;  ?>
			</aside>
			</section>
			
			
				
				
				<?php
				
				$queried = $wp_query->queried_object;
				$terms = wp_get_post_terms(get_the_ID(), 'brand-category');
				
				$args = array(
					'post_type'  => 'thuong-hieu',
					'orderby'    => 'rand',
					'posts_per_page' => 8,
					'brand-category' => $terms[0]->slug
				);
				
				$query = new WP_Query( $args );
				
				if($query->have_posts()) :
				?> <section class="feed_brands">
					<section class="feed_wrapper similar">
						<section class="news_feed">
						<h2 class="category-title barred-heading">
							<span><?php _e('Có thể bạn thích', 'harpersbazaar'); ?></span>
						</h2>
						<div class="inner_news_feed">		
						<div class="internal_news_row similar_brand">
					<?php while($query->have_posts()) : $query->the_post() ?>
						<article class="brand_internal_news" id="post-<?php the_ID(); ?>" role="article">
								<div class="image_wrapper"><a href="<?php the_permalink() ?>"><?php the_post_thumbnail('thumb-brand-people'); ?></a></div>
								<header class="article-header">
									<h2 class="h2"><a href="<?php the_permalink() ?>" rel="bookmark" title="<?php the_title_attribute(); ?>" itemprop="headline"><?php the_title(); ?></a></h2>
								</header>
	
							</article> <!-- end article -->
				<?php	
					endwhile;?>
							
						</div>
						</div>
						</section>
					</section>
				</section>
				<?php endif;
				?>	
		</div> <!-- end #main -->
	</div> <!-- end .container -->
</div> <!-- end #content -->

<?php get_footer(); ?>
