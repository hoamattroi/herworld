<?php get_header(); ?>

<div id="content">
	<div class="container">
		<div id="main" class="clearfix" role="main">

			<div class="article-pre">
				<?php if ( function_exists('yoast_breadcrumb') ) {
					yoast_breadcrumb('<p class="breadcrumbs">','</p>');
				} ?>
			</div>

			<div class="page-wrapper">

				<div class="category-content-wrapper alpha-category-content-wrapper">
	
					<?php get_template_part("parts/trends-latest"); ?>

					<h1 class="category-title barred-heading">
						<span><?php _e("Đánh Sách Xử Hướng", "harpersbazaar"); ?></span>
					</h1>

					<?php 
					// Alphabet nav
					if (function_exists('wp_snap')) { echo wp_snap('firstload=all'); }
					?>

					<?php 
					$postCount = 0;
					$prevTitleInitial = ""; 
					if (have_posts()) : while (have_posts()) : the_post();
						$postCount++; 
						$title = get_the_title();
						// Get first character of title
						$currTitleInitial = substr($title, 0, 1);
						if ($currTitleInitial != $prevTitleInitial) {
							// Don't close .row if outputting first item
							if ($postCount != 1) { 
								echo "</div> <!-- .alpha-category-content -->";
								echo "</div> <!-- .row -->";
							}
							echo "<div class=\"row\">";
							echo "<span class=\"alpha-letter\">$currTitleInitial</span>";
							echo "<div class=\"alpha-category-articles\">";
						}
						?>

						<article id="post-<?php the_ID(); ?>" <?php post_class('clearfix'); ?> role="article">
							<header class="article-header">
								<?php the_post_thumbnail('square-360'); ?>
								<h2 class="h2"><a href="<?php the_permalink() ?>" rel="bookmark" title="<?php the_title_attribute(); ?>"><?php the_title(); ?></a></h2>
							</header> <!-- end article header -->
						</article> <!-- end article -->
						<?php
						$prevTitleInitial = $currTitleInitial;
						?>

					<?php endwhile; ?>


					<?php if (function_exists('bones_page_navi')) { ?>
						<?php bones_page_navi(); ?>
					<?php } else { ?>
						<nav class="wp-prev-next">
							<ul class="clearfix">
								<li class="prev-link"><?php next_posts_link(__('&laquo; Older Entries', "harpersbazaar")) ?></li>
								<li class="next-link"><?php previous_posts_link(__('Newer Entries &raquo;', "harpersbazaar")) ?></li>
							</ul>
						</nav>
					<?php } ?>

					<?php else : ?>

						<div id="post-not-found" class="hentry clearfix">
							<p><?php _e("No articles in this category.", "harpersbazaar"); ?></p>
						</div>

					<?php endif; ?>
					</div> <!-- .alpha-category-content -->
					</div> <!-- .row -->

				</div> <!-- .category-content-wrapper -->

				<?php get_sidebar(); ?>

			</div> <!-- end .page-wrapper -->

		</div> <!-- end #main -->
	</div> <!-- end .container -->

</div> <!-- end #content -->

<section class="tags moremore">
	<h2 class="more-heading"><span><?php _e("More, More, More", "harpersbazaar"); ?></span></h2>
	<ul class=more_tags><?php 
		$curr_cat = get_cat_id( single_cat_title("",false) );
		top_tags($curr_cat); 
		?></ul>
</section>

<?php get_footer(); ?>
