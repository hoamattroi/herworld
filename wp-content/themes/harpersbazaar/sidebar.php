<?php global $halfpage; ?>
<div id="sidebar1" class="sidebar clearfix" role="complementary">
	<?php echo $halfpage; ?>
	
	<?php if ( is_active_sidebar( 'sidebar1' ) ) : ?>

		<?php dynamic_sidebar( 'sidebar1' ); ?>

	<?php else : ?>

		<!-- This content shows up if there are no widgets defined in the backend. -->

		<div class="alert alert-help">
			<p><?php _e("Please activate some Widgets.", "harpersbazaar");  ?></p>
		</div>

	<?php endif; ?>

</div>