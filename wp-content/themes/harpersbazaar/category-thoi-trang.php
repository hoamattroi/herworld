<?php get_header(); ?>

<div id="content">
	<div class="container">
		<div id="main" class="clearfix" role="main">

			<div class="article-pre">
				<?php if ( function_exists('yoast_breadcrumb') ) {
					yoast_breadcrumb('<p class="breadcrumbs">','</p>');
				} ?>
			</div>

			<div class="page-wrapper">

				<div class="category-content-wrapper">
	
					<?php get_template_part("parts/trends-latest"); ?>

					<!-- h1 class="category-title barred-heading">
						<span --><!-- ?php _e("Xuân Hè 2014", "harpersbazaar"); ? --><!-- /span>
					</h1 -->
					<!-- ?php get_template_part("parts/shows-latest"); ? -->


					<h1 class="category-title barred-heading">
						<span><?php single_cat_title(); ?></span>
					</h1>

					<div class="row">
					<?php $postCount = 1; ?>
					<?php if (have_posts()) : while (have_posts()) : the_post(); ?>
						<?php $postCount++; ?>

						<article id="post-<?php the_ID(); ?>" <?php post_class('clearfix'); ?> role="article">
							<header class="article-header">
								<?php the_post_thumbnail('square-360'); ?>
								<h2 class="h2"><a href="<?php the_permalink() ?>" rel="bookmark" title="<?php the_title_attribute(); ?>"><?php the_title(); ?></a></h2>
								<p class="byline vcard">
									<?php printf(__('trong %1$s.', 'harpersbazaar'), get_the_category_list(', ')); ?>
								</p>
							</header> <!-- end article header -->
						</article> <!-- end article -->

						<?php if ($postCount == 10): ?>
							<section class="subscribe-actions clearfix">
								<div class="subscribe-email newsletter-form">
									<h3><?php _e("Cập nhật ngay", "harpersbazaar"); ?></h3>
									<p><?php _e("Đăng ký nhận email mỗi tuần và cập nhật xu hướng mới nhất", "harpersbazaar"); ?></p>
									<div class="row">
									<?php gravity_form("Newsletter", $display_title=false, $display_description=false, $display_inactive=false, $field_values=null, $ajax=true); ?>
									</div>
								</div>
								<div class="subscribe-mag">
									<div class="row">
										<div class="mag_wrapper"><img src="<?php if(the_field('magazine_cover', 'option')){ echo getImgServerUrl(get_field('magazine_cover', 'option')); } ?>" alt="<?php bloginfo( 'name' ) ?>" title="<?php bloginfo( 'name' ) ?>" /></div>
										<div class="subscribe-mag-content">
											<h3><?php _e("Tạp Chí", "harpersbazaar"); ?></h3>
											<p><?php _e("Cập nhật Xu Hướng Mới Nhất và Bí Quyết Mua Sắm trong mùa", "harpersbazaar"); ?></p>
											<a class="button" href="#subscribe"><?php _e("Đăng ký", "harpersbazaar"); ?></a>
										</div>
									</div>
								</div>
							</section>
						<?php endif; ?>

					<?php endwhile; ?>


					<?php if (function_exists('bones_page_navi')) { ?>
						<?php bones_page_navi(); ?>
					<?php } else { ?>
						<nav class="wp-prev-next">
							<ul class="clearfix">
								<li class="prev-link"><?php next_posts_link(__('&laquo; Older Entries', "harpersbazaar")) ?></li>
								<li class="next-link"><?php previous_posts_link(__('Newer Entries &raquo;', "harpersbazaar")) ?></li>
							</ul>
						</nav>
					<?php } ?>

					<?php else : ?>

						<article id="post-not-found" class="hentry clearfix">
							<header class="article-header">
								<h1><?php _e("Chưa có bài viết trong chuyên mục này", "harpersbazaar"); ?></h1>
							</header>
						</article>

					<?php endif; ?>
					</div> <!-- .row -->

					<?php get_template_part('parts/brand-featured'); ?>


				</div> <!-- .category-content-wrapper -->

				<?php get_sidebar(); ?>

			</div> <!-- end .page-wrapper -->

		</div> <!-- end #main -->
	</div> <!-- end .container -->

</div> <!-- end #content -->

<section class="tags moremore">
	<h2 class="more-heading"><span><?php _e("Xem thêm...", "harpersbazaar"); ?></span></h2>
	<ul class=more_tags><?php 
		$curr_cat = get_cat_id( single_cat_title("",false) );
		top_tags($curr_cat); 
		?></ul>
</section>

<?php get_footer(); ?>
