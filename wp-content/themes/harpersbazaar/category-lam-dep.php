<?php get_header(); ?>

<div id="content">
	<div class="container">
		<div id="main" class="clearfix" role="main">

			<div class="article-pre">
				<?php if ( function_exists('yoast_breadcrumb') ) {
					yoast_breadcrumb('<p class="breadcrumbs">','</p>');
				} ?>
			</div>

			<div class="page-wrapper">

				<div class="category-content-wrapper">

					<h1 class="category-title barred-heading">
						<span><?php _e("Bí Qúyet Làm Đẹp", "harpersbazaar"); ?></span>
					</h1>

					<div class="row">
					<?php $postCount = 1; ?>
					<?php if (have_posts()) : while (have_posts()) : the_post(); ?>
						<?php $postCount++; ?>

						<article id="post-<?php the_ID(); ?>" <?php post_class('clearfix'); ?> role="article">
							<header class="article-header">
								<?php the_post_thumbnail('square-360'); ?>
								<h2 class="h2"><a href="<?php the_permalink() ?>" rel="bookmark" title="<?php the_title_attribute(); ?>"><?php the_title(); ?></a></h2>
								<p class="byline vcard">
									<?php printf(__('trong %1$s.', 'harpersbazaar'), get_the_category_list(', ')); ?>
								</p>
							</header> <!-- end article header -->
						</article> <!-- end article -->

					<?php endwhile; ?>


					<?php if (function_exists('bones_page_navi')) { ?>
						<?php bones_page_navi(); ?>
					<?php } else { ?>
						<nav class="wp-prev-next">
							<ul class="clearfix">
								<li class="prev-link"><?php next_posts_link(__('&laquo; Older Entries', "harpersbazaar")) ?></li>
								<li class="next-link"><?php previous_posts_link(__('Newer Entries &raquo;', "harpersbazaar")) ?></li>
							</ul>
						</nav>
					<?php } ?>

					<?php else : ?>

						<div id="post-not-found" class="hentry clearfix">
							<p><?php _e("Chưa có bài viết trong chuyên mục này.", "harpersbazaar"); ?></p>
						</div>

					<?php endif; ?>
					</div> <!-- .row -->
	
					<h1 class="category-title barred-heading">
						<span><?php _e("Thường Hiệu Làm Đẹp", "harpersbazaar"); ?></span>
					</h1>

					<?php 
					$brandCatID = 20;
					get_template_part("parts/brand-featured");
					?>

					<h1 class="category-title barred-heading">
						<span><?php _e("Harper’s Bazaar Chọn", "harpersbazaar"); ?></span>
					</h1>

					<div class="row">
						<?php
						$beautyChoiceCatID = 19;
						$postArgs = array(
							'category' => $beautyChoiceCatID,
							'orderby' => 'post_date',
							'order' => 'DESC',
							'post_status' => 'publish',
							'post_type' => 'post',  
							'posts_per_page' => 3,
						);
						$posts = get_posts($postArgs);
						
						foreach( $posts as $post ) :
							setup_postdata($essayPosts);
						?>
							<article id="post-<?php the_ID(); ?>" <?php post_class('clearfix'); ?> role="article">
								<a href="<?php the_permalink() ?>" rel="bookmark" title="<?php the_title_attribute(); ?>">
									<?php the_post_thumbnail('square-360'); ?>
								</a>
								<h2 class="h2">
									<a href="<?php the_permalink() ?>" rel="bookmark" title="<?php the_title_attribute(); ?>"><?php the_title(); ?></a>
								</h2>
								<p class="byline vcard">
									<?php printf(__('trong %1$s.', 'harpersbazaar'), get_the_category_list(', ')); ?>
								</p>
							</article> <!-- end article -->

						<?php
						endforeach;
						wp_reset_postdata();
						?>
					</div>

				</div> <!-- .category-content-wrapper -->

				<?php get_sidebar(); ?>

			</div> <!-- end .page-wrapper -->

			

		</div> <!-- end #main -->
	</div> <!-- end .container -->
</div> <!-- end #content -->

<?php get_footer(); ?>
