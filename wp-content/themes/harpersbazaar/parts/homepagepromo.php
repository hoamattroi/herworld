<section class="homepage-promo">
	<div class="container clearfix">
		<div class="row">
			<div id="homepage-promo-carousel" class="carousel slide" data-ride="carousel">
				<span class="triangle"><p>NOW</p></span>

				<!-- Wrapper for slides -->
				<div class="carousel-inner">
					<?php
					$counter = 1;
					while(has_sub_field('promo_slide', 'option')):
						if ($counter == 1) {
							$activeClass = "active";
						} else {
							$activeClass = "";
						}

						$article = get_sub_field('slide_article');
						$articleID = $article[0]->ID;
						$articleURL = get_permalink($articleID);
						//print_r($article);

						$fieldImage = get_sub_field('slide_image');
						if ($fieldImage != "") {
							$image = $fieldImage['sizes']['square-720'];
							$image = "<img src=\"$image\" alt=\"\" />";
						} else {
							$image = get_the_post_thumbnail($articleID, 'square-720');
						}
						$image = getImgServerUrl( $image );
						$fieldHeading = get_sub_field('slide_heading');
						if ($fieldHeading != "") {
							$heading = $fieldHeading;
						} else {
							$heading = $article[0]->post_title;
						}

						$fieldCaption = get_sub_field('slide_caption');
						if ($fieldCaption != "") {
							$caption = $fieldCaption;
						} else {
							$caption = $fieldCaption;
						}

						
					?>
						<a href="<?php echo $articleURL; ?>" class="item <?php echo $activeClass; ?>">
							<?php echo $image; ?>
							<div class="carousel-caption">
								<h2><?php echo $heading; ?></h2>
								<p><?php echo $caption; ?></p>
							</div>
						</a>
					<?php
					$counter++;
					endwhile; 
					?>
				</div>

				<?php 
				$counter--;
				if ($counter > 1) :
				?>
					<!-- Indicators -->
					<ol class="carousel-indicators">
						<?php
						$activeClass = "active";

						for ($indicator = 0; $indicator < $counter; $indicator++):
						?>
							<li data-target="#homepage-promo-carousel" data-slide-to="<?php echo $indicator; ?>" class="<?php echo $activeClass; ?>"></li>
						<?php 
							$activeClass = "";
						endfor; 
						?>
					</ol>

					<!-- Controls -->
					<a class="left carousel-control" href="#homepage-promo-carousel" data-slide="prev">Previous</a>
					<a class="right carousel-control" href="#homepage-promo-carousel" data-slide="next">Next</a>
				<?php endif; ?>

			</div>

			<?php if(have_rows('home_mpu', 'option')) : ?>
			<div class="homepage-promo-ads">
				<?php while ( have_rows('home_mpu', 'option') ) : the_row(); ?>
				<div>
					<p><?php echo get_sub_field("home_mpu_ad_code", 'option'); ?></p>
				</div>
				<?php endwhile; ?>
			</div>
			<?php endif; ?>
		</div>
	</div>
</section>