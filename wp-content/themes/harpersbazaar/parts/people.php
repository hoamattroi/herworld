<div class="people-wrapper">
	<a class="people-all" href="#people">
		<div class="people-all-container">
			<h1><span><?php _e("The", "harpersbazaar"); ?></span> <?php _e("People", "harpersbazaar"); ?></h1>
			<p><?php _e("Featuring the people who affect the fashion scene", "harpersbazaar"); ?></p>
			<span class="people-view-all"><?php _e("View All", "harpersbazaar"); ?></span>
		</div>
	</a>
	<div id="people-carousel" class="carousel slide" data-ride="carousel" data-interval="false">
		<!-- Wrapper for slides -->
		<div class="row">
			<div class="carousel-inner">
				<div class="item active">
					<figure class="people-carousel-first">
						<img src="http://placehold.it/300x300" alt="...">
						<figcaption>Đường Thu Hương</figcaption>
					</figure>
					<figure class="people-carousel-last">
						<img src="http://placehold.it/300x300" alt="...">
						<figcaption>Tony Birch</figcaption>
					</figure>
				</div>
				<div class="item">
					<figure class="people-carousel-first">
						<img src="http://placehold.it/300x300" alt="...">
						<figcaption>Đường Thu Hương</figcaption>
					</figure>
					<figure class="people-carousel-last">
						<img src="http://placehold.it/300x300" alt="...">
						<figcaption>Tony Birch</figcaption>
					</figure>
				</div>
			</div>
		</div>
		<!-- Controls -->
		
	</div>
	<a class="left carousel-control" href="#people-carousel" data-slide="prev">
		<span class="glyphicon glyphicon-chevron-left"></span>
	</a>
	<a class="right carousel-control" href="#people-carousel" data-slide="next">
		<span class="glyphicon glyphicon-chevron-right"></span>
	</a>
</div>