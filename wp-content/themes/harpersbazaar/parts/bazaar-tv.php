<?php
$videos = get_posts( array(
	'tax_query' => array(
		array(
			'taxonomy' => 'post_format',
			'field'    => 'slug',
			'terms'    => array( 'post-format-video' ),
			'operator' => 'IN'
		)
	),
	'numberposts' => 6
) );

if (count($videos) > 0):
?>
<section class="bazaar-tv-intro">
	<div class="container">
		<h1><img src="<?php echo get_stylesheet_directory_uri(); ?>/library/images/bazaar-tv.png" alt="Harper's Bazaar TV" /></h1>
		<div id="bazaar-tv-carousel" class="carousel slide" data-ride="carousel" data-interval="false">
			<!-- Wrapper for slides -->
			<div class="carousel-inner">
				<div class="item active">
					<?php
					$counter = 1;

					foreach( (array) $videos as $post ) :
						setup_postdata( $post );
					?>
						<a href="<?php echo the_permalink();?>" />
							<figure>
								<?php the_post_thumbnail('thumb-310x170'); ?>
								<figcaption><?php the_title(); ?></figcaption>
							</figure>
						</a>
					<?php
						if ($counter == 3) :
					?>
							</div>
							<div class="item">
					<?php
						endif;
					$counter++;
					endforeach;
					?>
				</div>
			</div>

			<!-- Controls -->
			<a class="left carousel-control" href="#bazaar-tv-carousel" data-slide="prev">
			<span class="glyphicon glyphicon-chevron-left"></span>
			</a>
			<a class="right carousel-control" href="#bazaar-tv-carousel" data-slide="next">
			<span class="glyphicon glyphicon-chevron-right"></span>
			</a>
		</div>
	</div>
</section>
<?php endif; ?>