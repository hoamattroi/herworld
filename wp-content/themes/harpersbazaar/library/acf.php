<?php


if(function_exists("register_field_group"))
{
	register_field_group(array (
		'id' => 'acf_top-three',
		'title' => 'Top Three',
		'fields' => array (
			array (
				'key' => 'field_52cfac1c2e846',
				'label' => 'Top Three Articles',
				'name' => 'top_three_articles',
				'type' => 'repeater',
				'sub_fields' => array (
					array (
						'key' => 'field_52cfb660feb04',
						'label' => 'Article',
						'name' => 'article',
						'type' => 'relationship',
						'required' => 1,
						'column_width' => '',
						'return_format' => 'object',
						'post_type' => array (
							0 => 'post',
						),
						'taxonomy' => array (
							0 => 'all',
						),
						'filters' => array (
							0 => 'search',
						),
						'result_elements' => array (
							0 => 'featured_image',
							1 => 'post_title',
						),
						'max' => 1,
					),
					array (
						'key' => 'field_52cfac3d2e848',
						'label' => 'Thumbnail',
						'name' => 'thumbnail',
						'type' => 'image',
						'column_width' => 25,
						'save_format' => 'object',
						'preview_size' => 'thumbnail',
						'library' => 'all',
					),
					array (
						'key' => 'field_52cfb2682e849',
						'label' => 'Heading',
						'name' => 'heading',
						'type' => 'text',
						'column_width' => 25,
						'default_value' => '',
						'placeholder' => '',
						'prepend' => '',
						'append' => '',
						'formatting' => 'html',
						'maxlength' => 100,
					),
				),
				'row_min' => 3,
				'row_limit' => 3,
				'layout' => 'table',
				'button_label' => 'Add Row',
			),
		),
		'location' => array (
			array (
				array (
					'param' => 'options_page',
					'operator' => '==',
					'value' => 'acf-options-top-three',
					'order_no' => 0,
					'group_no' => 0,
				),
			),
		),
		'options' => array (
			'position' => 'normal',
			'layout' => 'no_box',
			'hide_on_screen' => array (
			),
		),
		'menu_order' => 0,
	));
}

if(function_exists("register_field_group"))
{
	register_field_group(array (
		'id' => 'acf_homepage-promo',
		'title' => 'Homepage Promo',
		'fields' => array (
			array (
				'key' => 'field_52b27c91ef72e',
				'label' => 'Promo Slide',
				'name' => 'promo_slide',
				'type' => 'repeater',
				'sub_fields' => array (
					array (
						'key' => 'field_52b2a373ef72f',
						'label' => 'Slide Article',
						'name' => 'slide_article',
						'type' => 'relationship',
						'required' => 1,
						'column_width' => '',
						'return_format' => 'object',
						'post_type' => array (
							0 => 'post',
						),
						'taxonomy' => array (
							0 => 'all',
						),
						'filters' => array (
							0 => 'search',
						),
						'result_elements' => array (
							0 => 'featured_image',
							1 => 'post_type',
							2 => 'post_title',
						),
						'max' => 1,
					),
					array (
						'key' => 'field_52cfdae95635d',
						'label' => 'Slide Image',
						'name' => 'slide_image',
						'type' => 'image',
						'column_width' => '',
						'save_format' => 'object',
						'preview_size' => 'thumbnail',
						'library' => 'all',
					),
					array (
						'key' => 'field_52b2a3a9ef730',
						'label' => 'Slide Heading',
						'name' => 'slide_heading',
						'type' => 'text',
						'column_width' => '',
						'default_value' => '',
						'placeholder' => '',
						'prepend' => '',
						'append' => '',
						'formatting' => 'html',
						'maxlength' => '',
					),
					array (
						'key' => 'field_52b2a3beef731',
						'label' => 'Slide Caption',
						'name' => 'slide_caption',
						'type' => 'textarea',
						'column_width' => '',
						'default_value' => '',
						'placeholder' => '',
						'maxlength' => '',
						'formatting' => 'br',
					),
				),
				'row_min' => '',
				'row_limit' => '',
				'layout' => 'row',
				'button_label' => 'Add Slide',
			),
		),
		'location' => array (
			array (
				array (
					'param' => 'options_page',
					'operator' => '==',
					'value' => 'acf-options-home-slider',
					'order_no' => 0,
					'group_no' => 0,
				),
			),
		),
		'options' => array (
			'position' => 'normal',
			'layout' => 'no_box',
			'hide_on_screen' => array (
			),
		),
		'menu_order' => 0,
	));
}

if(function_exists("register_field_group"))
{
	register_field_group(array (
		'id' => 'acf_home-featured',
		'title' => 'Home Featured',
		'fields' => array (
			array (
				'key' => 'field_52d8ad9c0b470',
				'label' => 'Featured Article',
				'name' => 'featured_article',
				'type' => 'relationship',
				'required' => 1,
				'return_format' => 'object',
				'post_type' => array (
					0 => 'post',
				),
				'taxonomy' => array (
					0 => 'all',
				),
				'filters' => array (
					0 => 'search',
				),
				'result_elements' => array (
					0 => 'featured_image',
					1 => 'post_type',
					2 => 'post_title',
				),
				'max' => 1,
			),
			array (
				'key' => 'field_52d8adeb0b471',
				'label' => 'Article Heading',
				'name' => 'featured_article_heading',
				'type' => 'text',
				'default_value' => '',
				'placeholder' => '',
				'prepend' => '',
				'append' => '',
				'formatting' => 'none',
				'maxlength' => 255,
			),
			array (
				'key' => 'field_52d8aff03003a',
				'label' => 'Article Image',
				'name' => 'featured_article_image',
				'type' => 'image',
				'save_format' => 'object',
				'preview_size' => 'thumbnail',
				'library' => 'all',
			),
		),
		'location' => array (
			array (
				array (
					'param' => 'options_page',
					'operator' => '==',
					'value' => 'acf-options-home-featured',
					'order_no' => 0,
					'group_no' => 0,
				),
			),
		),
		'options' => array (
			'position' => 'normal',
			'layout' => 'no_box',
			'hide_on_screen' => array (
			),
		),
		'menu_order' => 0,
	));
}

// if(function_exists("register_field_group"))
// {
// 	register_field_group(array (
// 		'id' => 'acf_post-slideshow',
// 		'title' => 'Post Slideshow',
// 		'fields' => array (
// 			array (
// 				'key' => 'field_52cbab78eea87',
// 				'label' => 'Slideshow',
// 				'name' => 'slideshow',
// 				'type' => 'repeater',
// 				'sub_fields' => array (
// 					array (
// 						'key' => 'field_52cbace7eea88',
// 						'label' => 'Slide Image',
// 						'name' => 'slide_image',
// 						'type' => 'image',
// 						'column_width' => 20,
// 						'save_format' => 'object',
// 						'preview_size' => 'thumbnail',
// 						'library' => 'all',
// 					),
// 					array (
// 						'key' => 'field_52cbaddbeea89',
// 						'label' => 'Slide Text',
// 						'name' => 'slide_text',
// 						'type' => 'wysiwyg',
// 						'column_width' => '',
// 						'default_value' => '',
// 						'toolbar' => 'basic',
// 						'media_upload' => 'no',
// 					),
// 					array (
// 						'key' => 'field_52cbae13eea8a',
// 						'label' => 'Slide Layout',
// 						'name' => 'slide_layout',
// 						'type' => 'radio',
// 						'column_width' => 15,
// 						'choices' => array (
// 							'portrait' => 'Portrait',
// 							'landscape' => 'Landscape',
// 						),
// 						'other_choice' => 0,
// 						'save_other_choice' => 0,
// 						'default_value' => 'portrait',
// 						'layout' => 'vertical',
// 					),
// 				),
// 				'row_min' => '',
// 				'row_limit' => '',
// 				'layout' => 'table',
// 				'button_label' => 'Add Slide',
// 			),
// 		),
// 		'location' => array (
// 			array (
// 				array (
// 					'param' => 'post_type',
// 					'operator' => '==',
// 					'value' => 'post',
// 					'order_no' => 0,
// 					'group_no' => 0,
// 				),
// 			),
// 		),
// 		'options' => array (
// 			'position' => 'normal',
// 			'layout' => 'no_box',
// 			'hide_on_screen' => array (
// 			),
// 		),
// 		'menu_order' => 0,
// 	));
// }

if(function_exists("register_field_group"))
{
	register_field_group(array (
		'id' => 'acf_tabbed-slideshow',
		'title' => 'Tabbed Slideshow',
		'fields' => array (
			array (
				'key' => 'field_52ee8f5e60ae9',
				'label' => 'Slideshow',
				'name' => 'tabbed-slideshow',
				'type' => 'flexible_content',
				'layouts' => array (
					array (
						'label' => 'Slideshow Tab',
						'name' => 'slideshow-tab',
						'display' => 'row',
						'min' => '',
						'max' => '',
						'sub_fields' => array (
							array (
								'key' => 'field_52ee92050ebe7',
								'label' => 'Tab Heading',
								'name' => 'tab_heading',
								'type' => 'text',
								'column_width' => '',
								'default_value' => '',
								'placeholder' => '',
								'prepend' => '',
								'append' => '',
								'formatting' => 'html',
								'maxlength' => '',
							),
							array (
								'key' => 'field_52ee8f9c60aea',
								'label' => 'Tab Slides',
								'name' => 'tab_slides',
								'type' => 'repeater',
								'column_width' => '',
								'sub_fields' => array (
									array (
										'key' => 'field_52ee902b60aec',
										'label' => 'Slide Image',
										'name' => 'slide_image',
										'type' => 'image',
										'column_width' => '',
										'save_format' => 'object',
										'preview_size' => 'thumbnail',
										'library' => 'all',
									),
									array (
										'key' => 'field_52ee904060aed',
										'label' => 'Slide Text',
										'name' => 'slide_text',
										'type' => 'wysiwyg',
										'column_width' => 33,
										'default_value' => '',
										'toolbar' => 'full',
										'media_upload' => 'no',
									),
									array (
										'key' => 'field_52ee906b60aee',
										'label' => 'Slide Layout',
										'name' => 'slide_layout',
										'type' => 'radio',
										'column_width' => '',
										'choices' => array (
											'portrait' => 'Portrait',
											'landscape' => 'Landscape',
										),
										'other_choice' => 0,
										'save_other_choice' => 0,
										'default_value' => 'portrait',
										'layout' => 'vertical',
									),
								),
								'row_min' => '',
								'row_limit' => '',
								'layout' => 'table',
								'button_label' => 'Add Slide',
							),
						),
					),
					array (
						'label' => 'Gallery Tab',
						'name' => 'gallery-tab',
						'display' => 'row',
						'min' => '',
						'max' => '',
						'sub_fields' => array (
							array (
								'key' => 'field_52ef23a61f124',
								'label' => 'Tab Heading',
								'name' => 'tab_heading',
								'type' => 'text',
								'column_width' => '',
								'default_value' => '',
								'placeholder' => '',
								'prepend' => '',
								'append' => '',
								'formatting' => 'html',
								'maxlength' => '',
							),
							array (
								'key' => 'field_52ef23cc1f126',
								'label' => 'Tab Text',
								'name' => 'tab_text',
								'type' => 'wysiwyg',
								'toolba' => 'full',
								'column_width' => 100,
								'default_value' => '',
								'placeholder' => '',
								'maxlength' => '',
								'formatting' => 'br',
								'media_upload' => 'no'
							),
							array (
								'key' => 'field_52ef23b11f125',
								'label' => 'Tab Gallery',
								'name' => 'tab_gallery',
								'type' => 'gallery',
								'column_width' => '',
								'preview_size' => 'thumbnail',
								'library' => 'all',
							),
						),
					),
				),
				'button_label' => 'Add Slideshow Tab',
				'min' => '',
				'max' => '',
			),
		),
		'location' => array (
			array (
				array (
					'param' => 'post_type',
					'operator' => '==',
					'value' => 'fashion-trends',
					'order_no' => 0,
					'group_no' => 0,
				),
			),
			array (
				array (
					'param' => 'post_type',
					'operator' => '==',
					'value' => 'post',
					'order_no' => 0,
					'group_no' => 1,
				),
			),
			array (
				array (
					'param' => 'post_type',
					'operator' => '==',
					'value' => 'runway',
					'order_no' => 0,
					'group_no' => 2,
				),
			),
		),
		'options' => array (
			'position' => 'normal',
			'layout' => 'no_box',
			'hide_on_screen' => array (
			),
		),
		'menu_order' => 0,
	));
}

if(function_exists("register_field_group"))
{
	register_field_group(array (
		'id' => 'acf_video-information',
		'title' => 'Video information',
		'fields' => array (
			array (
				'key' => 'field_5312eb1cfc923',
				'label' => 'Video Link',
				'name' => 'video_or_youtube_link',
				'type' => 'text',
				'instructions' => 'Enter the id of youtube or of the uploaded image.',
				'default_value' => '',
				'placeholder' => '',
				'prepend' => '',
				'append' => '',
				'formatting' => 'none',
				'maxlength' => '',
			),
			array (
				'key' => 'field_5312eb36fc924',
				'label' => 'youtube or local?',
				'name' => 'youtube_or_local?',
				'type' => 'radio',
				'choices' => array (
					'Youtube' => 'Youtube',
					'Local' => 'Local',
				),
				'other_choice' => 0,
				'save_other_choice' => 0,
				'default_value' => '',
				'layout' => 'vertical',
			),
		),
		'location' => array (
			array (
				array (
					'param' => 'post_type',
					'operator' => '==',
					'value' => 'post',
					'order_no' => 0,
					'group_no' => 0,
				),
			),
		),
		'options' => array (
			'position' => 'normal',
			'layout' => 'no_box',
			'hide_on_screen' => array (
			),
		),
		'menu_order' => 0,
	));
}

if(function_exists("register_field_group"))
{
	register_field_group(array (
		'id' => 'acf_brand-people-tab',
		'title' => 'Brand / People Tab',
		'fields' => array (
			array (
				'key' => 'field_531f0bcbe57be',
				'label' => 'Facebook Link',
				'name' => 'facebook_link',
				'type' => 'text',
				'default_value' => '',
				'placeholder' => '',
				'prepend' => '',
				'append' => '',
				'formatting' => 'none',
				'maxlength' => '',
			),
			array (
				'key' => 'field_531bf74790de4',
				'label' => 'Brand / People Information',
				'name' => 'brand_information',
				'type' => 'repeater',
				'instructions' => 'Create new pages for your information about your brand or people',
				'sub_fields' => array (
					array (
						'key' => 'field_531bf8c428b3a',
						'label' => 'Tab Title',
						'name' => 'tab_title',
						'type' => 'text',
						'column_width' => 25,
						'default_value' => '',
						'placeholder' => '',
						'prepend' => '',
						'append' => '',
						'formatting' => 'html',
						'maxlength' => '',
					),
					array (
						'key' => 'field_531bf7a190de6',
						'label' => 'Tab Gallery',
						'name' => 'tab_gallery',
						'type' => 'gallery',
						'column_width' => 50,
						'preview_size' => 'thumbnail',
						'library' => 'all',
					),
					array (
						'key' => 'field_531bfe7c07027',
						'label' => 'Tab Content',
						'name' => 'tab_content',
						'type' => 'wysiwyg',
						'column_width' => 25,
						'default_value' => '',
						'toolbar' => 'full',
						'media_upload' => 'yes',
					),
				),
				'row_min' => 1,
				'row_limit' => 5,
				'layout' => 'table',
				'button_label' => 'Add Tab',
			),
		),
		'location' => array (
			array (
				array (
					'param' => 'post_type',
					'operator' => '==',
					'value' => 'chan-dung',
					'order_no' => 0,
					'group_no' => 0,
				),
			),
			array (
				array (
					'param' => 'post_type',
					'operator' => '==',
					'value' => 'thuong-hieu',
					'order_no' => 0,
					'group_no' => 0,
				),
			),
		),
		'options' => array (
			'position' => 'normal',
			'layout' => 'no_box',
			'hide_on_screen' => array (
			),
		),
		'menu_order' => 0,
	));
}

if(function_exists("register_field_group"))
{
	register_field_group(array (
		'id' => 'acf_magazine-cover',
		'title' => 'Magazine Cover',
		'fields' => array (
			array (
				'key' => 'field_531d881ea690f',
				'label' => 'Magazine Cover',
				'name' => 'magazine_cover',
				'type' => 'image',
				'instructions' => 'Upload the image',
				'save_format' => 'url',
				'preview_size' => 'large',
				'library' => 'all',
			),
		),
		'location' => array (
			array (
				array (
					'param' => 'options_page',
					'operator' => '==',
					'value' => 'acf-options-magazine-cover',
					'order_no' => 0,
					'group_no' => 0,
				),
			),
		),
		'options' => array (
			'position' => 'normal',
			'layout' => 'no_box',
			'hide_on_screen' => array (
			),
		),
		'menu_order' => 0,
	));
}

if(function_exists("register_field_group"))
{
	register_field_group(array (
		'id' => 'acf_search-and-analytics-settings',
		'title' => 'Search and Analytics Settings',
		'fields' => array (
			array (
				'key' => 'field_531eab8967632',
				'label' => 'Google Tag Manager Code',
				'name' => 'google_tag_manager_code',
				'type' => 'textarea',
				'instructions' => 'just insert the full code',
				'default_value' => '',
				'placeholder' => '',
				'maxlength' => '',
				'formatting' => 'html',
			),
			array (
				'key' => 'field_531eabbe67633',
				'label' => 'Analytics Code',
				'name' => 'analytics_code',
				'type' => 'textarea',
				'default_value' => '',
				'placeholder' => '',
				'maxlength' => '',
				'formatting' => 'html',
			),
		),
		'location' => array (
			array (
				array (
					'param' => 'options_page',
					'operator' => '==',
					'value' => 'acf-options-search-and-analytics-settings',
					'order_no' => 0,
					'group_no' => 0,
				),
			),
		),
		'options' => array (
			'position' => 'normal',
			'layout' => 'no_box',
			'hide_on_screen' => array (
			),
		),
		'menu_order' => 0,
	));
}

/* ADCODE CODES */

if(function_exists("register_field_group"))
{
	register_field_group(array (
		'id' => 'acf_brand-ads',
		'title' => 'Brand Ads',
		'fields' => array (
			array (
				'key' => 'field_531fc4c2d9905',
				'label' => 'Brand Leaderboard',
				'name' => 'brand_leaderboard',
				'type' => 'repeater',
				'sub_fields' => array (
					array (
						'key' => 'field_531fc4d6d9906',
						'label' => 'Brand Leaderboard Code Header',
						'name' => 'brand_leaderboard_code_header',
						'type' => 'textarea',
						'column_width' => '',
						'default_value' => '',
						'placeholder' => '',
						'maxlength' => '',
						'formatting' => 'html',
					),
					array (
						'key' => 'field_531fc4ecd9907',
						'label' => 'Brand Leaderboard Ad Code',
						'name' => 'brand_leaderboard_ad_code',
						'type' => 'textarea',
						'column_width' => '',
						'default_value' => '',
						'placeholder' => '',
						'maxlength' => '',
						'formatting' => 'html',
					),
				),
				'row_min' => 1,
				'row_limit' => 1,
				'layout' => 'table',
				'button_label' => 'Add Row',
			),
			array (
				'key' => 'field_531fc55dcf64b',
				'label' => 'Brand',
				'name' => 'brand_halfpage',
				'type' => 'repeater',
				'sub_fields' => array (
					array (
						'key' => 'field_531fc55dcf64c',
						'label' => 'Brand Halfpage Code Header',
						'name' => 'brand_halfpage_code_header',
						'type' => 'textarea',
						'column_width' => '',
						'default_value' => '',
						'placeholder' => '',
						'maxlength' => '',
						'formatting' => 'html',
					),
					array (
						'key' => 'field_531fc55dcf64d',
						'label' => 'Brand Halfpage Ad Code',
						'name' => 'brand_halfpage_ad_code',
						'type' => 'textarea',
						'column_width' => '',
						'default_value' => '',
						'placeholder' => '',
						'maxlength' => '',
						'formatting' => 'html',
					),
				),
				'row_min' => 1,
				'row_limit' => 1,
				'layout' => 'table',
				'button_label' => 'Add Row',
			),
		),
		'location' => array (
			array (
				array (
					'param' => 'options_page',
					'operator' => '==',
					'value' => 'acf-options-advertising',
					'order_no' => 0,
					'group_no' => 0,
				),
			),
		),
		'options' => array (
			'position' => 'normal',
			'layout' => 'no_box',
			'hide_on_screen' => array (
			),
		),
		'menu_order' => 0,
	));
	register_field_group(array (
		'id' => 'acf_category-ad-codes',
		'title' => 'Category Ad Codes',
		'fields' => array (
			array (
				'key' => 'field_531fc2a1916c5',
				'label' => 'Category Sidebar halfpage',
				'name' => 'cat_sidebar_halfpage',
				'type' => 'repeater',
				'sub_fields' => array (
					array (
						'key' => 'field_531fc2b8916c6',
						'label' => 'Category Halfpage Code Header',
						'name' => 'cat_halfpage_code_header',
						'type' => 'textarea',
						'column_width' => '',
						'default_value' => '',
						'placeholder' => '',
						'maxlength' => '',
						'formatting' => 'html',
					),
					array (
						'key' => 'field_531fc2e5916c7',
						'label' => 'Category Halfpage Ad Code',
						'name' => 'category_halfpage_ad_code',
						'type' => 'textarea',
						'column_width' => '',
						'default_value' => '',
						'placeholder' => '',
						'maxlength' => '',
						'formatting' => 'html',
					),
				),
				'row_min' => 1,
				'row_limit' => 1,
				'layout' => 'table',
				'button_label' => 'Add Row',
			),
			array (
				'key' => 'field_531fc30c916c8',
				'label' => 'Category Sidebar Lower MPU',
				'name' => 'category_sidebar_mpu',
				'type' => 'repeater',
				'sub_fields' => array (
					array (
						'key' => 'field_531fc327916c9',
						'label' => 'Category Lower MPU Code Header',
						'name' => 'category_mpu_code_header',
						'type' => 'textarea',
						'column_width' => '',
						'default_value' => '',
						'placeholder' => '',
						'maxlength' => '',
						'formatting' => 'html',
					),
					array (
						'key' => 'field_531fc345916ca',
						'label' => 'Category Lower MPU Ad Code',
						'name' => 'category_mpu_ad_code',
						'type' => 'textarea',
						'column_width' => '',
						'default_value' => '',
						'placeholder' => '',
						'maxlength' => '',
						'formatting' => 'html',
					),
				),
				'row_min' => 1,
				'row_limit' => 1,
				'layout' => 'table',
				'button_label' => 'Add Row',
			),
			array (
				'key' => 'field_531fc38875de8',
				'label' => 'Category Leaderboard',
				'name' => 'category_leaderboard',
				'type' => 'repeater',
				'sub_fields' => array (
					array (
						'key' => 'field_531fc3a775de9',
						'label' => 'Category Leaderboard Code Header',
						'name' => 'cat_leaderboard_code_header',
						'type' => 'textarea',
						'column_width' => '',
						'default_value' => '',
						'placeholder' => '',
						'maxlength' => '',
						'formatting' => 'html',
					),
					array (
						'key' => 'field_531fc3c875dea',
						'label' => 'Category Leaderboard Ad Code',
						'name' => 'category_leaderboard_ad_code',
						'type' => 'textarea',
						'column_width' => '',
						'default_value' => '',
						'placeholder' => '',
						'maxlength' => '',
						'formatting' => 'html',
					),
				),
				'row_min' => 1,
				'row_limit' => 1,
				'layout' => 'table',
				'button_label' => 'Add Row',
			),
		),
		'location' => array (
			array (
				array (
					'param' => 'ef_taxonomy',
					'operator' => '==',
					'value' => 'all',
					'order_no' => 0,
					'group_no' => 0,
				),
			),
			array (
				array (
					'param' => 'options_page',
					'operator' => '==',
					'value' => 'acf-options-advertising',
					'order_no' => 0,
					'group_no' => 1,
				),
			),
		),
		'options' => array (
			'position' => 'normal',
			'layout' => 'no_box',
			'hide_on_screen' => array (
			),
		),
		'menu_order' => 0,
	));
	register_field_group(array (
		'id' => 'acf_general-ad-code-for-header',
		'title' => 'General Ad Code for Header',
		'fields' => array (
			array (
				'key' => 'field_531f211ce8f42',
				'label' => 'Ad Code',
				'name' => 'gen_ad_code',
				'type' => 'textarea',
				'default_value' => '',
				'placeholder' => '',
				'maxlength' => '',
				'formatting' => 'html',
			),
		),
		'location' => array (
			array (
				array (
					'param' => 'options_page',
					'operator' => '==',
					'value' => 'acf-options-advertising',
					'order_no' => 0,
					'group_no' => 0,
				),
			),
		),
		'options' => array (
			'position' => 'normal',
			'layout' => 'no_box',
			'hide_on_screen' => array (
			),
		),
		'menu_order' => 0,
	));
	register_field_group(array (
		'id' => 'acf_homepage-post-ads',
		'title' => 'Homepage & Post Ads',
		'fields' => array (
			array (
				'key' => 'field_531f1f466fb22',
				'label' => 'Home Leaderboard',
				'name' => 'home_leaderboard',
				'type' => 'repeater',
				'sub_fields' => array (
					array (
						'key' => 'field_531f203f6fb24',
						'label' => 'Home Leaderboard Code Header',
						'name' => 'home_leaderboard_code_header',
						'type' => 'textarea',
						'required' => 1,
						'column_width' => 50,
						'default_value' => '',
						'placeholder' => '',
						'maxlength' => '',
						'formatting' => 'html',
					),
					array (
						'key' => 'field_531f20636fb25',
						'label' => 'Home Leaderboard Ad Code',
						'name' => 'home_leaderboard_header_ad_acode',
						'type' => 'textarea',
						'required' => 1,
						'column_width' => 50,
						'default_value' => '',
						'placeholder' => '',
						'maxlength' => '',
						'formatting' => 'html',
					),
				),
				'row_min' => 1,
				'row_limit' => 1,
				'layout' => 'table',
				'button_label' => 'Add Window',
			),
			array (
				'key' => 'field_531fb4e9e02cf',
				'label' => 'Home MPU',
				'name' => 'home_mpu',
				'type' => 'repeater',
				'sub_fields' => array (
					array (
						'key' => 'field_531fb50ae02d0',
						'label' => 'Home MPU Code Header',
						'name' => 'home_mpu_code_header',
						'type' => 'textarea',
						'column_width' => '',
						'default_value' => '',
						'placeholder' => '',
						'maxlength' => '',
						'formatting' => 'html',
					),
					array (
						'key' => 'field_531fb526e02d1',
						'label' => 'Home MPU Ad Code',
						'name' => 'home_mpu_ad_code',
						'type' => 'textarea',
						'column_width' => '',
						'default_value' => '',
						'placeholder' => '',
						'maxlength' => '',
						'formatting' => 'html',
					),
				),
				'row_min' => 1,
				'row_limit' => 2,
				'layout' => 'table',
				'button_label' => 'Add Row',
			),
			array (
				'key' => 'field_531fc63fc0436',
				'label' => 'Post Leaderboard',
				'name' => 'post_leaderboard',
				'type' => 'repeater',
				'sub_fields' => array (
					array (
						'key' => 'field_531fc64cc0437',
						'label' => 'Post Leaderboard Header Code',
						'name' => 'post_leaderboard_header_code',
						'type' => 'textarea',
						'column_width' => '',
						'default_value' => '',
						'placeholder' => '',
						'maxlength' => '',
						'formatting' => 'html',
					),
					array (
						'key' => 'field_531fc65dc0438',
						'label' => 'Post Leaderboard Ad Code',
						'name' => 'post_leaderboard_ad_code',
						'type' => 'textarea',
						'column_width' => '',
						'default_value' => '',
						'placeholder' => '',
						'maxlength' => '',
						'formatting' => 'html',
					),
				),
				'row_min' => 1,
				'row_limit' => 1,
				'layout' => 'table',
				'button_label' => 'Add Row',
			),
			array (
				'key' => 'field_531fc66cc0439',
				'label' => 'Post Sidebar halfpage',
				'name' => 'post_halfpage',
				'type' => 'repeater',
				'sub_fields' => array (
					array (
						'key' => 'field_531fc688c043a',
						'label' => 'Post Sidebar Halfpage Header Code',
						'name' => 'post_halfpage_header_code',
						'type' => 'textarea',
						'column_width' => '',
						'default_value' => '',
						'placeholder' => '',
						'maxlength' => '',
						'formatting' => 'html',
					),
					array (
						'key' => 'field_531fc6a9c043b',
						'label' => 'Post Sidebar Halfpage Ad Code',
						'name' => 'post_halfpage_ad_code',
						'type' => 'textarea',
						'column_width' => '',
						'default_value' => '',
						'placeholder' => '',
						'maxlength' => '',
						'formatting' => 'html',
					),
				),
				'row_min' => 1,
				'row_limit' => 1,
				'layout' => 'table',
				'button_label' => 'Add Row',
			),
			array (
				'key' => 'field_531fc6c3c043c',
				'label' => 'Post Sidebar Lower MPU',
				'name' => 'post_sidebar_mpu',
				'type' => 'repeater',
				'sub_fields' => array (
					array (
						'key' => 'field_531fc6dac043d',
						'label' => 'Post Sidebar Lower MPU Header Code',
						'name' => 'post_mpu_header_code',
						'type' => 'textarea',
						'column_width' => '',
						'default_value' => '',
						'placeholder' => '',
						'maxlength' => '',
						'formatting' => 'html',
					),
					array (
						'key' => 'field_531fc6eec043e',
						'label' => 'Post Sidebar Lower MPU Ad Code',
						'name' => 'post_mpu_ad_code',
						'type' => 'textarea',
						'column_width' => '',
						'default_value' => '',
						'placeholder' => '',
						'maxlength' => '',
						'formatting' => 'html',
					),
				),
				'row_min' => 1,
				'row_limit' => 1,
				'layout' => 'table',
				'button_label' => 'Add Row',
			),
		),
		'location' => array (
			array (
				array (
					'param' => 'options_page',
					'operator' => '==',
					'value' => 'acf-options-advertising',
					'order_no' => 0,
					'group_no' => 0,
				),
			),
		),
		'options' => array (
			'position' => 'normal',
			'layout' => 'no_box',
			'hide_on_screen' => array (
			),
		),
		'menu_order' => 0,
	));
	register_field_group(array (
		'id' => 'acf_people-ads',
		'title' => 'People Ads',
		'fields' => array (
			array (
				'key' => 'field_531fce904021b',
				'label' => 'People Leaderboard',
				'name' => 'people',
				'type' => 'repeater',
				'sub_fields' => array (
					array (
						'key' => 'field_531fc4d6d9906',
						'label' => 'People Leaderboard Code Header',
						'name' => 'people_leaderboard_code_header',
						'type' => 'textarea',
						'column_width' => '',
						'default_value' => '',
						'placeholder' => '',
						'maxlength' => '',
						'formatting' => 'html',
					),
					array (
						'key' => 'field_531fc4ecd9907',
						'label' => 'People Leaderboard Ad Code',
						'name' => 'people_leaderboard_ad_code',
						'type' => 'textarea',
						'column_width' => '',
						'default_value' => '',
						'placeholder' => '',
						'maxlength' => '',
						'formatting' => 'html',
					),
				),
				'row_min' => 1,
				'row_limit' => 1,
				'layout' => 'table',
				'button_label' => 'Add Row',
			),
			array (
				'key' => 'field_531fce9040fd5',
				'label' => 'People Halfpage',
				'name' => 'people_halfpage',
				'type' => 'repeater',
				'sub_fields' => array (
					array (
						'key' => 'field_531fc55dcf64c',
						'label' => 'People Halfpage Code Header',
						'name' => 'people_halfpage_code_header',
						'type' => 'textarea',
						'column_width' => '',
						'default_value' => '',
						'placeholder' => '',
						'maxlength' => '',
						'formatting' => 'html',
					),
					array (
						'key' => 'field_531fc55dcf64d',
						'label' => 'People Halfpage Ad Code',
						'name' => 'people_halfpage_ad_code',
						'type' => 'textarea',
						'column_width' => '',
						'default_value' => '',
						'placeholder' => '',
						'maxlength' => '',
						'formatting' => 'html',
					),
				),
				'row_min' => 1,
				'row_limit' => 1,
				'layout' => 'table',
				'button_label' => 'Add Row',
			),
		),
		'location' => array (
			array (
				array (
					'param' => 'options_page',
					'operator' => '==',
					'value' => 'acf-options-advertising',
					'order_no' => 0,
					'group_no' => 0,
				),
			),
		),
		'options' => array (
			'position' => 'normal',
			'layout' => 'no_box',
			'hide_on_screen' => array (
			),
		),
		'menu_order' => 0,
	));
}

if(function_exists("register_field_group"))
{
	register_field_group(array (
		'id' => 'acf_brand-featured-ad',
		'title' => 'Brand Featured Ad',
		'fields' => array (
			array (
				'key' => 'field_53285fa382efc',
				'label' => 'Featured Brand',
				'name' => 'featured_brand',
				'type' => 'repeater',
				'sub_fields' => array (
					array (
						'key' => 'field_53285fb482efd',
						'label' => 'Featured Brand Header',
						'name' => 'featured_brand_header',
						'type' => 'textarea',
						'column_width' => '',
						'default_value' => '',
						'placeholder' => '',
						'maxlength' => '',
						'formatting' => 'html',
					),
					array (
						'key' => 'field_53285fc882efe',
						'label' => 'Featured Brand Ad Code',
						'name' => 'featured_brand_ad_code',
						'type' => 'textarea',
						'column_width' => '',
						'default_value' => '',
						'placeholder' => '',
						'maxlength' => '',
						'formatting' => 'html',
					),
				),
				'row_min' => '',
				'row_limit' => '',
				'layout' => 'table',
				'button_label' => 'Add Row',
			),
		),
		'location' => array (
			array (
				array (
					'param' => 'options_page',
					'operator' => '==',
					'value' => 'acf-options-advertising',
					'order_no' => 0,
					'group_no' => 0,
				),
			),
		),
		'options' => array (
			'position' => 'normal',
			'layout' => 'no_box',
			'hide_on_screen' => array (
			),
		),
		'menu_order' => 0,
	));
}

if(function_exists("register_field_group"))
{
	register_field_group(array (
		'id' => 'acf_brand-hashtag',
		'title' => 'Brand Hashtag',
		'fields' => array (
			array (
				'key' => 'field_53290929e2147',
				'label' => 'Brand or People Hashtag',
				'name' => 'brand_hashtag',
				'type' => 'text',
				'default_value' => '',
				'placeholder' => '',
				'prepend' => '',
				'append' => '',
				'formatting' => 'none',
				'maxlength' => '',
			),
		),
		'location' => array (
			array (
				array (
					'param' => 'post_type',
					'operator' => '==',
					'value' => 'thuong-hieu',
					'order_no' => 0,
					'group_no' => 0,
				),
			),
			array (
				array (
					'param' => 'post_type',
					'operator' => '==',
					'value' => 'chan-dung',
					'order_no' => 0,
					'group_no' => 1,
				),
			),
		),
		'options' => array (
			'position' => 'normal',
			'layout' => 'no_box',
			'hide_on_screen' => array (
			),
		),
		'menu_order' => 0,
	));
}

if(function_exists("register_field_group"))
{
	register_field_group(array (
		'id' => 'acf_featured-person',
		'title' => 'Featured Person',
		'fields' => array (
			array (
				'key' => 'field_5343bf42c54d0',
				'label' => 'Choose a "Read it next article"',
				'name' => 'featured_person',
				'type' => 'post_object',
				'post_type' => array (
 						0 => 'post',
                        1 => 'thuong-hieu',
                        2 => 'chan-dung',
				),
				'taxonomy' => array (
					0 => 'all',
				),
				'allow_null' => 1,
				'multiple' => 0,
			),
		),
		'location' => array (
			array (
				array (
					'param' => 'post_type',
					'operator' => '==',
					'value' => 'post',
					'order_no' => 0,
					'group_no' => 0,
				),
			),
		),
		'options' => array (
			'position' => 'normal',
			'layout' => 'no_box',
			'hide_on_screen' => array (
			),
		),
		'menu_order' => 0,
	));
}


/*if(function_exists("register_field_group"))
{
	register_field_group(array (
		'id' => 'acf_runway-sesason',
		'title' => 'Runway Sesason',
		'fields' => array (
			array (
				'key' => 'field_5344d21472d33',
				'label' => 'Runway Season',
				'name' => 'runway_season',
				'type' => 'select',
				'choices' => array (
					'Xuân hè RTW' => 'Xuân hè RTW',
					'Couture Xuân Hè' => 'Couture Xuân Hè',
					'Pre-fall' => 'Pre-fall',
					'Resort' => 'Resort',
					'Thu Đông RTW' => 'Thu Đông RTW',
					'Couture Thu Đông' => 'Couture Thu Đông',
				),
				'default_value' => '',
				'allow_null' => 0,
				'multiple' => 0,
			),
			array (
				'key' => 'field_5344e2ae623aa',
				'label' => 'Fashion Year',
				'name' => 'fashion_year',
				'type' => 'number',
				'default_value' => '',
				'placeholder' => 'Choose year of fashion season',
				'prepend' => '',
				'append' => '',
				'min' => 2014,
				'max' => '',
				'step' => '',
			),
		),
		'location' => array (
			array (
				array (
					'param' => 'options_page',
					'operator' => '==',
					'value' => 'acf-options-current-runway-season',
					'order_no' => 0,
					'group_no' => 0,
				),
			),
		),
		'options' => array (
			'position' => 'normal',
			'layout' => 'no_box',
			'hide_on_screen' => array (
			),
		),
		'menu_order' => 0,
	));
}*/

if(function_exists("register_field_group"))
{
	register_field_group(array (
		'id' => 'acf_runway-information',
		'title' => 'Runway Information',
		'fields' => array (
			array (
				'key' => 'field_534689a24f078',
				'label' => 'Runway Date',
				'name' => 'runway_date',
				'type' => 'date_picker',
				'date_format' => 'yymmdd',
				'display_format' => 'dd/mm/yy',
				'first_day' => 1,
			),
			array (
				'key' => 'field_53468c72c6f88',
				'label' => 'Runway Time',
				'name' => 'runway_time',
				'type' => 'text',
				'default_value' => '',
				'placeholder' => '',
				'prepend' => '',
				'append' => '',
				'formatting' => 'html',
				'maxlength' => '',
			),
			array (
				'key' => 'field_53468c5da4405',
				'label' => 'Runway Subcat',
				'name' => 'runway_subcat',
				'type' => 'text',
				'default_value' => '',
				'placeholder' => '',
				'prepend' => '',
				'append' => '',
				'formatting' => 'none',
				'maxlength' => '',
			),
			array (
				'key' => 'field_53468fad04acd',
				'label' => 'Runway City',
				'name' => 'runway_city',
				'type' => 'select',
				'choices' => array (
					'Paris' => 'Paris',
					'London' => 'London',
					'Milan' => 'Milan',
					'New York' => 'New York',
				),
				'default_value' => '',
				'allow_null' => 0,
				'multiple' => 0,
			),
		),
		'location' => array (
			array (
				array (
					'param' => 'post_type',
					'operator' => '==',
					'value' => 'runway',
					'order_no' => 0,
					'group_no' => 0,
				),
			),
		),
		'options' => array (
			'position' => 'normal',
			'layout' => 'no_box',
			'hide_on_screen' => array (
			),
		),
		'menu_order' => 0,
	));
}

if(function_exists("register_field_group")){
	register_field_group(array (
		'id' => 'acf_runway-linked',
		'title' => 'Runway Linked',
		'fields' => array (
			array (
				'key' => 'field_53995ca6f60e0',
				'label' => 'Link to article?',
				'name' => 'runway_linked',
				'type' => 'checkbox',
				'choices' => array (
					'yes' => 'yes',
				),
				'default_value' => '',
				'layout' => 'vertical',
			),
		),
		'location' => array (
			array (
				array (
					'param' => 'post_type',
					'operator' => '==',
					'value' => 'runway',
					'order_no' => 0,
					'group_no' => 0,
				),
			),
		),
		'options' => array (
			'position' => 'normal',
			'layout' => 'no_box',
			'hide_on_screen' => array (
			),
		),
		'menu_order' => 0,
	));
}

if(function_exists("register_field_group"))
{
	register_field_group(array (
		'id' => 'acf_brand-connection',
		'title' => 'Brand Connection',
		'fields' => array (
			array (
				'key' => 'field_53996a60b90da',
				'label' => 'Runway Brand',
				'name' => 'runway_brand',
				'type' => 'post_object',
				'post_type' => array (
					0 => 'thuong-hieu',
				),
				'taxonomy' => array (
					0 => 'all',
				),
				'allow_null' => 1,
				'multiple' => 0,
			),
		),
		'location' => array (
			array (
				array (
					'param' => 'post_type',
					'operator' => '==',
					'value' => 'runway',
					'order_no' => 0,
					'group_no' => 0,
				),
			),
		),
		'options' => array (
			'position' => 'normal',
			'layout' => 'no_box',
			'hide_on_screen' => array (
			),
		),
		'menu_order' => 0,
	));
}

if(function_exists("register_field_group"))
{
	register_field_group(array (
		'id' => 'acf_runway-seasons',
		'title' => 'Runway Seasons',
		'fields' => array (
			array (
				'key' => 'field_542bb46f2397c',
				'label' => 'Runway Season',
				'name' => 'runway_season',
				'type' => 'repeater',
				'sub_fields' => array (
					array (
						'key' => 'field_542bb5052397d',
						'label' => 'Runway Tag',
						'name' => 'runway_tag',
						'type' => 'taxonomy',
						'column_width' => 25,
						'taxonomy' => 'post_tag',
						'field_type' => 'select',
						'allow_null' => 0,
						'load_save_terms' => 0,
						'return_format' => 'object',
						'multiple' => 0,
					),
					array (
						'key' => 'field_542bb5372397e',
						'label' => 'Runway image',
						'name' => 'runway_image',
						'type' => 'image',
						'column_width' => '',
						'save_format' => 'url',
						'preview_size' => 'medium',
						'library' => 'all',
					),
				),
				'row_min' => 0,
				'row_limit' => 6,
				'layout' => 'table',
				'button_label' => 'Add Row',
			),
		),
		'location' => array (
			array (
				array (
					'param' => 'options_page',
					'operator' => '==',
					'value' => 'acf-options-current-runway-season',
					'order_no' => 0,
					'group_no' => 0,
				),
			),
		),
		'options' => array (
			'position' => 'normal',
			'layout' => 'no_box',
			'hide_on_screen' => array (
			),
		),
		'menu_order' => 0,
	));
}

if(function_exists("register_field_group"))
{
	register_field_group(array (
		'id' => 'acf_facebook-ad-codes',
		'title' => 'Facebook Ad Codes',
		'fields' => array (
			array (
				'key' => 'field_54360e68cf952',
				'label' => 'Facebook Pixel script',
				'name' => 'facebook_pixel_script',
				'type' => 'textarea',
				'default_value' => '',
				'placeholder' => '',
				'maxlength' => '',
				'formatting' => 'html',
			),
		),
		'location' => array (
			array (
				array (
					'param' => 'post_type',
					'operator' => '==',
					'value' => 'post',
					'order_no' => 0,
					'group_no' => 0,
				),
			),
			array (
				array (
					'param' => 'post_type',
					'operator' => '==',
					'value' => 'fashion-trends',
					'order_no' => 0,
					'group_no' => 1,
				),
			),
			array (
				array (
					'param' => 'post_type',
					'operator' => '==',
					'value' => 'page',
					'order_no' => 0,
					'group_no' => 2,
				),
			),
			array (
				array (
					'param' => 'post_type',
					'operator' => '==',
					'value' => 'thuong-hieu',
					'order_no' => 0,
					'group_no' => 3,
				),
			),
			array (
				array (
					'param' => 'post_type',
					'operator' => '==',
					'value' => 'chan-dung',
					'order_no' => 0,
					'group_no' => 4,
				),
			),
		),
		'options' => array (
			'position' => 'normal',
			'layout' => 'no_box',
			'hide_on_screen' => array (
			),
		),
		'menu_order' => 0,
	));
}


?>