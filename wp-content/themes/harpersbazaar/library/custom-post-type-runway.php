<?php 
/*
add_action('init', 'cptui_register_my_cpt_runway');
function cptui_register_my_cpt_runway() {
register_post_type('runway', array(
'label' => 'Runways',
'description' => '',
'public' => true,
'show_ui' => true,
'show_in_menu' => true,
'capability_type' => 'post',
'map_meta_cap' => true,
'hierarchical' => false,
'rewrite' => array('slug' => 'runway_old', 'with_front' => true),
'query_var' => true,
'has_archive' => true,
'taxonomies' => array('post_tag'),
'supports' => array('title','editor','excerpt','trackbacks','custom-fields','comments','revisions','thumbnail','author','page-attributes','post-formats'),
'labels' => array (
  'name' => 'Runways',
  'singular_name' => 'runway',
  'menu_name' => 'Runways',
  'add_new' => 'Add runway',
  'add_new_item' => 'Add New runway',
  'edit' => 'Edit',
  'edit_item' => 'Edit runway',
  'new_item' => 'New runway',
  'view' => 'View runway',
  'view_item' => 'View runway',
  'search_items' => 'Search Runways',
  'not_found' => 'No Runways Found',
  'not_found_in_trash' => 'No Runways Found in Trash',
  'parent' => 'Parent runway',
)
) ); }

add_action('init', 'cptui_register_my_taxes_seasons');
function cptui_register_my_taxes_seasons() {
register_taxonomy( 'seasons',array (
  0 => 'runway',
),
array( 'hierarchical' => false,
	'label' => 'Runway Seasons',
	'show_ui' => true,
	'query_var' => true,
	'show_admin_column' => false,
	'labels' => array (
  'search_items' => 'Runway Season',
  'popular_items' => '',
  'all_items' => '',
  'parent_item' => '',
  'parent_item_colon' => '',
  'edit_item' => '',
  'update_item' => '',
  'add_new_item' => '',
  'new_item_name' => '',
  'separate_items_with_commas' => '',
  'add_or_remove_items' => '',
  'choose_from_most_used' => '',
)
) ); 
}



*/

?>