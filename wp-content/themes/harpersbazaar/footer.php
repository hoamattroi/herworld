			<?php get_template_part("parts/bazaar-tv"); ?>

			<footer class="footer" role="contentinfo">
				<div id="inner-footer" class="container clearfix">
					<div id="inner_footer_row">
					<div id="issue_frontcover">
						<img src="<?php if(get_field('magazine_cover', 'option')){ echo getImgServerUrl(get_field('magazine_cover', 'option')); } ?>" alt="<?php bloginfo( 'name' ) ?>" title="<?php bloginfo( 'name' ) ?>" />
					</div>
					<div id="inner_footer_wrapper">
						<div id="magazine_title">
							<div id="magazine_desc">
								<h4><?php _e('Tạp chí', 'harpersbazaar'); ?></h4>
								<h3 itemprop="publisher"><?php _e('harper’s bazaar việt nam', 'harpersbazaar'); ?></h3>
							</div>
							<div id="past_issues">
								<!-- div class="footer_button">
									<a href="#footerbutton"><?php _e('Xem các số báo trước', 'harpersbazaar'); ?></a>
								</div -->
							</div>
						</div>
						<nav role="navigation">
							<?php bones_footer_links(); ?>
						</nav>
						<div id="social_interaction">
							<div id="integration_wrapper">
								<p><p>
								<h3<br/></h3>
								
							</div>
							<div id="social_wrapper">
								<div>
									<a class="social-icon social-icon-facebook" href="https://www.facebook.com/HarpersBazaarVN">Facebook</a>
									<a class="social-icon social-icon-pinterest" href="https://www.pinterest.com">Pinterest</a>
									<a class="social-icon social-icon-gplus" href="https://plus.google.com/103707460003962033216/">Google Plus</a>
									<a class="social-icon social-icon-youtube" href="http://www.youtube.com/user/HarpersBazaarVN">YouTube</a>
								</div> 
								<p><?php _e("CÔNG TY TNHH TRUYỀN THÔNG HOA MẶT TRỜI<br />
&copy; Copyright Harper's Bazaar Việt Nam, All rights reserved<br />
Số 258 Nam Kỳ Khởi Nghĩa, P8, Q3, TP.HCM<br />
(+84 8) 393 28 000", "harpersbazaar"); ?><br/>Phiên bản thử nghiệm Beta</p>
							</div>
						</div>
					</div>
					</div>
				</div> <!-- end #inner-footer -->

			</footer> <!-- end footer -->
			<div id="back_to_top_button"><a href="#" class="back-to-top">&nbsp;</a></div>
		</div> <!-- end #container -->

		<!-- all js scripts are loaded in library/bones.php -->
		<?php wp_footer(); ?>
		<!--<script type="text/javascript" async src="//assets.pinterest.com/js/pinit.js"></script>-->
	</body>

</html> <!-- end page. what a ride! -->