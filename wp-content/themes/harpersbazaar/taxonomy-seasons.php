<?php get_header(); 

global $current_season;
global $current_season_id;
global $current_date;


$season = get_field('runway_season','option');
$fashion_year = get_field('fashion_year', 'option');
$current_season = $season ." " . $fashion_year;
$current_season_obj = get_term_by('name', $current_season, 'seasons');
$current_season_id = $current_season_obj->term_id;
$current_date = date('Ymd');

$taxonomy = get_query_var( 'taxonomy' );
$queried_object = get_queried_object();
$current_term_name = $queried_object->name;

?>

<div id="content">
	<div class="container">
		<div id="main" class="clearfix" role="main">

			<div class="article-pre">
				<?php if ( function_exists('yoast_breadcrumb') ) {
					yoast_breadcrumb('<p class="breadcrumbs">','</p>');
				} ?>
			</div>

			<div class="page-wrapper">

				<div class="category-runway">

					<h1 class="category-title barred-heading">
						<span><?php echo $current_term_name; ?></span>
					</h1>
					<?php get_template_part("parts/shows-latest"); ?>

					<h1 class="category-title barred-heading">
						<span><?php _e("Lịch Trình Diẻn", "harpersbazaar"); ?></span>
					</h1>
					
					<section class="schedule">
						<h2 class="schedule-title"><?php echo __('The ', 'harpersbazaar') . $current_term_name .__(" Calendar", "harpersbazaar"); ?></h2>
						<div class="schedule-main">
							<div class="schedule-controls">
								<div class="schedule-calendar">
									<?php get_runway_calendar( false, true ); ?> 
									<footer>
										<ul>
											<li class="bullet bullet-blue">Paris</li>
											<li class="bullet bullet-orange">London</li>
											<li class="bullet bullet-green">Milan</li>
											<li class="bullet bullet-red">New York</li>
										</ul>
									</footer>
								</div>
							</div>
							<div class="schedule-listings">
								<?php 
								
								$args = array(
									'post_status' => 'publish',
									'post_type' => 'runway', 
									'posts_per_page' => -1,
									'meta_key' => 'runway_time',
            						'orderby' => 'meta_value_num',
            						'order' => 'ASC',
									'tax_query' => array(
							            array(
							                'taxonomy' => 'seasons',
							                'field' => 'name',
							                'terms' => $current_term_name
								        )
							        ),
							        'meta_query' => array(
										array(
											'key' => 'runway_date',
											'value' => $current_date,
										)
									)
								);	
								$todayPosts = get_posts( $args );
								?>
								
								<header><?php echo date('l F d, Y'); ?></header>
								<?php foreach($todayPosts as $today_post) : ?>
								<div class="schedule-entry">
									<div class="schedule-time">
										
										<span><?php the_field('runway_time', $today_post->ID); ?></span>
									</div>
									<div class="schedule-detail">
										<?php
										$city = get_field('runway_city', $today_post->ID);
										if($city == "Paris"){
											$bullet_color = "bullet-blue";
										}elseif($city == "London"){
											$bullet_color = "bullet-orange";
										}elseif($city == "Milan"){
											$bullet_color = "bullet-green";
										} else{
											$bullet_color = "bullet-red";
										}
										$runway_linked = get_field('runway_linked', $today_post->ID);
										?>
										<h3 class="bullet <?php echo $bullet_color; ?>"><a href="<?php echo get_permalink( $today_post->ID ); ?>"><?php echo $today_post->post_title; ?></a></h3>
										<p><?php the_field('runway_subcat', $today_post->ID); ?></p>
									</div>
								</div>
								<?php endforeach; ?>
							</div>
						</div>
					</section>
					<h1 class="barred-heading category-title">
						<span><?php _e("Mới Nhất", "harpersbazaar"); ?></span>
					</h1>
					<?php 
					
					$args = array(
					    'orderby'       => 'name', 
					    'order'         => 'ASC',
					    'hide_empty'    => true
					); 
					
					$custom_taxes = get_terms('seasons', $args);
					$tax_categories = array('xuan-he-rtw','couture-xuan-he','pre-fall','thu-dong-rtw', 'couture-thu-dong', 'resort');
					?>
					<section class="runway_latest">
						<?php
						foreach($custom_taxes as $tax) : 
	
							$taxname = $tax->slug;	
							for($i=0;$i<=sizeof($tax_categories);$i++){
								if(strpos($tax->slug, $tax_categories[$i]) !== false ){
									$tax_category = $tax_categories[$i];
								}
							}
							
							$fashion_year2 = $fashion_year-1;
													
							if(strpos($taxname,$tax_category) !== false){
								
								if(!(strpos($taxname,$fashion_year) == false) || !(strpos($taxname,$fashion_year2) == false)){  ?>
								<article id="season-<?php echo $tax->slug; ?>" <?php post_class('clearfix'); ?>>							
									<a href="<?php bloginfo('wpurl') ?>/seasons/<?php echo $tax->slug; ?>" rel="bookmark">
										<?php 
			
										$args = array(
											'orderby' => 'post_date',
											'order' => 'DESC',
											'post_status' => 'publish',
											'post_type' => 'runway', 
											'posts_per_page' => 1,
											'tax_query' => array(
									            array(
									                'taxonomy' => 'seasons',
									                'field' => 'id',
									                'terms' => $tax->term_id
										        )
									        )
										);
										
										$post_season = get_posts($args);
										
										foreach( $post_season as $post ) :
											setup_postdata($post);
											echo get_the_post_thumbnail($post->ID, 'runway_category');
										endforeach; 
										wp_reset_postdata();
										
										 ?>
										<h2 class="h2"><?php echo $tax->name; ?></h2>
									</a>
								</article> <!-- end article -->
							<?php
								} 
							}
							$counter++;
						endforeach; ?>
					</section>
					<h1 class="barred-heading category-title">
						<span><?php _e("Cả Các Mừa Thời Trang", "harpersbazaar"); ?></span>
					</h1>
					
					<?php
					wp_reset_query(); ?>
					<section class="runway_latest">
					<?php
					for($i=0;$i<=sizeof($tax_categories);$i++){ ?>
							<?php 
							$counter = 0;
							foreach($custom_taxes as $tax) : 
								if(strpos($tax->slug, $tax_categories[$i]) !== false){ ?>
									<article <?php post_class('clearfix'); ?>>
									<?php
									if($counter == 0){
										$args = array(
											'orderby' => 'post_date',
											'order' => 'DESC',
											'post_status' => 'publish',
											'post_type' => 'runway', 
											'posts_per_page' => 1,
											'tax_query' => array(
									            array(
									                'taxonomy' => 'seasons',
									                'field' => 'id',
									                'terms' => $tax->term_id
										        )
									        )
										); 
									
										$post_season = get_posts($args); 
										echo get_the_post_thumbnail($post_season[0]->ID,'runway_category'); ?>
										
										<div>
												<span><?php echo $tax_categories[$i]; ?></span><ul>
										<?php
										$args_2 = array(
											'orderby'       => 'name', 
	    									'order'         => 'DESC',
	    									'number'		=> 10
										);
										
										$all_tax = get_terms('seasons', $args_2);
										
										// print_r($all_tax);
										
										foreach($all_tax as $tax_1) : 
											if(strpos($tax_1->slug, $tax_categories[$i]) !== false){?>
												<li><a href="<?php bloginfo('wpurl'); ?>/seasons/<?php echo $tax_1->slug; ?>"><?php echo $tax_1->name; ?></a></option>
										<?php 	}
											endforeach; ?>
										</ul>
								<?php
									$counter++;
									}

						?> </article>	<?php
								}
								
							endforeach; ?>	
					<?php } ?>
					</section>
				</div> <!-- .category-content-wrapper -->

				<?php get_sidebar(); ?>

			</div> <!-- end .page-wrapper -->

		</div> <!-- end #main -->
	</div> <!-- end .container -->

</div> <!-- end #content -->

<section class="tags moremore">
	<h2 class="more-heading"><span><?php _e("XEM THÊM...", "harpersbazaar"); ?></span></h2>
	<ul class=more_tags><?php 
		$curr_cat = get_cat_id( single_cat_title("",false) );
		top_tags($curr_cat); 
		?></ul>
</section>

<?php get_footer(); ?>
