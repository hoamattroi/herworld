<?php get_header(); ?>

<div id="content">
	<div class="container">
		<div id="main" class="clearfix" role="main">

			<div class="article-pre">
				<?php if ( function_exists('yoast_breadcrumb') ) {
					yoast_breadcrumb('<p class="breadcrumbs">','</p>');
				} ?>
			</div>

			<div class="page-wrapper">
				<?php if (have_posts()) : while (have_posts()) : the_post(); ?>

				<div class="article-wrapper">
					<article class="article-primary" id="post-<?php the_ID(); ?>" <?php post_class('clearfix'); ?> role="article" itemscope itemtype="http://schema.org/Article">

						<header class="article-header">
							<?php the_category(); ?>
							<h1 class="entry-title single-title" itemprop="headline"><?php the_title(); ?></h1>
							<?php
								$myExcerpt = get_the_excerpt();
								$tags = array("<p>", "</p>");
								$myExcerpt = str_replace($tags, "", $myExcerpt);
  
							?>
							<p class="standfirst"><?php echo $myExcerpt; ?></p>
							<div class="byline vcard clearfix">
								<div class="authorship">
									<?php _e("Do", 'harpersbazaar'); ?>
									<span class="author" itemprop="author"><?php the_author_posts_link(); ?></span>
									<?php _e("đăng ngày", 'harpersbazaar'); ?>
									<time class="time" pubdate itemprop="datePublished"><?php the_date('d-m-Y'); ?></time>
								</div>
								<?php if(!is_preview()){  ?>
								<div class="share">
									<span><?php _e("chia sẻ trên", 'harpersbazaar'); ?></span>
									<!-- a class="social-icon social-icon-zing-dark" name="zm_share" type="text" title="Chia sẻ lên Zing Me"><?php _e('Share on Zing Me','harpersbazaar'); ?></a -->
									<a class="social-icon social-icon-gplus-dark" href="https://plus.google.com/share?url=<?php the_permalink(); ?>" target="_blank"><?php _e('Chia sẻ Google Plus','harpersbazaar'); ?></a>
									
									<a class="social-icon social-icon-facebook-dark" href="https://www.facebook.com/sharer/sharer.php?u=<?php the_permalink(); ?>" target="_blank"><?php _e('Chia sẻ Facebook','harpersbazaar'); ?></a>
								</div>
								<?php } ?>
							</div>
							

						</header> <!-- end article header -->

						<section class="entry-content clearfix" itemprop="articleBody">

							<?php 
								if(get_field('tabbed-slideshow')) {
									get_template_part("parts/slideshow-tabbed");
								} elseif(get_field('video_or_youtube_link')){
									$videolink = get_field('video_or_youtube_link');
									$field = get_field_object('youtube_or_local?' );
									if($field[ 'value' ]=='Local'){
										echo JWP6_Shortcode::the_content_filter('[jwplayer mediaid="'.$videolink.'"]');
									} else { 
										$videolink = "http://www.youtube.com/embed/". $videolink ."/?autoplay=1";
										?>
										<div class="video-container" itemprop="video">
											<iframe src="<?php echo $videolink ?>" frameborder="0" ></iframe>
										</div>
										<?php
									}
								} else {
									the_post_thumbnail( 'full');
								}
							?>
							
							<?php the_content(); 
							if(!(is_preview()) ){ ?>
							<div class="fb-like" data-width="400" data-layout="button" data-action="like" data-show-faces="false" data-share="false"></div>
						</section> <!-- end article section -->
						<?php } ?>
						
					<?php if(!(get_field('tabbed-slideshow')) && !(is_preview()) ) : ?>
						<div class="inline-wrapper">
							<div class="inline-sharing">
								<h4><?php _e('Chia sẻ trên', 'harpersbazaar'); ?></h4>
								<div class="inline-icon-wrap">
									<a class="social-icon social-icon-facebook-dark" href="https://www.facebook.com/sharer/sharer.php?u=<?php the_permalink(); ?>" target="_blank"><?php _e('Share on Facebook','harpersbazaar'); ?></a>
								</div>
								<!-- div class="inline-icon-wrap">
									<a class="social-icon social-icon-zing-dark" name="zm_share" type="text" title="Chia sẻ lên Zing Me"><?php _e('Share on Zing Me','harpersbazaar'); ?></a>
								</div -->
								<div class="inline-icon-wrap">
									<a class="social-icon social-icon-gplus-dark" href="https://plus.google.com/share?url=<?php the_permalink(); ?>" target="_blank"><?php _e('Share on Google Plus','harpersbazaar'); ?></a>
								</div>
							</div>
						</div>
						
						<?php related_posts(); ?>
					<?php endif; ?>
						<?php if(!is_preview()){  ?>
						<section class="article-interactivity">
							<?php $permalink = get_permalink(); ?>
							<div class="article-interactivity-wrapper">
								<div class="share-wrapper">
									<div class="share-facebook">
										<a href="http://www.facebook.com/sharer/sharer.php?s=100&amp;p[url]=<?php echo $permalink; ?>&amp;p[title]=<?php the_title(); ?>&amp;p[summary]=<?php echo strip_tags(get_the_excerpt()); ?>"><?php echo getFacebookShares($permalink); ?> <?php _e('Chia sẻ FB', 'harpersbazaar'); ?></a>
									</div>
									<div class="share-gplus"><a href="https://plus.google.com/share?url=<?php echo $permalink; ?>"><?php echo getGplusShares($permalink); ?> <?php _e('Chia sẻ G+', 'harpersbazaar'); ?></a></div>
									<div class="share-comment"><a href="#article-comments"><?php _e('Bình luận', 'harpersbazaar'); ?></a></div>
								</div>
							</div>
							
							<!-- div class="share-zing"><a href="#share-zing"> <?php _e('shares', 'harpersbazaar'); ?></a></div>
							<script src="http://stc.ugc.zdn.vn/link/js/lwb.0.7.js" type="text/javascript"></script -->
						</section>
						<?php } ?>

					</article> <!-- end article -->

				<?php if(get_field('featured_person')) {
						$featured_person = get_field('featured_person');
					?>
					
					<div class="container">
					<article class="related-featured">
						<a href="<?php echo get_permalink( $featured_person->ID); ?>">
							<?php echo get_the_post_thumbnail( $featured_person->ID, 'featured-person'); ?>
							<div class="related-featured-info">
								<p class="category">Có thể bạn thích:</p>
								<h1><?php echo $featured_person->post_title;  ?></h1>
							</div>
						</a>
					</article>
					</div>
				<?php } ?>
					
					<div class="recent_post_wrap">
					<section class="recent-posts">
					
					<?php
					$curr_post_id = get_the_id();
					$curr_category = get_the_category();
					$args=array(
						'cat' => $curr_category[0]->term_id,
						'post_type' => 'post',
						'post_status' => 'publish',
						'posts_per_page' => 9,
						'caller_get_posts'=> 1
					);
					$my_query = null;
					$my_query = new WP_Query($args);
					if( $my_query->have_posts() ) {
						echo '<h3>'. __('Đừng bỏ qua:', 'harpersbazaar').'</h3>';
						while ($my_query->have_posts()) : $my_query->the_post(); ?>
							<article>
									<div><a href="<?php the_permalink() ?>" rel="bookmark" title="<?php the_title_attribute(); ?>"><?php echo get_the_post_thumbnail(get_the_id(), 'square-360'); ?></a></div>
									<a href="<?php the_permalink() ?>" rel="bookmark" title="<?php the_title_attribute(); ?>"><?php the_title(); ?></a>
							</article>
				<?php	endwhile;
					 }
					wp_reset_query();  // Restore global post data stomped by the_post(). 
					?>
					
					</section>
					</div>
					
					
					<footer class="article-footer">
						<?php if(has_tag()){ ?>
							<h3><?php _e('Từ khóa cho bạn', 'harpersbazaar'); ?></h3>
							<?php the_tags('<ul class="tags" itemprop="keywords"><li>','</li><li>','</li></ul>'); ?>
						<?php } ?>
					</footer> <!-- end article footer -->
					<div id="article-comments">
						<h3><?php _e("Bình luận", 'harpersbazaar'); ?></h3>
						<?php comments_template(); ?>
						
						<!--<div class="fb-comments" data-href="<?php the_permalink();?>" data-numposts="5" data-colorscheme="light"></div>
						<style>.fb-comments, .fb-comments iframe[style], .fb-like-box, .fb-like-box iframe[style] {width: 100% !important;}.fb-comments span, .fb-comments iframe span[style], .fb-like-box iframe span[style] {width: 100% !important;}.uiBoxLightblue iframe{display:none !important;}</style>-->
					</div>
				</div>

				<?php endwhile; ?>

				<?php else : ?>

					<article id="post-not-found" class="hentry clearfix">
						<header class="article-header">
							<h1><?php _e("Không tìm thấy bài viết!", "harpersbazaar"); ?></h1>
						</header>
					</article>
				<?php endif; ?>

				<?php get_sidebar(); ?>

			</div> <!-- end .page-wrapper -->

		</div> <!-- end #main -->
	</div> <!-- end .container -->
</div> <!-- end #content -->

<?php get_footer(); ?>
