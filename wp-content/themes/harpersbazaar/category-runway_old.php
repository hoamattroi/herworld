<?php get_header(); ?>

<div id="content">
	<div class="container">
		<div id="main" class="clearfix" role="main">

			<div class="article-pre">
				<?php if ( function_exists('yoast_breadcrumb') ) {
					yoast_breadcrumb('<p class="breadcrumbs">','</p>');
				} ?>
			</div>

			<div class="page-wrapper">

				<div class="category-content-wrapper category-runway">

					<h1 class="category-title barred-heading">
						<span><?php _e("Xuân Hè 2014", "harpersbazaar"); ?></span>
					</h1>
					<?php get_template_part("parts/shows-latest"); ?>

					<h1 class="category-title barred-heading">
						<span><?php single_cat_title(); ?></span>
					</h1>

					<h1 class="category-title barred-heading">
						<span><?php _e("Lịch Trình Diễn", "harpersbazaar"); ?></span>
					</h1>

					<section class="schedule">
						<h2 class="schedule-title">The Spring Ready to Wear Schedule</h2>
						<div class="schedule-main">
							<div class="schedule-controls">
								<div class="schedule-calendar">
									<h3>March 2014</h3>
									<table class="calendar-month">
										<thead>
											<tr>
												<td>Mo</td>
												<td>Tu</td>
												<td>We</td>
												<td>Th</td>
												<td>Fr</td>
												<td>Sa</td>
												<td>Su</td>
											</tr>
										</thead>
										<tbody>
											<tr>
												<td>1</td>
												<td>2</td>
												<td>3</td>
												<td>4</td>
												<td>5</td>
												<td>6</td>
												<td>7</td>
											</tr>
											<tr>
												<td>8</td>
												<td>9</td>
												<td>10</td>
												<td>11</td>
												<td>12</td>
												<td>13</td>
												<td>14</td>
											</tr>
											<tr>
												<td>15</td>
												<td>16</td>
												<td>17</td>
												<td>18</td>
												<td>19</td>
												<td>20</td>
												<td>21</td>
											</tr>
											<tr>
												<td>22</td>
												<td>23</td>
												<td>24</td>
												<td>25</td>
												<td>26</td>
												<td>27</td>
												<td>28</td>
											</tr>
											<tr>
												<td>29</td>
												<td>30</td>
												<td>31</td>
											</tr>
										</tbody>
									</table>
									<footer>
										<ul>
											<li class="bullet bullet-blue">City Name</li>
											<li class="bullet bullet-orange">City Name</li>
											<li class="bullet bullet-green">City Name</li>
											<li class="bullet bullet-red">City Name</li>
										</ul>
									</footer>
								</div>
							</div>
							<div class="schedule-listings">
								<header>Tuesday March 9, 2014</header>
								<div class="schedule-entry">
									<div class="schedule-time">
										<span>12:45&nbsp;am</span>
									</div>
									<div class="schedule-detail">
										<h3 class="bullet bullet-orange">Giles</h3>
										<p>EC4 - The Stationers Company</p>
									</div>
								</div>
								<div class="schedule-entry">
									<div class="schedule-time">12:45&nbsp;am</div>
									<div class="schedule-detail">
										<h3 class="bullet bullet-orange">Giles</h3>
										<p>EC4 - The Stationers Company</p>
									</div>
								</div>
								<div class="schedule-entry">
									<div class="schedule-time">12:45&nbsp;am</div>
									<div class="schedule-detail">
										<h3 class="bullet bullet-orange">Giles</h3>
										<p>EC4 - The Stationers Company</p>
									</div>
								</div>
								<div class="schedule-entry">
									<div class="schedule-time">12:45&nbsp;am</div>
									<div class="schedule-detail">
										<h3 class="bullet bullet-orange">Giles</h3>
										<p>EC4 - The Stationers Company</p>
									</div>
								</div>
								<div class="schedule-entry">
									<div class="schedule-time">12:45&nbsp;am</div>
									<div class="schedule-detail">
										<h3 class="bullet bullet-orange">Giles</h3>
										<p>EC4 - The Stationers Company</p>
									</div>
								</div>
								<div class="schedule-entry">
									<div class="schedule-time">12:45&nbsp;am</div>
									<div class="schedule-detail">
										<h3 class="bullet bullet-orange">Giles</h3>
										<p>EC4 - The Stationers Company</p>
									</div>
								</div>
								<div class="schedule-entry">
									<div class="schedule-time">12:45&nbsp;am</div>
									<div class="schedule-detail">
										<h3 class="bullet bullet-orange">Giles</h3>
										<p>EC4 - The Stationers Company</p>
									</div>
								</div>
							</div>
						</div>
					</section>

					<div class="row">
					<?php $postCount = 1; ?>
					<?php if (have_posts()) : while (have_posts()) : the_post(); ?>
						<?php $postCount++; ?>

						<article id="post-<?php the_ID(); ?>" <?php post_class('clearfix'); ?> role="article">
							<header class="article-header">
								<?php the_post_thumbnail('square-360'); ?>
								<h2 class="h2"><a href="<?php the_permalink() ?>" rel="bookmark" title="<?php the_title_attribute(); ?>"><?php the_title(); ?></a></h2>
								<p class="byline vcard">
									<?php printf(__('trong %1$s.', 'harpersbazaar'), get_the_category_list(', ')); ?>
								</p>
							</header> <!-- end article header -->
						</article> <!-- end article -->

						<?php if ($postCount == 10): ?>
							<section class="subscribe-actions clearfix">
								<div class="subscribe-email newsletter-form">
									<h3><?php _e("Cập nhật ngay", "harpersbazaar"); ?></h3>
									<p><?php _e("Đăng ký nhận email mỗi tuần và cập nhật xu hướng mới nhất", "harpersbazaar"); ?></p>
									<div class="row">
									<?php gravity_form("Newsletter", $display_title=false, $display_description=false, $display_inactive=false, $field_values=null, $ajax=true); ?>
									</div>
								</div>
								<div class="subscribe-mag">
									<div class="row">
										<div class="mag_wrapper"><img src="<?php if(the_field('magazine_cover', 'option')){ echo the_field('magazine_cover', 'option'); } ?>" alt="<?php bloginfo( 'name' ) ?>" title="<?php bloginfo( 'name' ) ?>" /></div>
										<div class="subscribe-mag-content">
											<h3><?php _e("Tạp Chí", "harpersbazaar"); ?></h3>
											<p><?php _e("Cập nhật Xu Hướng Mới Nhất và Bí Quyết Mua Sắm trong mùa", "harpersbazaar"); ?></p>
											<a class="button" href="#subscribe"><?php _e("Đăng Ký", "harpersbazaar"); ?></a>
										</div>
									</div>
								</div>
							</section>
						<?php endif; ?>

					<?php endwhile; ?>


					<?php if (function_exists('bones_page_navi')) { ?>
						<?php bones_page_navi(); ?>
					<?php } else { ?>
						<nav class="wp-prev-next">
							<ul class="clearfix">
								<li class="prev-link"><?php next_posts_link(__('&laquo; Older Entries', "harpersbazaar")) ?></li>
								<li class="next-link"><?php previous_posts_link(__('Newer Entries &raquo;', "harpersbazaar")) ?></li>
							</ul>
						</nav>
					<?php } ?>

					<?php else : ?>

						<div id="post-not-found" class="hentry clearfix">
							<p><?php _e("Chưa có bài viết trong chuyên mục này", "harpersbazaar"); ?></p>
						</div>

					<?php endif; ?>
					</div> <!-- .row -->

					<h1 class="category-title">
						<span><?php _e("Kiến Thức", "harpersbazaar"); ?></span>
					</h1>
					<?php 
					$postArgs = array(
						'category' => $object_id,
						'orderby' => 'post_date',
						'order' => 'DESC',
						'post_status' => 'publish',
						'post_type' => 'post',  
						'posts_per_page' => 4,
					);
					$posts = get_posts($postArgs);
					
					foreach( $posts as $post ) :
						setup_postdata($essayPosts);
					?>
						<article id="post-<?php the_ID(); ?>" <?php post_class('clearfix'); ?> role="article">
							<?php the_post_thumbnail('thumb-300x410'); ?>
							<h2 class="h2"><a href="<?php the_permalink() ?>" rel="bookmark" title="<?php the_title_attribute(); ?>"><?php the_title(); ?></a></h2>
						</article> <!-- end article -->

					<?php
					endforeach;
					wp_reset_postdata();
					?>

				</div> <!-- .category-content-wrapper -->

				<?php get_sidebar(); ?>

			</div> <!-- end .page-wrapper -->

		</div> <!-- end #main -->
	</div> <!-- end .container -->

</div> <!-- end #content -->

<section class="tags moremore">
	<h2 class="more-heading"><span><?php _e("Xem thêm...", "harpersbazaar"); ?></span></h2>
	<ul class=more_tags><?php 
		$curr_cat = get_cat_id( single_cat_title("",false) );
		top_tags($curr_cat); 
		?></ul>
</section>

<?php get_footer(); ?>
