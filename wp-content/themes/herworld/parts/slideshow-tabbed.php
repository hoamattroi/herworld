<?php
// $tabCounter = 1;
// $tabsHTML = "";
// if( have_rows('slideshow') ) {
// 	while ( have_rows('slideshow') ) : the_row();
// 		if ($tabCounter == 1) {
// 			$tabActiveClass = "active";
// 		} else {
// 			$tabActiveClass= "";
// 		}
// 		$tabHeading = get_sub_field("tab_heading");
// 		$tabsHTML.="<li class=\"$tabActiveClass\"><a href=\"#tab-$tabCounter\" data-toggle=\"pill\">$tabHeading</a></li>";
// 		$tabCounter++;
// 	endwhile;
// }

// if ($tabCounter >2) {
// 	echo "<nav class=\"nav-tabbed\">";
// 		echo "<ul class=\"nav nav-pills\">";
// 			echo $tabsHTML;
// 		echo "</ul>";
// 	echo "</nav>";
// }
?>

<?php
if( have_rows('slideshow') ):
?>
<div class="tab-content">
<?php
	$tabCounter = 1;
	
	// while ( have_rows('slideshow') ) : the_row();
	// 	if ($tabCounter == 1) {
	// 		$tabActiveClass = "active";
	// 	} else {
	// 		$tabActiveClass= "";
	// 	}
		$tabActiveClass = "active";
?>
			<div class="tab-pane <?php echo $tabActiveClass; ?>" id="tab-<?php echo $tabCounter; ?>">

				<div id="article-slideshow-<?php echo $tabCounter; ?>" class="article-slideshow carousel slide" data-ride="carousel" data-interval="false">
					<!-- Wrapper for slides -->
					<div class="carousel-inner">
						<?php 
						$slideCounter = 1;
						$totalSlides = count( get_field('slideshow') );
						while(has_sub_field('slideshow')):

							if ($slideCounter == 1) {
								$activeClass = "active";
								
							} else {
								$activeClass= "";
							}
						?>
							<?php $slideLayout = get_sub_field('slide_layout');
								if($slideLayout == 0)
									$slideLayout = "portrait";
								else
									$slideLayout = "landscape";
							 ?>
							<div class="item <?php echo $slideLayout; ?> <?php echo $activeClass; ?>">
								<figure class="slideshow-image" itemprop="thumbnailUrl">
									<?php
										$slideImage = get_sub_field('slide_img');
										$slideImageLargeURL = $slideImage['sizes']['large'];
										if ($slideLayout == "landscape") {
											$slideImageURL = $slideImage['sizes']['slideshow-landscape'];
										} else {
											$slideImageURL = $slideImage['sizes']['slideshow-portrait'];
										}
										$slideImageURL = getImgServerUrl ($slideImageURL);
										$slideImageLargeURL = getImgServerUrl ($slideImageLargeURL);
									?>
									<a class="action-expand" data-toggle="modal" data-target="#expand-<?php echo $tabCounter; ?>-<?php echo $slideCounter; ?>"><img src="<?php echo $slideImageURL; ?>" alt="slideimg"></a>
									<figcaption class="slideshow-image-actions">
										<a class="action-pinterest" href="http://www.pinterest.com/pin/create/button/?media=<?php echo $slideImageLargeURL; ?>&amp;description=<?php the_title(); ?>&amp;url=<?php the_permalink() ?>#slide-<?php echo $slideCounter;?>">P</a>
										<a class="action-expand" data-toggle="modal" data-target="#expand-<?php echo $tabCounter; ?>-<?php echo $slideCounter; ?>">+</a>
										<span class="slideshow-counter"><?php echo $slideCounter; ?> <?php _e( " / ", 'harpers-bazaar' ); ?> <?php echo $totalSlides; ?></span>
									</figcaption>
								</figure>
								<div class="carousel-caption">
									<?php echo bz_the_content_filter(get_sub_field("slide_description")); ?>
								</div>
								<!-- Modal -->
								<div class="modal fade" id="expand-<?php echo $tabCounter; ?>-<?php echo $slideCounter; ?>" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
									<div class="modal-dialog">
										<div class="modal-content">
											<div class="modal-body">
												<button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
												<img src="<?php echo $slideImageLargeURL; ?>"></img>
											</div>
										</div>
									</div>
								</div>
								<!-- /Modal -->
							</div>
						<?php 
						$slideCounter++;
						endwhile;
						?>
					</div>

					<?php if ($slideCounter > 2): ?>
					<!-- Controls -->
					<a class="left carousel-control" href="#article-slideshow-<?php echo $tabCounter; ?>" data-slide="prev">Previous</a>
					<a class="right carousel-control" href="#article-slideshow-<?php echo $tabCounter; ?>" data-slide="next">Next</a>
					<?php endif; ?>

				</div> <!-- .article-slideshow -->
			</div> <!-- .tab-pane -->

<?php
		//endif;
	$tabCounter++; 
	// endwhile; 
?>

</div> <!-- .tab-content -->
<?php endif; ?>