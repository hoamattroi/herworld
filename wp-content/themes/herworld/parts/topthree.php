<section class="top-three">
	<div class="container clearfix">
		<div class="row">
			<h1 class="barred-heading"><span><?php _e("What's now", 'harpersbazaar'); ?></span></h1>

			<?php
			$counter = 1;
			while(has_sub_field('top_three_articles', 'option')):
				
				$article = get_sub_field('article');
				$articleID = $article[0]->ID;
				$articleURL = get_permalink($articleID);
				$articleDate = $article[0]->post_date;
				$articleDate = strtotime($articleDate);
				$articleDate = date("d-m-Y", $articleDate);
				$articleCategory = get_the_category($articleID);
				//print_r($articleCategory);
				$articleCategoryName = $articleCategory[0]->name;
				$articleCategoryID = $articleCategory[0]->cat_ID;
				$articleCategoryLink = get_category_link($articleCategoryID);
				//print_r($article);

				$fieldHeading = get_sub_field('heading');
				if ($fieldHeading != "") {
					$heading = $fieldHeading;
				} else {
					$heading = $article[0]->post_title;
				}

				$fieldThumbnail = get_sub_field('thumbnail');
				if ($fieldThumbnail != "") {
					$thumbnail = $fieldThumbnail['sizes']['square-360'];
					$thumbnail = "<img itemprop=\"thumbnailUrl\" src=\"$thumbnail\" alt=\"\" />";
				} else {
					$thumbnail = get_the_post_thumbnail($articleID, 'square-360');
				}
			?>
				<article itemscope itemtype="http://schema.org/NewsArticle">
					<figure class="row">
						<div class="thumb-wrap">
							<div class="top-three-thumb" itemprop="thumbnailURL">
								<a href="<?php echo $articleURL; ?>"><?php echo $thumbnail; ?></a>
							</div>
						</div>
						<!-- <span class="top-three-number"><?php echo $counter; ?></span> -->
						<figcaption>
							<a href="<?php echo $articleURL; ?>" class="imgTitle"><?php echo $heading; ?></a>
							<div class="date"><time><?php echo $articleDate; ?></time> trong <a href="<?php echo $articleCategoryLink; ?>"><?php echo $articleCategoryName; ?></a></div>
						</figcaption>
					</figure>
				</article>
			<?php
				$counter++;
			endwhile; 
			?>

		</div>
	</div>
</section>