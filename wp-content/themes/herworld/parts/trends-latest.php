<h1 class="category-title barred-heading">
	<span><?php _e("Xu Hướng", "harpersbazaar"); ?></span>
</h1>
<h2 class="barred-heading-sub"><?php _e("What's In, What's Out", "harpersbazaar"); ?></h2>

<div class="trends-latest trends-in">
	<?php 
	$the_query = new WP_Query(
		array (
			'post_type' => array ('post', 'fashion-trends'),
			'trend_status' => 'in',
			'showposts' => '4',
			'orderby' => 'modified'
		)
	);
	if ($the_query->have_posts()) : while ($the_query->have_posts()) : $the_query->the_post(); 
	?>
	<div class="trend">
		<a href="<?php the_permalink(); ?>">
			<figure>
				<?php the_post_thumbnail('square-360'); ?>
				<figcaption><?php the_title(); ?></figcaption>
			</figure>
		</a>
	</div>
	<?php endwhile; endif; ?>
	<div class="trend-direction">In</div>
</div>

<div class="trends-latest trends-out">
	<?php 
	$the_query = new WP_Query(
		array (
			'post_type' => array ('post', 'fashion-trends'),
			'trend_status' => 'out',
			'showposts' => '4',
			'orderby' => 'modified'
		)
	);
	if ($the_query->have_posts()) : while ($the_query->have_posts()) : $the_query->the_post(); 
	?>
	<div class="trend" itemscope itemtype="http://schema.org/NewsArticle">
		<a href="<?php the_permalink(); ?>" itemprop="url">
			<figure>
				<?php the_post_thumbnail('square-360'); ?>
				<figcaption itemprop="headline"><?php the_title(); ?></figcaption>
			</figure>
		</a>
	</div>
	<?php endwhile; endif; ?>
	<div class="trend-direction">Out</div>
</div>