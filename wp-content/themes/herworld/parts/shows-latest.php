<?php global $current_season; 
$current_season = get_term_by('name', $current_season, 'seasons');
$termChildren = get_term_children($current_season->term_id, 'seasons');

$today_date = date('Ymd');


$postArgs = array(
			'meta_key' => 'runway_date',
    		'orderby' => 'meta_value',
			'order' => 'DESC',
			'post_status' => 'publish',
			'post_type' => 'runway', 
			'posts_per_page' => 1,
			'tax_query' => array(
	            array(
	                'taxonomy' => 'seasons',
	                'field' => 'id',
	                'terms' => $current_season->term_id
		            ),
	            array(
	                'taxonomy' => 'seasons',
	                'field' => 'id',
	                'terms' => $termChildren,
	                'operator' => 'NOT IN'
	            )
	        ),
	        'meta_query' => array(
				array(
					'key' => 'runway_date',
					'value' => $today_date,
    				'compare' => '<=',
    				'type' => 'date'
				)
			)       
		);
		$posts = get_posts($postArgs);


?>

<div class="shows-latest">
	<div class="shows-featured">
		<?php foreach( $posts as $post ) :
			setup_postdata($post);
		echo get_the_post_thumbnail($post->ID, 'runway_thumbnail');
		endforeach; ?>
	</div>
	<div class="shows-cat">
		<ul>
			<li><a id="last_shows_link" href="#"><?php _e("Latest Shows", "harpersbazaar"); ?></a></li>
			<li><a id="all_shows_link" href="#"><?php _e("All Shows", "harpersbazaar"); ?></a></li>
		</ul>
	</div>
	<div id="last_shows" class="shows-latest-list list_active">
		<ul>
		<?php 
		
		wp_reset_postdata();
		
		$postArgs = array(
			'orderby' => 'post_date',
			'order' => 'DESC',
			'post_status' => 'publish',
			'post_type' => 'runway', 
			'posts_per_page' => 20,
			'tax_query' => array(
	            array(
	                'taxonomy' => 'seasons',
	                'field' => 'id',
	                'terms' => $current_season->term_id
		            ),
	            array(
	                'taxonomy' => 'seasons',
	                'field' => 'id',
	                'terms' => $termChildren,
	                'operator' => 'NOT IN'
	            )
	        ),
			'meta_query' => array(
				array(
					'key' => 'runway_date',
					'value' => $today_date,
    				'compare' => '<=',
    				'type' => 'date'
				)
			) 
		);
		$posts = get_posts($postArgs);
		foreach( $posts as $post ) :
			setup_postdata($post);
		?>
			<li><a href="<?php the_permalink() ?>"><?php the_title(); ?></a></li>
		<?php
		endforeach;
		wp_reset_postdata();
		?>
		</ul>
	</div>
	<div id="all_shows" class="shows-latest-list">
		<ul>
		<?php 
		
		$postArgs = array(
			'tax_query' => array(
	            array(
	                'taxonomy' => 'seasons',
	                'field' => 'id',
	                'terms' => $current_season->term_id
		            ),
	            array(
	                'taxonomy' => 'seasons',
	                'field' => 'id',
	                'terms' => $termChildren,
	                'operator' => 'NOT IN'
	            )
	        ),
			'orderby' => 'post_date',
			'order' => 'DESC',
			'post_status' => 'publish',
			'post_type' => 'runway',  
			'posts_per_page' => -1,
		);
		$posts = get_posts($postArgs);
		
		foreach( $posts as $post ) :
			setup_postdata($post);
		?>
			<li><a href="<?php the_permalink() ?>"><?php the_title(); ?></a></li>
		<?php
		endforeach;
		wp_reset_postdata();
		?>
		</ul>
	</div>
</div>