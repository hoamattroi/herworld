<?php

add_action('init', 'cptui_register_my_cpt_fashion_trends');
function cptui_register_my_cpt_fashion_trends() {
register_post_type('fashion-trends', array(
'label' => 'Xu Hướng',
'description' => '',
'public' => true,
'show_ui' => true,
'show_in_menu' => true,
'capability_type' => 'post',
'map_meta_cap' => true,
'hierarchical' => false,
'rewrite' => array('slug' => 'xu-huong', 'with_front' => true),
'query_var' => true,
'has_archive' => true,
'taxonomies' => array('post_tag'),
'supports' => array('title','editor','excerpt','revisions','thumbnail','author'),
//'taxonomies' => array('category'),
'labels' => array (
  'name' => 'Xu Hướng',
  'singular_name' => 'Xu Hướng',
  'menu_name' => 'Xu Hướng',
  'add_new' => 'Add Trend',
  'add_new_item' => 'Add New Trend',
  'edit' => 'Edit',
  'edit_item' => 'Edit Trend',
  'new_item' => 'New Trend',
  'view' => 'View Trend',
  'view_item' => 'View Trend',
  'search_items' => 'Search Xu Hướng',
  'not_found' => 'No Xu Hướng Found',
  'not_found_in_trash' => 'No Xu Hướng Found in Trash',
  'parent' => 'Parent Xu Hướng',
)
) ); }

add_action('init', 'cptui_register_my_taxes_trend_status');
function cptui_register_my_taxes_trend_status() {
register_taxonomy( 'trend_status',array (
  0 => 'fashion-trends',
),
array( 'hierarchical' => true,
  'label' => 'Trend Status',
  'show_ui' => true,
  'query_var' => true,
  'show_admin_column' => false,
  'labels' => array (
  'search_items' => '',
  'popular_items' => '',
  'all_items' => '',
  'parent_item' => '',
  'parent_item_colon' => '',
  'edit_item' => '',
  'update_item' => '',
  'add_new_item' => '',
  'new_item_name' => '',
  'separate_items_with_commas' => '',
  'add_or_remove_items' => '',
  'choose_from_most_used' => '',
)
) ); 
}

?>