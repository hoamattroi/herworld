// Avoid `console` errors in browsers that lack a console.
(function() {
  var method;
  var noop = function() {};
  var methods = [
    'assert', 'clear', 'count', 'debug', 'dir', 'dirxml', 'error',
    'exception', 'group', 'groupCollapsed', 'groupEnd', 'info', 'log',
    'markTimeline', 'profile', 'profileEnd', 'table', 'time', 'timeEnd',
    'timeStamp', 'trace', 'warn'
  ];
  var length = methods.length;
  var console = (window.console = window.console || {});

  while (length--) {
    method = methods[length];

    // Only stub undefined methods.
    if (!console[method]) {
      console[method] = noop;
    }
  } 

}());
$.fn.enterKey = function (fnc) {
    return this.each(function () {
        $(this).keypress(function (ev) {
            var keycode = (ev.keyCode ? ev.keyCode : ev.which);
            if (keycode == '13') {
                fnc.call(this, ev);
            }
        })
    })
}


var LAURIER = LAURIER || {};
$win = $(window);
$doc = $(document);

/* ==================================================
    initUploadStep1
================================================== */
LAURIER.initUploadStep1 = function() {
    var formUpload = $('#form-upload-avatar');
    if(formUpload.length) {
      var btnChooseImg = $('#btn-upload-avatar'),
          btnChangeImg = $('#btn-change-img'),
          inputFile = $("#imgInp", formUpload),
          imgPreview = $('#img-preview'),
          errMsg = $('#error-message');

      function readURL(input) {
          if (input.files && input.files[0]) {
              var reader = new FileReader();

              reader.onload = function (e) {
                  imgPreview.attr('src', e.target.result).show();
                  btnChooseImg.hide();

                  var image  = new Image();
                  image.addEventListener("load", function () {
                    var fileSize = input.files[0].size/1024; // KB
                    if(fileSize > 5000) {
                      errMsg.text('Hình tải lên phải < 5Mb');
                      validForm = false;
                    }
                    else {
                      validForm = true;
                    }
                  });
                  image.src = e.target.result;
              }

              reader.readAsDataURL(input.files[0]);
          }
          else {
            imgPreview.hide();
            btnChooseImg.show();
          }
      }

      var allowExtension = ['jpeg', 'jpg', 'png'],
          validForm = false;
      inputFile.change(function(){
          var fileExtension = inputFile.val().split('.').pop();
          if(allowExtension.indexOf(fileExtension) != -1) {
            readURL(this);
            btnChangeImg.show();
            errMsg.text('');
            validForm = true;
          }
          else {
            btnChangeImg.hide();
            imgPreview.hide();
            btnChooseImg.show();
            validForm = false;
          }
      });

      btnChooseImg.on('click.chooseImg', function(e) {
        e.preventDefault();
        inputFile.click();
      })

      btnChangeImg.on('click.changeImg', function(e) {
        e.preventDefault();
        inputFile.click();
      })

      $('.gbtn-continue').on('click.upload', function(e) {
        validForm ? formUpload.submit() : e.preventDefault();
        if(!validForm) {
          e.preventDefault();
          if(!inputFile.val()) {
            errMsg.text('Tải lên hình ảnh');
          }
        }
        else {
          formUpload.submit();
        }
      })
    }
}


/* ==================================================
    initUploadStep2
================================================== */
LAURIER.initUploadStep2 = function() {
    if($('.share-template').length) {
      var btnContinue = $('.gbtn-continue'),
          activeTab = $('.show-template');

      btnContinue.on('click.continue', function(e) {
          e.preventDefault();
          e.stopPropagation();
          html2canvas(activeTab[0], {
            onrendered: function(canvas) {
                var shareImage = canvas.toDataURL();
                $.ajax({
                  method: "POST",
                  url: '/ajax.php',
                  data: {
                      'action': 'uploadLaurierInfo',
                      'image': shareImage
                  },
                  success:function(result){
                      try {
                          // console.log(result);
                          var jsonRes = $.parseJSON(result); 
                          if(jsonRes.code == 1) {
                              window.location.replace(jsonRes.url);
                          }
                      }
                      catch(err) {console.log('err', err)}
                  },
                  error: function(err) {
                    console.log('err', err);
                  }
                })
          }
        });
      }); 
    }
}

LAURIER.updateUserInfo = function() {
    if($('#form-update-avatar').length) {
      var btnContinue = $('#save-info'),
          activeTab = $('.show-template');

      btnContinue.on('click.continue', function(e) {
          e.preventDefault();
          e.stopPropagation();
          html2canvas(activeTab[0], {
            onrendered: function(canvas) {
                var shareImage  = canvas.toDataURL();
                var user_id     = $('#form-update-user-id').val();
                $.ajax({
                  method: "POST",
                  url: '/ajax.php',
                  data: {
                      'action': 'update_user_info',
                      'image': shareImage,
                      'id' : user_id,
                  },
                  success:function(result){
                      try {
                          console.log(result);
                          var jsonRes = $.parseJSON(result); 
                          if(jsonRes.code == 1) {
                              window.location.replace(jsonRes.url);
                          }
                      }
                      catch(err) {console.log('err', err)}
                  },
                  error: function(err) {
                    console.log('err', err);
                  }
                })
          }
        });
      }); 
    }
}


/* ==================================================
    initUploadStep2
================================================== */
LAURIER.initUploadStep3 = function() {
    var form = $('#form-edit-info');
    form.validate({
      rules: {
        'step3-name':{
          required: true,
        },
        'step3-phone':{
          required: true,
          number: true,
          minlength: 8,
          maxlength: 11,
        },
        'step3-cmnd':{
          required: true,
          number: true,
          minlength: 9,
          maxlength: 13,
        }
      },
      messages :{
        'step3-name':{
          required: "Bắt buộc nhập!",
        },
        'step3-phone':{
          required: "Bắt buộc nhập!",
          number: "Phải là số!",
          minlength: "Sđt quá ngắn!",
          maxlength: "Sđt quá dài!",
        },
        'step3-cmnd':{
          required: "Bắt buộc nhập!",
          number: "Phải là số!",
          minlength: "CMND quá ngắn!",
          maxlength: "CMND quá dài!",
        }
      },
      submitHandler: function(form) {
        var form_3 = $('#form-edit-info');
        $.ajax({
            method: "POST",
            url: '/ajax.php?action=uploadLaurierFinal&' + form_3.serialize(), 
            success:function(result){
                try {
                    // console.log(result);
                    var jsonRes = $.parseJSON(result); 
                    if(jsonRes.code == 1) {
                        window.location.replace(jsonRes.url);
                    }
                }
                catch(err) {console.log('err', err)}
            },
            error: function(err) {
              console.log('err', err);
            }
          })
      }
    }); 
}

/* ==================================================
    initUploadStep2
================================================== */
LAURIER.selectTemplateStep2 = function() {
    var selectTab = $('.instock li a'),
        showTemplate = $('.share-template .show-template'),
        imgBackground = $('<img src="" style="position:absolute; top:0; left:0; width:100%; height:100%; z-index:10;">').appendTo(showTemplate);

    selectTab.on('click', function(e) {
        e.preventDefault();
        e.stopPropagation();
        $(this).closest('ul').find('li').removeClass("active");
        $(this).parent().addClass("active");
        var t = $(this).attr('href'),
            src = $(this).data('src');

        showTemplate.removeClass('show01 show02 show03 show04').addClass(t);
        imgBackground.attr('src', src)
    });

    selectTab.parent().filter('.active').find('a').click();
};

/* ==================================================
    initUploadStep2
================================================== */
LAURIER.search = function() {
  $("#search-laurier").enterKey(function (e) {
      e.preventDefault();
      e.stopPropagation();
      if($("#search-laurier").val() == "") {
        alert("Nhập từ khóa tìm kiếm");
        return false;
      }
      var baseHref = window.location.href.split('?');
      var url      = baseHref[0] + "?search=" + $("#search-laurier").val();
      // similar behavior as an HTTP redirect
      window.location.replace(url);
      return false;
  })

  $("#search-laurier-btn").click(function (e) {
      e.preventDefault();
      e.stopPropagation();
      if($("#search-laurier").val() == "") {
        alert("Nhập từ khóa tìm kiếm");
        return false;
      }
      var baseHref = window.location.href.split('?');
      var url      = baseHref[0] + "?search=" + $("#search-laurier").val();
      // similar behavior as an HTTP redirect
      window.location.replace(url);
      return false;  
  });
}
/**
 * Created by duyenpt on 11/19/16.
 */
$(document).ready(function(){
    $('.nav-toggle').click(function(){
        $('body').toggleClass('show-nav');
    });

    LAURIER.initUploadStep1();
    // LAURIER.initUploadStep2();
    LAURIER.selectTemplateStep2();

    LAURIER.initUploadStep3();
    LAURIER.search();

    LAURIER.updateUserInfo();
    // LAURIER.moveImage();
});
 

 $(document).ready(function(){
    // validation
    if($.validator) {
      $.validator.setDefaults({
        highlight: function(element) {
          $(element).addClass('error').closest('.selecter').addClass('select-error');
        },
        unhighlight: function(element) {
          $(element).removeClass('error').closest('.selecter').removeClass('select-error');
        },
        errorClass: 'error'
      });

      $('form').each(function() {
        var form = $(this),
            formID = form.attr('id'),
            configValidate = {
              messages: {
                  'step1-name': {
                      required: "Bắt buộc nhập!"
                  },
                  'step1-job': {
                      required: "Bắt buộc nhập!"
                  },
                  'step1-style': {
                      required: "Bắt buộc nhập!"
                  },
                  'step3-name': {
                      required: "Bắt buộc nhập!"
                  },
                  'step3-phone': {
                      required: "Bắt buộc nhập!"
                  },
                  'step3-cmnd': {
                      required: "Bắt buộc nhập!"
                  }
              },
              errorPlacement: function(error, element) {
                var elm = $(element),
                    selectWrap = elm.closest('.selecter');
                if(selectWrap.length) {
                  selectWrap.append(error);
                }
                else {
                  error.insertAfter(element);
                }
              }
            };
        if(!form.data('no-validate-js')) {
          form.validate(configValidate);
        }
      })
      $('form').on('submit.setFlag', function(e) {
        var form = $(this);
        form.addClass('show-error');
      });
    }

    // $( ".show-template" ).click(function() {
    //   $( ".show-template" ).mousemove(function(e){
    //       var amountMovedX = (e.pageX * -1 / 6);
    //       var amountMovedY = (e.pageY * -1 / 6);
    //       $("#imgUpload").css(
    //         top, parseInt(amountMovedX) + 'px',
    //         right, parseInt(amountMovedX) + 'px',
    //       );
    //   });
    // });
 });

  function moveLeftImage(){
    var leftPos = $("#imgUpload").css("right");
    if ($(".show-template").hasClass('show03')) {
      $("#imgUpload").css({
        width: $(".imgUpload").css("width"),
        left: parseInt( $("#imgUpload").css("left") ) - 10 + 'px',
      });
    }else{
      $("#imgUpload").css({
        width: $("#imgUpload").css("width"),
        right: parseInt(leftPos) + 10 + 'px'
      });
    }
    return false;
  }

  function moveRightImage(){
    var rightPos   = $("#imgUpload").css("right");
    if ($(".show-template").hasClass('show03')) {
      $("#imgUpload").css({
        width: $(".imgUpload").css("width"),
        left: parseInt( $("#imgUpload").css("left") ) + 10 + 'px',
      });
    }else{
      $("#imgUpload").css({
        width: $(".imgUpload").css("width"),
        right: parseInt(rightPos) - 10 + 'px'
      });
    }
    return false;
  }

  function moveTopImage(){
    var imgOri = document.getElementById("imgUpload");
    var topPos = $("#imgUpload").css("top");
    $("#imgUpload").css({
      width: $("#imgUpload").css("width"),
      top: parseInt(topPos) - 10 + 'px'
    });
    return false;
  }

  function moveBottomImage(){
    var imgOri = document.getElementById("imgUpload");
    var botPos = $("#imgUpload").css("top");
    $("#imgUpload").css({
      width: $("#imgUpload").css("width"),
      top: parseInt(botPos) + 10 + 'px'
    });
    return false;
  }

  function zoomInImage(){
    var imgOri = document.getElementById("imgUpload");
    if ($(".show-template").hasClass('show03')) {
      $("#imgUpload").css({
        width: parseInt( $("#imgUpload").css("width") ) + 20 +"px",
      });
      $("#imgUpload").find('.img-responsive').css({
        'max-height': parseInt( $("#imgUpload").find('.img-responsive').css("max-height") ) + 2 + '%',
        height: parseInt( $("#imgUpload").find('.img-responsive').css("height") ) + 20 + 'px',
      });
    }else{
      $("#imgUpload").find('.img-responsive').css({
        'max-width': parseInt( $("#imgUpload").find('.img-responsive').css("max-width") ) + 2 + '%',
      });
    }
    return false;
  }

  function zoomOutImage(){
    var imgOri = document.getElementById("imgUpload");
    if ($(".show-template").hasClass('show03')) {
      $("#imgUpload").css({
        width: parseInt( $("#imgUpload").css("width") ) - 20 +"px",
      });
      $("#imgUpload").find('.img-responsive').css({
        'max-height': parseInt( $("#imgUpload").find('.img-responsive').css("max-height") ) - 2 + '%',
        height: parseInt( $("#imgUpload").find('.img-responsive').css("height") ) - 20 + 'px',
      });
    }else{
      $("#imgUpload").find('.img-responsive').css({
        'max-width': parseInt( $("#imgUpload").find('.img-responsive').css("max-width") ) - 2 + '%',
      });
    }
    return false;
  }

