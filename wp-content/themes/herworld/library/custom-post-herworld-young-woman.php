<?php 
	$forms = RGFormsModel::get_forms( null, 'title' );
	if(!empty($forms)){
		$title_form = null;
		foreach($forms as $form){
			if($form->id == $idCurrentForm){
				$title_form = $form->title;
			}
		}
	}
?>
<div class="row">
	<div class="col-md-6 vote-img">
		<?php echo get_the_post_thumbnail( get_the_ID(), 'slideshow-portrait' ); ?>
		<?php if ($idCurrentForm != null): ?>
			<div class="button-vote">
				<p><?php if(!empty($title_form)) echo $title_form; ?></p>
				
<!--<a id='inline-box' href="#inline_content" style="text-decoration:none;">
					<img src="<?php echo get_stylesheet_directory_uri().'/library/images/hywa-vote.png'; ?>" alt="image vote" />
				</a>-->
				<span class="vote-total">
				<?php 

/*
					if ($entry_total != 0){
						$in_total = ($entry_total < 10) ? '0'.$entry_total : $entry_total;
						echo $in_total; 
					} else {
						echo "0";
					}
*/
				?>
				</span>
			</div>
		<?php endif;?>
	</div>
	
	<div class="col-md-6">
		<article class="article-primary" id="post-<?php the_ID();?>" <?php post_class('clearfix'); ?> role="article" itemscope itemtype="http://schema.org/Article">
			<header class="article-header">
			<?php the_category(); ?>
				<h1 class="entry-title single-title single-custom-vote" itemprop="headline"><?php the_title(); ?></h1>
				<?php
					$myExcerpt = get_the_excerpt();
					$tags      = array("<p>","</p>");
					$myExcerpt = str_replace($tags, "", $myExcerpt);
				?>
				<p class="standfirst"><?php echo $myExcerpt; ?></p>
				<div class="byline vcard clearfix">
					<div class="authorship">
					<?php
						$category = get_the_category();
						if (strpos($category[0]->slug, "bai-du-thi") === false) {
						    _e("Do", 'harpersbazaar');
					?>
						<span class="author" itemprop="author"><?php the_author_posts_link(); ?></span>
					<?php
						}
					?>
					<?php _e("đăng ngày", 'harpersbazaar'); ?>
						<time class="time" pubdate itemprop="datePublished"><?php the_date('d-m-Y'); ?></time>
					</div>
					<?php if (!is_preview()) {
						?>
						<div class="share">
							<span><?php _e("chia sẻ trên", 'harpersbazaar'); ?></span>
								<!-- a class="social-icon social-icon-zing-dark" name="zm_share" type="text" title="Chia sẻ lên Zing Me">
							<?php _e('Share on Zing Me', 'harpersbazaar');?></a -->
								<a class="social-icon social-icon-gplus-dark" href="https://plus.google.com/share?url=<?php the_permalink(); ?>" target="_blank"><?php _e('Chia sẻ Google Plus', 'harpersbazaar'); ?></a>
									
								<a class="social-icon social-icon-facebook-dark" href="https://www.facebook.com/sharer/sharer.php?u=<?php the_permalink(); ?>" target="_blank"><?php _e('Chia sẻ Facebook', 'harpersbazaar'); ?></a>
						</div>
							<?php
							}
						?>
				</div>
			</header> <!-- end article header -->
		</article>

		<div class="vote-success">
			<div class="vote-success-in">
				<span class="flash-message"></span>
			</div>
		</div>
	</div>
</div>

<!-- Show Popup -->
<?php
	$get_cookie 	= $_COOKIE['hywa_cate'];
	$arr_cookie 	= explode(",", $get_cookie);
	$get_cate_post 	= $category[0]->cat_ID;
	$get_name_cate 	= $category[0]->name;
	$all_cate 		= $get_cate_post.','.$get_cookie;
?>

<div style='display: none'>
	<div id='inline_content' style='padding:10px; background:#fff;'>
		<?php 
			if(in_array($get_cate_post, $arr_cookie)){
				echo 	'<div class="note-vote">
							<span>Bạn đã bình chọn cho một nhân vật thuộc nhóm: <b style="text-transform: uppercase; color: #F37021;">'.$get_name_cate.'.</b> Không được bình chọn nhân vật nào cho nhóm này nữa!</span>
						</div>';
			}
			else{
				echo do_shortcode('[gravityform id="'.$idCurrentForm.'" title="false" description="false"]');
			}
		?>
		<input type="hidden" id="form-cate-id" value="<?php echo $all_cate; ?>">
		<input type="hidden" id="form-id" value="<?php echo $idCurrentForm; ?>">
		<input type="hidden" id="fomr-name-vote" value="<?php echo $title_form; ?>">
	</div>
</div>