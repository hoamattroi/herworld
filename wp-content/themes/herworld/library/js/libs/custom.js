
$(document).ready(function(){

	// AJAX LOAD SUB VOTE
	
/*var formId = $("#form-id").val();
	$.ajax({
		url: "/ajax.php",
		data: {"action": "load_total_vote_hywa", "form_id": formId},
		success: function(data){
			$(".button-vote .vote-total").html(data);
		}
	});
*/
	// SHOW MESSAGE WHEN TO SUBMIT FORM SUCCESSFULLY
	var getCookieForm = getCookie("hywa_form_id");
	var formNameTitle = $("#fomr-name-vote").val();
	if(getCookieForm && getCookieForm == formId){
		$("div.vote-success").css("display", "block");
		$(".vote-success .vote-success-in .flash-message").html("Bạn đã bình chọn thành công cho nhân vật: "+formNameTitle+". Mời bạn tiếp tục bình chọn cho các nhân vật khác.");
	}

	// SET TIMEOUT MESSAGE VOTE SUCCESSFULLY
	setTimeout(function(){
        $('.vote-success').fadeOut('fast');
    }, 10000);

	// GET WIDTH BROWSER TO FIX WIDTH POPUP VOTE
	// var widthBrowser = window.innerWidth;
	// if(widthBrowser > 768){
	// 	$("#inline_content").css("padding", "20px 30px");
	// 	$("#inline-box").colorbox({inline:true, width:"600px"});
	// }
	// else{
	// 	$('.hwya_date label').css("width", "100%");
	// 	$('.hwya_gender label').css("width", "100%");
	// 	$('.hwya_name input, .hwya_cmnd input, .hwya_phone input, .hwya_email input, .hwya_address input').css("width", "100%");
	// 	$("#inline-box").colorbox({inline:true, width:"90%"});
	// }
	
	var gender 		= $('form.submit-vote ul li.hwya_gender input').attr("checked", true).val();
	if($('form.submit-vote ul li.hwya_gender input').on("change", function(){
		gender 		= $(this).val();
	}));
	$("form.submit-vote input[type='submit']").on('click', function(event){

		// SAVE COOKIE FOR HERWORD YOUNG WOMAN ACHIEVER
		var cate_id 	= $("#form-cate-id").val();
		var fullname 	= $('form.submit-vote ul li.hwya_name input').val();
		var old			= $('form.submit-vote ul li.hywa_old input').val();
		var phone 	 	= $('form.submit-vote ul li.hwya_phone input').val();
		var address 	= $('form.submit-vote ul li.hywa_address input').val();
		var city 		= $('form.submit-vote ul li.hywa_city input').val();
		var form_id     = $("#form-id").val();
		var address 	= $('form.submit-vote ul li.hywa_address input').val();
		var city 		= $('form.submit-vote ul li.hywa_city input').val();
		
		// VALIDATE EMAIL ADDRESS
		var email    	= $('.submit-vote .hwya_email input').val();
		var email_regex = /^[a-zA-Z0-9._-]+@[a-zA-Z0-9.-]+\.[a-zA-Z]{2,4}$/i;

		if(email != ""){
			$.ajax({
				url: "/ajax.php",
				data: {"action": "check_email_form_hywa", "form_id": form_id, "email_address": email},
				success: function(data){
					if(data == 1){
						$('.submit-vote .hwya_email div .hywa-form-error').remove();
						$('.submit-vote .hwya_email div').prepend("<span class='hywa-form-error'>e-mail đã tồn tại</span>");
						event.preventDefault();
					}
				}
			});
		}
		if(fullname == "" || fullname.length < 5){
			$('.submit-vote .hwya_name div .hywa-form-error').remove();
			$('.submit-vote .hwya_name div').prepend("<span class='hywa-form-error'>trường bắt buộc và họ tên phải trên 4 ký tự</span>");
		}
		if(phone == "" || isNaN(phone) == true || phone.length < 10 || phone.length > 11){
			$('.submit-vote .hwya_phone div .hywa-form-error').remove();
			$('.submit-vote .hwya_phone div').prepend("<span class='hywa-form-error'>trường bắt buộc và điện thoại phải từ 10 đến 11 số</span>");
		}
		if(email == "" || !email_regex.test(email)){
			$('.submit-vote .hwya_email div .hywa-form-error').remove();
			$('.submit-vote .hwya_email div').prepend("<span class='hywa-form-error'>trường bắt buộc hoặc định dạng e-mail chưa hợp lệ</span>");
		}
		if(old == "" || isNaN(old) == true || parseInt(old, 10) < 13){
			$('.submit-vote .hywa_old div .hywa-form-error').remove();
			$('.submit-vote .hywa_old div').prepend("<span class='hywa-form-error'>trường bắt buộc hoặc tuổi phải là số và trên 12 tuổi</span>");
		}
		if(address == ""){
			$('.submit-vote .hywa_address div .hywa-form-error').remove();
			$('.submit-vote .hywa_address div').prepend("<span class='hywa-form-error'>trường bắt buộc</span>");
		}
		if(city == ""){
			$('.submit-vote .hywa_city div .hywa-form-error').remove();
			$('.submit-vote .hywa_city div').prepend("<span class='hywa-form-error'>trường bắt buộc</span>");
		}
		// function check_mail(){
		// 	if(email != ""){
		// 		$.ajax({
		// 			url: "/ajax.php",
		// 			data: {"action": "check_email_form_hywa", "form_id": form_id, "email_address": email},
		// 			success: function(data){
		// 				if(data == 1){
		// 					$('.submit-vote .hwya_email div').prepend("<span class='hywa-form-error'>e-mail đã tồn tại</span>");
		// 					return false;
		// 				}
		// 			}
		// 		});
		// 	}
		// }
		// if(check_mail() == 1){
		// 	$('.submit-vote .hwya_email div').prepend("<span class='hywa-form-error'>e-mail đã tồn tại</span>");
		// }
		if(fullname == "" || phone == "" || isNaN(phone) == true || email == "" || !email_regex.test(email) || old == "" || isNaN(old) == true || address == "" || city == "" || phone.length < 10 || phone.length > 11 || fullname.length < 5 || parseInt(old, 10) < 13){
			event.preventDefault();
		}
		else if(fullname != "" && phone != "" && isNaN(phone) == false && email != "" && old != "" && isNaN(old) == false && address != "" && city != "" && (10 <= phone.length <= 11) && fullname.length >= 5 || parseInt(old, 10) > 12){

			// SAVE COOKIE
			document.cookie = "hywa_cate=" +cate_id+ "; path=/";
			document.cookie = "hywa_name=" +fullname+ "; path=/";
			document.cookie = "hywa_phone=" +phone+ "; path=/";
			document.cookie = "hywa_email=" +email+ "; path=/";
			document.cookie = "hywa_address=" +address+ "; path=/";
			document.cookie = "hywa_city=" +city+ "; path=/";
			document.cookie = "hywa_old=" +old+ "; path=/";
			document.cookie = "hywa_form_id=" +form_id+ "; path=/";
			document.cookie = "hywa_gender=" +gender+ "; path=/";
			$("form.submit-vote").submit();
			return true;
		}
	});

	// FUNCTION GET COOKIE
	function getCookie(name) {
  		var value = "; " + document.cookie;
  		var parts = value.split("; " + name + "=");
  		if (parts.length == 2) 
  			return parts.pop().split(";").shift();
	}

	// GET COOKIE INFO AND ADD FORM
	var hywa_cookie_name 	= getCookie("hywa_name");
	var hywa_cookie_phone 	= getCookie("hywa_phone");
	var hywa_cookie_email   = getCookie("hywa_email");
	var hywa_cookie_address = getCookie("hywa_address");
	var hywa_cookie_city    = getCookie("hywa_city");
	var hywa_cookie_old     = getCookie("hywa_old");
	var hywa_cookie_gender	= getCookie("hywa_gender");
	if(hywa_cookie_name){
		$('form.submit-vote ul li.hwya_name input').val(hywa_cookie_name);
	}
	if(hywa_cookie_old){
		$('form.submit-vote ul li.hywa_old input').val(hywa_cookie_old);
	}
	if(hywa_cookie_phone){
		$('form.submit-vote ul li.hwya_phone input').val(hywa_cookie_phone);
	}
	if(hywa_cookie_email){
		$('form.submit-vote ul li.hwya_email input').val(hywa_cookie_email);
	}
	if(hywa_cookie_address){
		$('form.submit-vote ul li.hywa_address input').val(hywa_cookie_address);
	}
	if(hywa_cookie_city){
		$('form.submit-vote ul li.hywa_city input').val(hywa_cookie_city);
	}
	if(hywa_cookie_gender){
		if(hywa_cookie_gender == "Nam"){
			$('form.submit-vote ul li.hwya_gender input[value="Nữ"]').removeAttr("checked");
			$('form.submit-vote ul li.hwya_gender input[value="Nam"]').prop("checked", "checked");
		}
		else{
			$('form.submit-vote ul li.hwya_gender input[value="Nam"]').removeAttr("checked");
			$('form.submit-vote ul li.hwya_gender input[value="Nữ"]').prop("checked", "checked");
		}
	}
});