/*
Bones Scripts File
Author: Eddie Machado

This file should contain any js scripts you want to add to the site.
Instead of calling it in the header or throwing it inside wp_head()
this file will be called automatically in the footer so as not to
slow the page load.

*/

//Share functionality in articles
jQuery(document).ready(function($){
	$("#share_article").click(function(e){
		e.preventDefault();
	});
	
	$("#share_article").mouseenter(function(){
		$(".share-utilities").slideDown();
	});
	$(".share-utilities").mouseover(function(){
		$(this).show();
	});
	$(".share-wrapper").mouseleave(function(){
		$(".share-utilities").slideUp();
	});
	$("#all_shows_link").click(function(event){
		$("#last_shows").hide();
		$("#all_shows").show();
		$("#all_shows_link").addClass('list_active');
		$("#last_shows_link").removeClass('list_active');
		event.preventDefault();
	});
	$("#last_shows_link").click(function(event){
		$("#last_shows").show();
		$("#all_shows").hide();
		$("#all_shows_link").removeClass('list_active');
		$("#last_shows_link").addClass('list_active');
		event.preventDefault();
	});
	
	$('.clicked_month').unbind('click').bind('click', function(event) {
		event.preventDefault();
		var clicked_month;
		var clicked_year;
		var this_date = $(this).attr('data');
		var target = $(this).attr('data-target');
		this_date = this_date.split('-');
		var this_month = this_date[0];
		var this_year = this_date[1];
		if(target=='next'){
			if(this_month == 12){
				clicked_month = 1;
				clicked_year = +this_year+1;
			} else {
				clicked_month = +this_month+1;
				clicked_year = this_year;	
			}		
		}else {
			if(this_month == 1){
				clicked_month = 12;
				clicked_year = this_year-1;
			} else {
				clicked_month = this_month-1;
				clicked_year = this_year;	
			}	
		}	
		
		if(window.location.href.indexOf("?clicked_month")!=-1){
			window.location.href = window.location.href.replace(/&??clicked_month=([^&]$|[^&]*)&?&clicked_year=([^&]$|[^&]*)/i, "?clicked_month=" + clicked_month + "&clicked_year=" + clicked_year);
		} else {
			window.location.href = window.location.href + "?clicked_month=" + clicked_month  + "&clicked_year=" + clicked_year;
		}
	}); 
	
	$('.choose_day').unbind('click').bind('click', function(event) {
	    event.preventDefault();
	    var date = $(this).attr('data-date');
	    var season_id = $(this).attr('data-target');
	    var dt1   = parseInt(date.substring(6));
		var mon1  = parseInt(date.substring(4,6));
		var yr1   = parseInt(date.substring(0,4));
		var date1 = new Date(yr1, mon1-1, dt1);
		var date2 = moment(date1).format("dddd MMMM D, YYYY");
	        
	    $('.schedule-listings').html('');
	    $(".schedule-listings").append("<header>"+date2+"</header>");
    	$(".today").removeClass();
        //$(this).parent().addClass("today");
        $(this).parent().addClass('today');
	    $.ajax({
	      type: 'POST',
	      url: MyAjax.ajaxurl,
	      data: {'action' : 'ajax_request', 'date' : date, 'season_id' : season_id},
	      dataType: 'json',
	      success: function(data) {
	        var posts = data['posts'];
	        var times = data['times'];
	        var locations = data['locations'];
	        var subheadings = data['subheadings'];
	        var linked_runway = data['linked_runway'];
	        var i;
	        var bullet_color;
	        for(i=0;i<posts.length;i++){
	        	if(locations[i] == "Paris"){
					bullet_color = "bullet-blue";
				}else if(locations[i] == "London"){
					bullet_color = "bullet-orange";
				}else if(locations[i] == "Milan"){	
					bullet_color = "bullet-green";
				} else{
					bullet_color = "bullet-red";
				}
	        	$(".schedule-listings").append("<div id='entry_"+i+"' class='schedule-entry'></div>");
	        	if(linked_runway[i]=='yes'){
	        		$("#entry_"+i).html("<div class='schedule-time'><span>"+times[i]+"</span></div><div class='schedule-detail'><h3 class='bullet "+bullet_color+"'><a class='cal_runway_link' href='"+posts[i].guid+"'>"+posts[i].post_title+"</a></h3><p>"+subheadings[i]+"<p></div>");
	    		}else{
	    			$("#entry_"+i).html("<div class='schedule-time'><span>"+times[i]+"</span></div><div class='schedule-detail'><h3 class='bullet "+bullet_color+"'>"+posts[i].post_title+"</h3><p>"+subheadings[i]+"<p></div>");
	    		}
	      	}
	      
	      }
	    });  
	      
	    return false;
	});
	
	
});

// Add Back to top to the site
jQuery(document).ready(function() {
	var offset = 220;
	var duration = 500;
	jQuery(window).scroll(function() {
		if (jQuery(this).scrollTop() > offset) {
			jQuery('#back_to_top_button').fadeIn(duration);
		} else {
			jQuery('#back_to_top_button').fadeOut(duration);
		}
	});
	
	jQuery('#back_to_top_buttonp').click(function(event) {
		event.preventDefault();
		jQuery('html, body').animate({scrollTop: 0}, duration);
		return false;
	});
});

jQuery(document).ready(function($){
// Move inline sharing into the article
	$(".inline-sharing").prependTo(".entry-content p:nth-of-type(4)");

// Move related articles into the article
	// $(".yarpp-related").insertBefore(".entry-content p:nth-last-child(2)");
});

// Scroll to comments in articles
jQuery(document).ready(function($){
	function goToByScroll(id){
		// Remove "link" from the ID
		id = id.replace("link", "");
		// Scroll
		$('html,body').animate({
		scrollTop: $("#facebook-comment").offset().top},
			'slow');
	}

	$("#comment_article").click(function(e) {
		// Prevent a page reload when a link is pressed
		e.preventDefault();
		// Call the scroll function
		goToByScroll($(this).attr("id"));
	});
});


// IE8 ployfill for GetComputed Style (for Responsive Script below)
if (!window.getComputedStyle) {
	window.getComputedStyle = function(el, pseudo) {
		this.el = el;
		this.getPropertyValue = function(prop) {
			var re = /(\-([a-z]){1})/g;
			if (prop == 'float') prop = 'styleFloat';
			if (re.test(prop)) {
				prop = prop.replace(re, function () {
					return arguments[2].toUpperCase();
				});
			}
			return el.currentStyle[prop] ? el.currentStyle[prop] : null;
		};
		return this;
	};
}

// as the page loads, call these scripts
jQuery(document).ready(function($) {

	/*
	Responsive jQuery is a tricky thing.
	There's a bunch of different ways to handle
	it, so be sure to research and find the one
	that works for you best.
	*/
	
	/* getting viewport width */
	var responsive_viewport = $(window).width();
	
	/* if is below 481px */
	if (responsive_viewport < 481) {
	
	} /* end smallest screen */
	
	/* if is larger than 481px */
	if (responsive_viewport > 481) {
		
	} /* end larger than 481px */
	
	/* if is above or equal to 768px */
	if (responsive_viewport >= 768) {
	
		/* load gravatars */
		$('.comment img[data-gravatar]').each(function(){
			$(this).attr('src',$(this).attr('data-gravatar'));
		});
		
	}
	
	/* off the bat large screen actions */
	if (responsive_viewport > 1030) {
		
	}
	
	$('.brand-wp-prev-next ul li').find('a').on('click', function(e){
        e.preventDefault();
        var link = $(this).attr('href');
        alert(link);
        $('.inner_news_feed').fadeOut(100, function(){
            $(this).load(link + ' .inner_news_feed', function() {
                $(this).fadeIn(100);
            });
        });
    });
	
	
	// add all your scripts here
	
 
}); /* end of as page load scripts */


/*! A fix for the iOS orientationchange zoom bug.
 Script by @scottjehl, rebound by @wilto.
 MIT License.
*/
(function(w){
	// This fix addresses an iOS bug, so return early if the UA claims it's something else.
	if( !( /iPhone|iPad|iPod/.test( navigator.platform ) && navigator.userAgent.indexOf( "AppleWebKit" ) > -1 ) ){ return; }
	var doc = w.document;
	if( !doc.querySelector ){ return; }
	var meta = doc.querySelector( "meta[name=viewport]" ),
		initialContent = meta && meta.getAttribute( "content" ),
		disabledZoom = initialContent + ",maximum-scale=1",
		enabledZoom = initialContent + ",maximum-scale=10",
		enabled = true,
		x, y, z, aig;
	if( !meta ){ return; }
	function restoreZoom(){
		meta.setAttribute( "content", enabledZoom );
		enabled = true; }
	function disableZoom(){
		meta.setAttribute( "content", disabledZoom );
		enabled = false; }
	function checkTilt( e ){
		aig = e.accelerationIncludingGravity;
		x = Math.abs( aig.x );
		y = Math.abs( aig.y );
		z = Math.abs( aig.z );
		// If portrait orientation and in one of the danger zones
		if( !w.orientation && ( x > 7 || ( ( z > 6 && y < 8 || z < 8 && y > 6 ) && x > 5 ) ) ){
			if( enabled ){ disableZoom(); } }
		else if( !enabled ){ restoreZoom(); } }
	w.addEventListener( "orientationchange", restoreZoom, false );
	w.addEventListener( "devicemotion", checkTilt, false );
})( this );




/* Normalize heights of carousel slides to prevent content shifting 
jQuery(document).ready(function($){
	function carouselNormalization() { // https://coderwall.com/p/uf2pka

		var items = $('.article-slideshow .item'), //grab all slides
			heights = [], //create empty array to store height values
			tallest; //create variable to make note of the tallest slide

		if (items.length) {
			function normalizeHeights() {
				items.each(function() { //add heights to array
					heights.push($(thisx`).height()); 
				});
				tallest = Math.max.apply(null, heights); //cache largest value
				items.each(function() {
					$(this).css('min-height',tallest + 'px');
				});
			};
			normalizeHeights();

			$(window).on('resize orientationchange', function () {
				tallest = 0, heights.length = 0; //reset vars
				items.each(function() {
					$(this).css('min-height','0'); //reset min-height
				}); 
				normalizeHeights(); //run it again 
			});
		}
	}
	carouselNormalization();
});*/