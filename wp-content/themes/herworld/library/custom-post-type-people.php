<?php
add_action('init', 'cptui_register_my_cpt_nguoi');
function cptui_register_my_cpt_nguoi() {
register_post_type('chan-dung', array(
'label' => 'Chân dung',
'description' => '',
'public' => true,
'show_ui' => true,
'show_in_menu' => true,
'capability_type' => 'post',
'map_meta_cap' => true,
'hierarchical' => false,
'rewrite' => array('slug' => 'chan-dung', 'with_front' => true),
'query_var' => true,
'has_archive' => true,
'taxonomies' => array('post_tag'),
'supports' => array('title','editor','excerpt','trackbacks','custom-fields','comments','revisions','thumbnail','author','page-attributes','post-formats'),
'labels' => array (
  'name' => 'Chân dung',
  'singular_name' => 'Chân dung',
  'menu_name' => 'Chân dung',
  'add_new' => 'Add Chân dung',
  'add_new_item' => 'Add New Nguoi',
  'edit' => 'Edit',
  'edit_item' => 'Edit Chân dung',
  'new_item' => 'New Chân dung',
  'view' => 'View Chân dung',
  'view_item' => 'View Chân dung',
  'search_items' => 'Search Chân dung',
  'not_found' => 'No Chân dung Found',
  'not_found_in_trash' => 'No Chân dung Found in Trash',
  'parent' => 'Parent Chân dung',
)
) ); }

add_action('init', 'cptui_register_my_taxes_profession');
function cptui_register_my_taxes_profession() {
register_taxonomy( 'profession',array (
  0 => 'chan-dung',
),
array( 'hierarchical' => true,
	'label' => 'Professions',
	'show_ui' => true,
	'query_var' => true,
	'show_admin_column' => false,
	'labels' => array (
  'search_items' => 'Profession',
  'popular_items' => '',
  'all_items' => '',
  'parent_item' => '',
  'parent_item_colon' => '',
  'edit_item' => '',
  'update_item' => '',
  'add_new_item' => '',
  'new_item_name' => '',
  'separate_items_with_commas' => '',
  'add_or_remove_items' => '',
  'choose_from_most_used' => '',
)
) ); 
}




?>