<?php 
	get_template_part('laurier/submit');

	global $user_ID;
    $id = 0;
    $uri_custom = $_SERVER['REQUEST_URI'];
    $uri_custom = (substr($uri_custom, -1) == '/' ) ? trim($uri_custom, "/") : $uri_custom;
    $end        = end( explode('/', $uri_custom) );
    if(is_numeric($end)) { 
        $id = $end; 
    }
	if ( is_single('thong-tin-cuoc-thi') ) {
		$cmnd_exist 		= ( is_user_logged_in() ) ? get_user_meta($user_ID, 'cmnd', true) : '';
		$laurier_body_class = ( empty($cmnd_exist) && is_user_logged_in() ) ? 'laurier-share' : 'laurier-home';
	} elseif ( is_single('giai-thuong') ) {
		$laurier_body_class = 'laurier-prize';
	}elseif ( is_single('the-le') ) {
		$laurier_body_class = 'laurier-rule';
	}else{
		if ( is_single('tham-gia') && !is_user_logged_in() ) {
			$laurier_body_class = 'laurier-home';
		}elseif ( is_single('bai-du-thi-ca-nhan') && !is_user_logged_in()  ) {
			$laurier_body_class = ( $id != 0 ) ? 'laurier-share' :'laurier-home';
		}else{
			$laurier_body_class = 'laurier-share';
		}
	}
	
?>

<?php get_header('cgqn'); ?>

<?php if (have_posts()) { ?>
	<?php while (have_posts()) : the_post(); ?>

	<div class="laurier-wrap <?php echo $laurier_body_class; ?>">
	    <!-- Begin Header -->
	    <header class="header-nav">
	        <div class="laurier-container">
	            <a href="<?php echo site_url( '/co-gai-quyen-nang' ); ?>" class="logo"><img src="<?php echo URL_IMAGE; ?>/logo.png" /></a>
	            <button class="nav-toggle visible-xs"></button>
	        </div>
	    </header>
	    <!-- End Header -->
	    <!-- Begin Cotnent -->
	    <div id="laurier-content">
	        <section class="sections">
	            <div class="laurier-container">
	                <!-- Leftcol -->
	                <div class="leftcol">
	                    <div class="navbar-collapse laurier-nav" id="bs-example-navbar-collapse-1">
		                    <?php laurier_main_nav(); ?>
	                    </div>
	                </div>
	                <!-- /Leftcol -->

					<!-- Maincol -->
					<div class="maincol">
					<?php 
						if ( is_single('thong-tin-cuoc-thi') ) {
							get_template_part('laurier/laurier-home');
						} elseif ( is_single('giai-thuong') ) {
							get_template_part('laurier/laurier-prize');
						}elseif ( is_single('the-le') ) {
							get_template_part('laurier/laurier-rule');
						}elseif ( is_single('tham-gia') ) {
							get_template_part('laurier/laurier-share');
						}elseif ( is_single('bai-du-thi-ca-nhan') ) {
							get_template_part('laurier/laurier-profile');
						}elseif ( is_single('thu-vien') ) {
							get_template_part('laurier/laurier-library');
						}elseif ( is_single('danh-sach-dat-giai') ) {
							get_template_part('laurier/laurier-list');
						}else{
							the_content();
						}
					?>
					</div>
					<!-- /maincol -->
				</div>
			</section>
			
		</div>  
		<!-- End Content -->

	    <!-- Begin Footer -->
	    <footer class="laurier-footer">
	        <div class="laurier-container">
	            <div class="footer-area">
	                <div class="connect">
	                    <img class="product" src="<?php echo URL_IMAGE; ?>/front/img-product-footer.png" alt="SlimGuard">
	                    <div class="connect-us">
	                        <label>Kết nối với chúng tôi</label> <a target="_blank" href="https://www.facebook.com/DangCap1mm/" class="connect-fb-link"><strong>Facebook</strong></a>
	                    </div>
	                </div>
	            </div><!-- /footer-area -->
	            <div class="organizational-brand">
	                <div class="brand">
	                    <label>Đơn vị tổ chức</label>
	                    <span><img src="<?php echo URL_IMAGE; ?>/brand-01.png" alt="SlimGuard"></span>
	                    <span><img src="<?php echo URL_IMAGE; ?>/brand-02.png" alt="Herworld"></span>
	                </div>
	            </div><!-- /organizational -->
	        </div>
	    </footer>
	    <!-- End Footer -->

	<?php if ( is_single('the-le') ) { ?>
        <script>
        $(document).ready(function() {
            $(".nano").nanoScroller();
        });
        </script>
	<?php } ?>

	<?php endwhile; ?>
<?php } ?>

<?php get_footer(); ?>
