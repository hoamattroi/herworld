<!doctype html>
<!--[if lt IE 7]><html lang="vi-VN" class="no-js lt-ie9 lt-ie8 lt-ie7"> <![endif]-->
<!--[if (IE 7)&!(IEMobile)]><html lang="vi-VN" class="no-js lt-ie9 lt-ie8"><![endif]-->
<!--[if (IE 8)&!(IEMobile)]><html lang="vi-VN" class="no-js lt-ie9"><![endif]-->
<!--[if gt IE 8]><!--> <html lang="vi-VN" class="no-js"><!--<![endif]-->

	<head>
		<meta charset="utf-8">

		<!-- Google Chrome Frame for IE -->
		<meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1">

		<title><?php wp_title(''); ?></title>

		<!-- mobile meta (hooray!) -->
		<meta name="HandheldFriendly" content="True">
		<meta name="MobileOptimized" content="320">
		<meta name="viewport" content="width=device-width, initial-scale=1.0"/>

		<!-- icons & favicons (for more: http://www.jonathantneal.com/blog/understand-the-favicon/) -->
		<link rel="apple-touch-icon" href="<?php echo get_template_directory_uri(); ?>/library/images/apple-icon-touch.png">
		<link rel="icon" href="<?php echo get_template_directory_uri(); ?>/favicon.ico">
		<!--[if IE]>
			<link rel="shortcut icon" href="<?php echo get_template_directory_uri(); ?>/favicon.ico">
		<![endif]-->
		<!-- or, set /favicon.ico for IE10 win -->
		<meta name="msapplication-TileColor" content="#f01d4f">
		<meta name="msapplication-TileImage" content="<?php echo get_template_directory_uri(); ?>/library/images/win8-tile-icon.png">

		<link rel="pingback" href="<?php bloginfo('pingback_url'); ?>">

		<link href='http://fonts.googleapis.com/css?family=Source+Sans+Pro&amp;subset=vietnamese' rel='stylesheet' type='text/css'>
		
		<!-- wordpress head functions -->
		<?php wp_head(); ?>
		<!-- end of wordpress head -->
		
		<!-- Facebook Pixel Script -->
		<?php 
		$curr_post_id = $wp_query->post->ID;
		if(get_field('facebook_pixel_script', $curr_post_id)){
			 echo get_field('facebook_pixel_script', $curr_post_id); 
		}
		?>
		<!-- End Facebook Pixel Script -->
		
		<!-- Doublecklick codes -->
		<?php if(the_field('gen_ad_code', 'option')){ echo the_field('gen_ad_code', 'option'); } ?>
		<?php require_once('library/adfields_header.php'); ?>


		<!-- end Doubclick Codes -->
		<!-- drop Google Analytics Here -->
		<?php if(the_field('analytics_code', 'option')){ echo the_field('analytics_code', 'option'); } ?>
		<!-- end analytics -->
	
	</head>

	<body <?php body_class(); ?>>
	
		<?php if(the_field('google_tag_manager_code', 'option')){ echo the_field('google_tag_manager_code', 'option'); } ?>
		<div id="fb-root"></div>
		<script>(function(d, s, id) {
		  var js, fjs = d.getElementsByTagName(s)[0];
		  if (d.getElementById(id)) return;
		  js = d.createElement(s); js.id = id;
		  js.src = "//connect.facebook.net/en_GB/all.js#xfbml=1&appId=561106073976518";
		  fjs.parentNode.insertBefore(js, fjs);
		}(document, 'script', 'facebook-jssdk'));</script>
		<div id="container" itemscope itemtype="http://schema.org/CreativeWork">
		<section class="pre-header">
			<script type='text/javascript'><!--//<![CDATA[
				var w = window.innerWidth
						|| document.documentElement.clientWidth
						|| document.body.clientWidth;
				if( w <=480 ) {
					var m3_u = (location.protocol=='https:'?'https://ad.hoamattroi.com/www/delivery/ajs.php':'http://ad.hoamattroi.com/www/delivery/ajs.php');
				   var m3_r = Math.floor(Math.random()*99999999999);
				   if (!document.MAX_used) document.MAX_used = ',';
				   document.write ("<scr"+"ipt type='text/javascript' src='"+m3_u);
				   document.write ("?zoneid=123001");
				   document.write ('&amp;cb=' + m3_r);
				   if (document.MAX_used != ',') document.write ("&amp;exclude=" + document.MAX_used);
				   document.write (document.charset ? '&amp;charset='+document.charset : (document.characterSet ? '&amp;charset='+document.characterSet : ''));
				   document.write ("&amp;loc=" + escape(window.location));
				   if (document.referrer) document.write ("&amp;referer=" + escape(document.referrer));
				   if (document.context) document.write ("&context=" + escape(document.context));
				   if (document.mmm_fo) document.write ("&amp;mmm_fo=1");
				   document.write ("'><\/scr"+"ipt>");
				} else {
					var m3_u = (location.protocol=='https:'?'https://ad.hoamattroi.com/www/delivery/ajs.php':'http://ad.hoamattroi.com/www/delivery/ajs.php');
				   var m3_r = Math.floor(Math.random()*99999999999);
				   if (!document.MAX_used) document.MAX_used = ',';
				   document.write ("<scr"+"ipt type='text/javascript' src='"+m3_u);
				   document.write ("?zoneid=122501");
				   document.write ('&amp;cb=' + m3_r);
				   if (document.MAX_used != ',') document.write ("&amp;exclude=" + document.MAX_used);
				   document.write (document.charset ? '&amp;charset='+document.charset : (document.characterSet ? '&amp;charset='+document.characterSet : ''));
				   document.write ("&amp;loc=" + escape(window.location));
				   if (document.referrer) document.write ("&amp;referer=" + escape(document.referrer));
				   if (document.context) document.write ("&context=" + escape(document.context));
				   if (document.mmm_fo) document.write ("&amp;mmm_fo=1");
				   document.write ("'><\/scr"+"ipt>");
				}
				//]]>--></script><noscript><a href='http://ad.hoamattroi.com/www/delivery/ck.php?n=a42c3bdf&amp;cb=INSERT_RANDOM_NUMBER_HERE' target='_blank'><img src='http://ad.hoamattroi.com/www/delivery/avw.php?zoneid=122501&amp;cb=INSERT_RANDOM_NUMBER_HERE&amp;n=a42c3bdf' border='0' alt='' /></a></noscript>
			</section>
			<header class="header" role="banner">

				<div id="inner-header" class="container clearfix">

					<div class="header-upper">
						<div class="header-welcome"><?php //_e('Welcome!', 'harpersbazaar'); ?></div>
						<div class="header-social">
							<!-- <a class="social-icon social-icon-facebook" href="https://www.facebook.com/herworldvn">Facebook</a> -->
							<!-- <a class="social-icon social-icon-gplus" href="https://plus.google.com">Google Plus</a> -->
							<!-- <a class="social-icon social-icon-zing" href="#zing">Zing</a -->
						</div>
						<?php get_search_form(); ?>
					</div> 

					<nav>
						<div class="navbar-header">
							<button type="button" class="navbar-toggle collapsed" data-toggle="collapse" data-target=".navbar-collapse">
							<span class="sr-only">Toggle navigation</span>
							<span class="icon-bar"></span>
							<span class="icon-bar"></span>
							<span class="icon-bar"></span>
							</button>
							<div itemscope itemtype="http://schema.org/Organization">
								<a itemprop="url" class="navbar-brand" href="<?php echo home_url(); ?>" rel="nofollow">
									<img itemprop="logo" src="<?php echo get_stylesheet_directory_uri(); ?>/library/images/site-logo.png" alt="<?php bloginfo( 'name' ); ?> - <?php bloginfo( 'description' ); ?>" />
								</a>
							</div>
						</div>
					</nav>

				</div> <!-- end #inner-header -->

				<nav class="nav navbar" role="navigation">
					<div class="container clearfix">
						<div class="collapse navbar-collapse">
							<?php bones_main_nav(); ?>
							<span class="mobile_search"><?php get_search_form(); ?></span>
						</div>
					</div>
				</nav>

			</header> <!-- end header -->

			<?php 
				if (get_field('top_three_articles', 'option') && !is_singular('cgqn') && !is_archive('cgqn')) {
					get_template_part("parts/topthree");
				}
			?>