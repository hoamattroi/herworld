<?php
/**
 * The Template for displaying all single products.
 *
 * Override this template by copying it to yourtheme/woocommerce/single-product.php
 *
 * @author 		WooThemes
 * @package 	WooCommerce/Templates
 * @version     1.6.4
 */

if ( ! defined( 'ABSPATH' ) ) exit; // Exit if accessed directly

get_header();

$post_type='product';
$postsPerPage = get_option('posts_per_page');
if($postsPerPage == null) {
    $postsPerPage =12;
}

$paged = get_query_var('paged') ? get_query_var('paged') : 1;
$args = array(
    'post_type' => $post_type,
    'posts_per_page' => $postsPerPage,
    'orderby'=> 'post_date',
    'order' => 'DESC',
    'post_status'=> 'publish',
    'paged' => $paged    
);

$temp = $wp_query;
$wp_query = null;
$wp_query = new WP_Query( $args );

?>

<div id="content">
    <div class="container">
        <div id="main" class="clearfix" role="main">

            <div class="article-pre">
                <?php if ( function_exists('yoast_breadcrumb') ) {
                    yoast_breadcrumb('<p class="breadcrumbs">','</p>');
                } ?>
            </div>

            <div class="page-wrapper">
                <div class="category-content-wrapper">    
                    <h1 class="category-title barred-heading">
                        <span>Review sản phẩm làm đẹp</span>
                    </h1>     

                    <?php if ( $wp_query->have_posts() ) : ?>
                        <?php while ( $wp_query->have_posts() ) : $wp_query->the_post(); ?>
                            <article id="post-<?php echo $post->ID; ?>" <?php post_class('clearfix'); ?> role="article">

                                <header class="article-header">
                                    <a href="<?php the_permalink() ?>" title="<?php the_title_attribute(); ?>">
                                        <?php the_post_thumbnail('square-360', array('class' => 'category product review')); ?>
                                    </a>

                                    <h2 class="h2"><a href="<?php the_permalink() ?>" rel="bookmark" title="<?php the_title_attribute(); ?>"><?php the_title(); ?></a></h2>
                                    <p class="byline vcard">
                                        <?php 
                                        $cat_count = sizeof( get_the_terms( $post->ID, 'product_cat' ) );
                                        echo $product->get_categories( ', ', 'trong' . _n( '', '', $cat_count, 'woocommerce' ) . ' ', '' ); ?>
                                    </p>
                                    <?php woocommerce_get_template( 'loop/price.php' ); ?>
                                </header> <!-- end article header -->

                                <footer class="article-footer">
                                </footer> <!-- end article footer -->

                            </article> <!-- end article --> 
                        <?php endwhile; ?>
                    <?php endif; ?>

                    <!--Pagination-->
                    <?php if (function_exists('bones_page_navi')) { ?>
                        <?php bones_page_navi(); ?>
                    <?php } else { ?>
                        <nav class="wp-prev-next">
                            <ul class="clearfix">
                                <li class="prev-link"><?php next_posts_link(__('&laquo; Older Entries', "harpersbazaar")) ?></li>
                                <li class="next-link"><?php previous_posts_link(__('Newer Entries &raquo;', "harpersbazaar")) ?></li>
                            </ul>
                        </nav>
                    <?php } ?>
                    <?php 
                        $wp_query = $temp;
                        wp_reset_query(); 
                    ?>
                </div> <!-- article-wrapper -->
                
                <?php get_sidebar(); ?>

            </div> <!-- end .page-wrapper -->

        </div> <!-- end #main -->
    </div> <!-- end .container -->
</div> <!-- end #content -->

<?php get_footer(); ?>
