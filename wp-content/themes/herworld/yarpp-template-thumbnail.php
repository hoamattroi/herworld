<?php
/*
YARPP Template: Thumbnails
Description: Requires a theme which supports post thumbnails
Author: ThanhVo
*/ ?>
<?php if (have_posts()):?>
	<div class="recent_post_wrap">
		<section class="recent-posts">
			<h3><?php _e("Tin liên quan", 'harpersbazaar'); ?>:</h3>
				<?php while (have_posts()) : the_post(); ?>
					<?php if (has_post_thumbnail()):?>
					<article>
						<div>
							<a href="<?php the_permalink() ?>" rel="bookmark" title="<?php the_title_attribute(); ?>">
									<?php echo get_the_post_thumbnail(get_the_ID(), 'square-360'); ?>
							</a>
						</div>
						<a href="<?php the_permalink() ?>" rel="bookmark" title="<?php the_title_attribute(); ?>"><?php the_title(); ?></a>
					</article>
					<?php endif; ?>
				<?php endwhile; ?>
		</section>
	</div>
<?php endif; ?>