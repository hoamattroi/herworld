<?php get_header(); ?>

<div id="content" role="main">

	<?php 
		if (get_field('promo_slide', 'option')) {
			get_template_part("parts/homepagepromo");
		}
	?>

	<section class="latest-articles">

		<div class="container clearfix">

			<div class="row">
				
				<h1 class="barred-heading"><span><?php _e("Mới nhất trên Her world", 'harpersbazaar'); ?></span></h1>

				<?php 
				$runway_cat = get_category_by_slug( 'cuoc-thi-her-world-street-style-awards' );
				$catlist = get_categories('child_of=' . $runway_cat->term_id);
				$sub_cat_ids = array();
				foreach($catlist as $cat){
				   $sub_cat_ids[] = $cat->cat_ID;
				}
				$the_query = new WP_Query(
					array (
						'post_type' => array ('post', 'fashion-trends'),
						'showposts' => '12',
						'category__not_in' => $sub_cat_ids
					)
				);
			
				if ($the_query->have_posts()) : while ($the_query->have_posts()) : $the_query->the_post(); 
				?>

				<article id="post-<?php the_ID(); ?>" <?php post_class('clearfix'); ?> role="article">

					<a href="<?php the_permalink() ?>"><?php the_post_thumbnail('latest-article-thumb'); ?></a>

					<header class="article-header">

						<h2 class="h2"><a href="<?php the_permalink() ?>" rel="bookmark" title="<?php the_title_attribute(); ?>"><?php the_title(); ?></a></h2>
						<div class="article-meta">
							<p class="byline vcard">
								<?php if (get_post_type()!='post'){ ?>
									<?php 
										$post_type_loc = get_post_type();
										$post_type_loc = get_post_type_object($post_type_loc);
									?>
									<?php printf(__('trong %1s.', 'harpersbazaar'), $post_type_loc->labels->name ); ?>
								<?php }else { ?>
									<?php printf(__('trong %1$s.', 'harpersbazaar'), get_the_category_list(', ')); ?>
								<?php } ?>
							</p>
						</div>

					</header> <!-- end article header -->

				</article> <!-- end article -->

				<?php endwhile; ?>

				<?php else : ?>

					<div id="post-not-found" class="hentry clearfix">
						<p><?php _e("No articles in this category.", "harpersbazaar"); ?></p>
					</div>

				<?php endif; ?>
			</div>
				
		</div> <!-- end .container -->

	</section> <!-- end .latest-articles -->

	<section class="featured">
		<div class="container">
			<?php
			$featuredArticle = get_field('featured_article', 'option');
			
			$featuredImage = get_field('featured_article_image', 'option');
			$featuredHeading = get_field('featured_article_heading', 'option');
			if( $featuredArticle ): ?>
				<?php foreach( $featuredArticle as $post): // variable must be called $post (IMPORTANT) ?>
					<?php 
					setup_postdata($featuredArticle);
					//$featuredArticleID = get_the_id();
					$featuredArticleFullImage = get_post_thumbnail_id();
					$featuredArticleFullImage = wp_get_attachment_thumb_url($featuredArticleFullImage);
					?>
					<article class="featured-primary">
						<figure>
							<div class="featured-primary-imgwrap">
								<a href="<?php the_permalink(); ?>">
									<?php
									if ($featuredImage) {
										$featuredImage = getImgServerUrl($featuredImage["sizes"]["square-720"]);
										
										echo "<img src=\"$featuredImage\" />";

									} else {
										the_post_thumbnail('square-720');
									}
									?>
								</a>
								<a class="pinterest" href="http://www.pinterest.com/pin/create/button/?url=<?php the_permalink() ?>&amp;media=<?php echo $featuredArticleFullImage; ?>&amp;description=<?php the_title(); ?>">Pinterest</a>
								
							</div>
							<figcaption>
								<h2 class="h2">
									<a href="<?php the_permalink() ?>" rel="bookmark" title="<?php the_title_attribute(); ?>">
										<?php
										if ($featuredHeading) {
											echo $featuredHeading;
										} else {
											the_title();
										}
										?>
									</a>
								</h2>
								<p class="byline vcard">
									<?php
									printf(__('%1$s.', 'harpersbazaar'), get_the_category_list(', '));
									?>
								</p>
							</figcaption>	
						</figure>
					</article>
				<?php endforeach; ?>
				<?php wp_reset_postdata(); ?>
			<?php endif; ?>

			<?php //get_template_part( "parts/people"); ?>

			<div class="newsletter-form newsletter-form-home container">
				<div class="row">
					<h1><?php _e("Cập nhật ngay", "harpersbazaar"); ?></h1>
					<p><?php _e("Đăng ký nhận email mỗi tuần và cập nhật xu hướng mới nhất", "harpersbazaar"); ?></p>
					<?php gravity_form("Newsletter", $display_title=false, $display_description=false, $display_inactive=false, $field_values=null, $ajax=true); ?>
				</div>
			</div>

		</div>
		
	</section>

</div> <!-- end #content -->

<?php get_footer(); ?>