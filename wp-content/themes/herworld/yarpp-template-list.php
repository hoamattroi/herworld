<?php
/*
YARPP Template: List
Description: This template returns the related posts as a comma-separated list.
Author: mitcho (Michael Yoshitaka Erlewine)
*/
?>

<section class="related-posts">
<h4><?php _e('Bài viết liên quan','harpersbazaar'); ?></h4>
<ol>
<?php if (have_posts()):
	// $postsArray = array();
	$d = __('d-m-Y','harpersbazaar'); 
	while (have_posts()) : the_post();
		echo '<li><a href="'.get_permalink().'" rel="bookmark">'.get_the_title(). ' - ' . get_the_date($d).'</a></li>';
	endwhile;
?>
</ol>
</section>


<?php
// echo implode(', '."\n",$postsArray); // print out a list of the related items, separated by commas

else:?>

<p><?php _e('Không có bài viết liên quan.','harpersbazaar'); ?></p>
<?php endif; ?>
