<?php

if ( ! function_exists( 'reg_cogaiquyennang_post_type' ) ) {
	function reg_cogaiquyennang_post_type() {
		
	  	// update 22/11/2016
	  	register_post_type('cgqn', array(
			'label' => 'Cô gái quyền năng',
			'description' => 'Laurier',
			'public' => true,
			'show_ui' => true,
			'show_in_menu' => true,
			'capability_type' => 'post',
			'map_meta_cap' => true,
			'hierarchical' => false,
          	'rewrite'	=> array('slug' => 'co-gai-quyen-nang', 'with_front' => true),
			'query_var' => true,
			'has_archive' => true,
			'taxonomies' => array('post_tag'),
			'supports' => array('title','editor','excerpt','trackbacks','custom-fields','comments','revisions','thumbnail','author','page-attributes','post-formats',),
			'labels' => array (
				'name' => 'Cô gái quyền năng',
				'singular_name' => 'Cô gái quyền năng',
				'menu_name' => 'Cô gái quyền năng',
				// 'add_new' => 'Add Cô gái quyền năng',
				// 'add_new_item' => 'Add New Cô gái quyền năng',
				// 'edit' => 'Edit',
				// 'edit_item' => 'Edit Cô gái quyền năng',
				// 'new_item' => 'New Cô gái quyền năng',
				// 'view' => 'View Cô gái quyền năng',
				// 'view_item' => 'View Cô gái quyền năng',
				// 'search_items' => 'Search Cô gái quyền năng',
				// 'not_found' => 'No Cô gái quyền năng Found',
				// 'not_found_in_trash' => 'No Cô gái quyền năng Found in Trash',
				// 'parent' => 'Parent Cô gái quyền năng',
				)
			) 
		);

	  	//Register TAXONOMY
		register_taxonomy( 'cgqn-cat', array(0 => 'cgqn'), array(
	      'hierarchical' 		=> true, 
	      'query_var' 			=> true,
	      'show_in_nav_menus' 	=> true,
	      'has_archive'       	=> true,
          // 'rewrite'				=> array('slug' => 'cgqn', 'with_front' => true),
	      'label' 				=> 'Categories'
		  )
		);
	}
	add_action( 'init', 'reg_cogaiquyennang_post_type' );
}