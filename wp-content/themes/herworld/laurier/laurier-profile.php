<?php

global $current_user, $post;

$url_avatar =   get_user_meta( $current_user->ID, 'facebookall_user_thumbnail', true );
//$id         =   get_query_var('thamgia_id') ? get_query_var('thamgia_id') : ''; 
$uri_custom = $_SERVER['REQUEST_URI'];
$uri_custom = (substr($uri_custom, -1) == '/' ) ? trim($uri_custom, "/") : $uri_custom;
$end = end( explode('/', $uri_custom) );
$id = 0;
if(is_numeric($end)) { 
    $id = $end; 
}
$id         =   ( is_user_logged_in() && empty($id) ) ? $current_user->ID : $id;
$img_tpl    =   get_user_meta( $id , 'user_img_tpl', true );
?>

<?php if ( $id ){ ?>
    <?php $thamgiaInfo =  get_userdata( $id ); ?>

    <div class="box01 over-header">
        <div class="box01-heading">
            <h1 class="title-general small">ĐI TÌM GƯƠNG MẶT</h1>
            <img src="<?php echo URL_IMAGE; ?>/co-gai-quyen-nang-small.png" class="text-title" alt="Cô gái quyền năng">
        </div>
        <div class="box01-body">
            <div class="share-template finish">

                <?php 
                if ( $thamgiaInfo->roles && (in_array('thamgia', $thamgiaInfo->roles) || in_array('administrator', $thamgiaInfo->roles)) ){ ?>
                        
                    <div class="choose">
                        <?php if(!empty($img_tpl)) { ?>      
                            <img src="<?php echo $img_tpl ?>" class="img-responsive" alt="<?php echo $thamgiaInfo->nickname; ?>"/>  
                        <?php } else { ?>
                            <img src="<?php echo URL_IMAGE; ?>/temp-1-big.jpg" class="img-responsive" alt="<?php echo $thamgiaInfo->nickname; ?>"/>
                        <?php } ?>
                        <div class="infor-temp">
                            <h3 class="name"><?php echo $thamgiaInfo->nickname; ?></h3>
                            <div class="num">
                                <?php display_like_share_facebook( get_permalink(). "/" . $id  ); ?>
                            </div>
                        </div>
                    </div>
                    <div class="instock">
                        <div class="text-center profile">

                            <?php if ( is_user_logged_in() ){ ?>

                                <div class="avatar-profile">
                                    <?php if ( $url_avatar ){ ?>
                                        <img src="<?php echo $url_avatar; ?>" alt="<?php echo $current_user->nickname; ?>"/>
                                    <?php }else{ ?>
                                        <img src="<?php echo URL_IMAGE; ?>/avatar.jpg" alt="<?php echo $current_user->nickname; ?>"/>
                                    <?php } ?>
                                </div>
                                <p class="hello">
                                    Xin chào, <span><?php echo $current_user->nickname; ?> | <a href="<?php echo wp_logout_url( site_url('/co-gai-quyen-nang') ); ?>">Thoát</a></span>
                                </p>
                            <?php } ?>

                            <div class="description">
                                <?php //echo $thamgiaInfo->description; ?>
                                <?php if ( $current_user->ID == $id || $id == '' ){ ?>
                                    Chia sẻ Facebook để kêu gọi bình chọn.
                                    Cơ hội xuất hiện trên bảng quảng cáo
                                    của Laurier Super Slimguard đang chờ bạn!
                                <?php }else{ ?>
                                    <p class="hello">
                                        <span>Hãy chia sẻ quan điẻm sống của</span></br> <?php echo $thamgiaInfo->nickname; ?> <span>và truyền cảm hứng cho những cô gái khác nhé!</span>
                                    </p>
                                <?php } ?>
                            </div>
                            <a target="_blank" href="https://www.facebook.com/sharer/sharer.php?u=<?php echo get_permalink(). "/" . $id ?>&display=popup" class="gbtn gbtn-share"></a>
                        </div>
                    </div>

                <?php } else { ?>

                    <p>Trang không tìm thấy!</p>

                <?php } ?>

            </div>
        </div>
    </div><!-- /.box01 -->
        
<?php }else{ ?>

    <?php if ( !is_user_logged_in()){ ?>

        <?php get_template_part('laurier/laurier-home'); ?>
        
    <?php } ?>
    
<?php } ?>
