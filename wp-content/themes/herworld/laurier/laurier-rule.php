<?php



?>
<div class="box01">
    <div class="box01-heading">
        <h1 class="title-general small">ĐI TÌM GƯƠNG MẶT</h1>
        <img src="<?php echo URL_IMAGE; ?>/co-gai-quyen-nang-small.png" alt="Cô gái quyền năng" class="text-title">
    </div>
    <div class="box01-body">
        <div class="rules">
            <h3>Cuộc thi<br>ĐI TÌM GƯƠNG MẶT CÔ GÁI QUYỀN NĂNG</h3>
            <div class="inner-rule">
                <div class="nano">
                    <div class="nano-content">
                        <div class="rule-description">
                        	<?php the_content(); ?>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div><!-- /.box01 -->