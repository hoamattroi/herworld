<?php 
if ( !is_user_logged_in() ) {  
  get_template_part('laurier/laurier-home'); 
  } else { 
  $step = ( isset($_REQUEST['step']) ) ? $_REQUEST['step'] : ''; 
  if ( empty($step) || $step == 1 ) { 
    get_template_part('laurier/tpl-share-step-1'); 
  } else {   
    if ( $step == 2 ) { 
        get_template_part('laurier/tpl-share-step-2');
    } elseif ( $step == 3 ) { 
        get_template_part('laurier/tpl-share-step-3');
    } elseif ( $step == 4 ) { 
        get_template_part('laurier/tpl-share-step-4');
    } else {
      get_template_part('laurier/tpl-share-step-1'); 
    }
  } 
} 
?>