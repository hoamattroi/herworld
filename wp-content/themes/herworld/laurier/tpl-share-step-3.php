<?php 
	global $current_user;
	$phone 	= get_user_meta( $current_user->ID, 'phone', true );
	$cmnd 	= get_user_meta( $current_user->ID, 'cmnd', true );
?>
<form id="form-edit-info" action="<?php the_permalink(); ?>?step=4" method="post">
	<div class="box01 over-header">
	  <div class="box01-heading">
		<h1 class="title-general small">ĐI TÌM GƯƠNG MẶT </h1>
		<img src="<?php echo URL_IMAGE; ?>/co-gai-quyen-nang-small.png" alt="Cô gái quyền năng" class="text-title">
	  </div>
	  <div class="box01-body">
		<div class="share-template finish">
		  <div class="choose">

			<img src="<?php echo $_SESSION['user_img_tpl']; ?>" class="img-responsive" alt=""/>
		  </div>
		  <div class="instock">
		  	<p><strong>Đăng ký thông tin để bài thi hợp lệ:</strong></p>
			<dl class="form-inline">
			  <dd>
				<label class="form-label">Tên</label>
				<div class="outer">
				  <input type="text" name="step3-name" placeholder="Họ và tên" value="<?php echo $current_user->nickname; ?>" required/>
				</div>
			  </dd>
			</dl>
			<dl class="form-inline">
			  <dd>
				<label class="form-label">Điện thoại</label>
				<div class="outer">
				  <input type="text" name="step3-phone"  value="<?php echo $phone; ?>" required/>
				  <span class="required">Bắt buộc</span>
				</div>
			  </dd>
			</dl>
			<dl class="form-inline">
			  <dd>
				<label class="form-label">CMND</label>
				<div class="outer">
				  <input type="text" name="step3-cmnd"  value="<?php echo $cmnd; ?>" required/>
				  <span class="required">Bắt buộc</span>
				</div>
			  </dd>
			</dl>
			<dl class="form-inline">
			  <dd>
				<label class="form-label">E-mail</label>
				<div class="outer">
				  <input type="text" name="step3-email" readonly placeholder="E-mail" value="<?php echo $current_user->user_email; ?>" required/>
				</div>
			  </dd>
			</dl>
			<div class="text-center">
			  <button class="gbtn-finish gbtn"><span class="text-none">Kết thúc</span></button>
			</div>
		  </div><!-- /signup-form -->
		</div>
	  </div>              
	</div><!-- /.box01 -->
  </form> 