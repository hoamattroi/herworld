
<div class="box01 over-header">
    <div class="box01-heading">
        <h1 class="title-general small">ĐI TÌM GƯƠNG MẶT </h1>
        <img src="<?php echo URL_IMAGE; ?>/co-gai-quyen-nang-small.png" class="text-title" alt="Cô gái quyền năng">
    </div>
    <div class="box01-body">
        <div class="search-form">
            
                <input id="search-laurier" type="search" name="search" placeholder="Nhập từ khóa" value="<?php echo ( get_query_var('search') ) ? get_query_var('search') : '' ; ?>" />
                <button id="search-laurier-btn"></button>
                
        </div>

        <?php 
        // grab the current page number and set to 1 if no page number is set
        $page = isset($_REQUEST['trang']) ? $_REQUEST['trang'] : 1;

        // how many users to show per page
        $users_per_page = 6;

        // calculate the total number of pages.
        $total_pages = 1;
        $offset = $users_per_page * ($page - 1);

        if ( isset($_REQUEST['search']) ) {
            $search_string = esc_attr( trim( get_query_var('search') ) );
            $users = new WP_User_Query( array(
                'meta_query' => array(
                'relation' => 'AND',
                array( 
                    'key'           => 'cmnd',
                    'compare'       => 'EXISTS'),
                array(
                    'relation' => 'OR',
                        array(
                            'key'     => 'nickname',
                            'value'   => $search_string,
                            'compare' => 'LIKE'
                        ),
                        array(
                            'key'     => 'first_name',
                            'value'   => $search_string,
                            'compare' => 'LIKE'
                        ),
                        array(
                            'key'     => 'last_name',
                            'value'   => $search_string,
                            'compare' => 'LIKE'
                        ),
                        array(
                            'key'     => 'facebookall_user_email',
                            'value'   => $search_string,
                            'compare' => 'LIKE'
                        ),
                    ),
                ), 
                'role'      => 'thamgia',
                'meta_key'  => 'date_modify',
                'orderby'   => 'meta_value',
                'order'     => 'DESC',
                'fields'    => 'all_with_meta',
                'number'    => 999999,
                )
            );

            $user_count  = $users->get_results();
            // count the number of users found in the query
            $total_users = $user_count ? count($user_count) : 1;

            $total_pages = ceil($total_users / $users_per_page);

            // main user query
            $args  = array(
                'meta_query' => array(
                'relation' => 'OR',
                    array(
                        'key'     => 'nickname',
                        'value'   => $search_string,
                        'compare' => 'LIKE'
                    ),
                    array(
                        'key'     => 'first_name',
                        'value'   => $search_string,
                        'compare' => 'LIKE'
                    ),
                    array(
                        'key'     => 'last_name',
                        'value'   => $search_string,
                        'compare' => 'LIKE'
                    ),
                    array(
                        'key'     => 'facebookall_user_email',
                        'value'   => $search_string,
                        'compare' => 'LIKE'
                    ),
                ),
                'role'      => 'thamgia',
                'meta_key'  => 'date_modify',
                'orderby'   => 'meta_value',
                'order'     => 'DESC',
                'fields'    => 'all_with_meta',
                'number'    => $users_per_page,
                'offset'    => $offset // skip the number of users that we have per page  
            );

            // Create the WP_User_Query object
            $wp_user_query = new WP_User_Query($args);
            // var_dump($users_found);
            $authors =  $wp_user_query->get_results();
        }else{
            $count_args  = array(
                'role'      => 'thamgia',
                'meta_query' => array(
                    'relation' => 'AND', array( 
                        'key'           => 'cmnd',
                        'compare'       => 'EXISTS'
                    ),
                ),
                'meta_key'  => 'date_modify',
                'orderby'   => 'meta_value',
                'order'     => 'DESC',
                'fields'    => 'all_with_meta',
                'number'    => 999999      
            );
            $user_count_query   = new WP_User_Query($count_args);
            $user_count         = $user_count_query->get_results();
            // count the number of users found in the query
            $total_users = $user_count ? count($user_count) : 1;

            $total_pages = ceil($total_users / $users_per_page);

            // main user query
            $args  = array(
                // search only for Authors role
                'role'      => 'thamgia',
                'meta_query' => array(
                    'relation' => 'AND', array( 
                        'key'           => 'cmnd',
                        'compare'       => 'EXISTS'
                    ),
                ),
                'meta_key'  => 'date_modify',
                'orderby'   => 'meta_value',
                'order'     => 'DESC',
                // return all fields
                'fields'    => 'all_with_meta',
                'number'    => $users_per_page,
                'offset'    => $offset // skip the number of users that we have per page  
            );

            // Create the WP_User_Query object
            $wp_user_query = new WP_User_Query($args);

            // Get the results
            $authors = $wp_user_query->get_results();
        }

        // check to see if we have users
        if (!empty($authors)) {

            echo '<ul class="gallery">';
            // loop trough each author
            foreach ($authors as $author) {
                $img_tpl     = get_user_meta( $author->ID, 'user_img_tpl', true );
                $url_profile = site_url( '/co-gai-quyen-nang/bai-du-thi-ca-nhan' ) ."/" . $author->ID;
                $author_info = get_userdata($author->ID); ?>
                    <li>
                        <?php if ( $img_tpl ){ ?>
                            <a href="<?php echo $url_profile; ?>">
                            <img src="<?php echo $img_tpl; ?>" alt="<?php echo $author_info->nickname; ?>"/></a>
                        <?php }else{ ?>
                            <a href="<?php echo $url_profile; ?>">
                            <img src="<?php echo URL_IMAGE; ?>/gallery-1.jpg" alt="<?php echo $author_info->nickname; ?>"/></a>
                        <?php } ?>
                        <div class="infor-temp">
                            <h3 class="name"><?php echo $author_info->nickname; ?></h3>
                            <div class="num">
                                <?php display_like_share_facebook( $url_profile ); ?>
                            </div>
                        </div>
                    </li>
                <?php 
            }
            echo '</ul>';
            // grab the current query parameters
            $query_string = $_SERVER['QUERY_STRING'];

            // The $base variable stores the complete URL to our page, including the current page arg

            // if in the admin, your base should be the admin URL + your page
            // $base = admin_url(the_permalink()) . '?' . remove_query_arg('trang', $query_string) . '%_%';

            // if on the front end, your base is the current page
            $base = get_permalink( get_the_ID() ) . '?' . remove_query_arg('trang', $query_string) . '%_%';

            if ( $total_pages > 1) {
                echo '<div class="pagging"><strong>Trang | </strong>';
                echo paginate_links( array(
                    'base'      => $base, // the base URL, including query arg str_replace( $big, '%#%', esc_url( get_pagenum_link( $big ) ) )
                    'format'    => '&trang=%#%', // this defines the query parameter that will be used, in this case "p"
                    'next_text' => __('»'),
                    'prev_text' => __('«'),
                    'current'   => $page, // the current page
                    'total'     => $total_pages, // the total number of pages we have
                    'end_size'  => 1,
                    'mid_size'  => 1,
                ));
                echo "</div>";
            }
        } else {
            echo 'Không tìm thấy!';
        }
        ?>
    </div>
</div><!-- /.box01 -->