<?php
global $current_user; 
$step = ( isset($_REQUEST['step']) ) ? $_REQUEST['step'] : ''; 
if(empty($step) || $step == 1 ) { 
    if(!empty($_POST)) { 
        if(!empty($_FILES["fileToUpload"]["name"])) {
          $upload_dir = wp_upload_dir();  
          $tmp_img_file = basename($_FILES["fileToUpload"]["name"]); 
          // Check if image file is a actual image or fake image
          include( ABSPATH . 'wp-admin/includes/image.php' );
          $output_file = $upload_dir['path'].'/' . $tmp_img_file; 
          move_uploaded_file($_FILES["fileToUpload"]["tmp_name"], $output_file);
            // The ID of the post this attachment is for.
            // 0 Unattached file
            $parent_post_id = 0;
            $attachment = array(
              'guid'           => $upload_dir['url'] . '/' . basename( $output_file ), 
              'post_mime_type' => 'image/png',
              'post_title'     => preg_replace( '/\.[^.]+$/', '', basename( $output_file ) ),
              'post_content'   => '',
              'post_status'    => 'inherit'
            );

            // Insert the attachment.
            $attach_id = wp_insert_attachment( $attachment, $output_file, $parent_post_id );
            // Generate the metadata for the attachment, and update the database record.
            $attach_data = wp_generate_attachment_metadata( $attach_id, $output_file );
            wp_update_attachment_metadata( $attach_id, $attach_data ); 
            $output_file_url = wp_get_attachment_url( $attach_id  ); 
            $_SESSION['file_url'] = $output_file_url;
        }else{
           $_SESSION['file_url'] = get_user_meta($current_user->ID, 'user_img_ori', true);
        }


          if ($_POST['step1-name']) {
            update_user_meta( $current_user->ID, 'nickname', $_POST['step1-name']);
          }
          if ($_POST['step1-job']) {
            update_user_meta( $current_user->ID, 'job', $_POST['step1-job']);
          }
          if ($_POST['step1-style']) {
            update_user_meta( $current_user->ID, 'description', $_POST['step1-style']);
          }
          
          $_SESSION['name'] = $_POST['step1-name'];
          $_SESSION['job'] = $_POST['step1-job'];
          $_SESSION['style'] = $_POST['step1-style']; 
          $redirect = get_permalink(). "?step=2";
          header("Location: $redirect"); 
          exit;
    } 
} elseif($step == 2 ) {
  //print_r($_SESSION);
}
?> 