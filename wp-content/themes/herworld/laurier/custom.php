<?php

    define("URL_IMAGE", get_template_directory_uri()."/library/asset/images");

    /*==================================================================
    ========================  Filter, Add_acction  =====================
    ====================================================================*/

    add_action('show_admin_bar', 'remove_admin_bar');
    //add_action('admin_init', 'no_mo_dashboard');

    add_filter('pre_option_default_role', function($default_role){
        // You can also add conditional tags here and return whatever
        return 'thamgia'; // This is changed
        return $default_role; // This allows default
    });


    /*==================================================================
    =============================  Functions  ==========================
    ====================================================================*/

    // the main menu
    function laurier_main_nav() {
    	// display the wp3 menu if available
        wp_nav_menu(array(
        	'container' => false,                           // remove nav container
        	'container_class' => '',           				// class of container (should you choose to use it)
        	'menu' => __( 'Laurier Menu', 'herworld' ) 	,  	// nav name
        	'menu_class' => 'list-group', 					// adding custom nav class
        	'theme_location' => 'laurier-menu',             // where it's located in the theme
        	'before' => '',                                 // before the menu
            'after' => '',                                  // after the menu
            'link_before' => '',                            // before each link
            'link_after' => '',                             // after each link
            'depth' => 0,                                   // limit the depth of the nav
            'walker' => new wp_bootstrap_navwalker(),
        	'fallback_cb' => 'laurier_main_nav_fallback'    // fallback function
    	));
    } /* end bones main nav */

    // this is the fallback for header menu
    function laurier_main_nav_fallback() {
    	wp_page_menu( array(
    		'show_home' => true,
        	'menu_class' => 'list-group',      				// adding custom nav class
    		'include'     => '',
    		'exclude'     => '',
    		'echo'        => true,
            'link_before' => '',                            // before each link
            'link_after' => ''
    	) );
    }

    // add_filter('nav_menu_css_class' , 'special_nav_class' , 10 , 2);

    // function special_nav_class ($classes, $item) {
    //     if ( in_array('current-menu-item', $classes ) ){
    //         $classes[] = 'active';
    //     }
    //     return $classes;
    // }

    function remove_admin_bar() {
        if ( current_user_can('thamgia') && !is_admin()) {
            show_admin_bar(false);
        }
    }

    function no_mo_dashboard() {
      if ( current_user_can('thamgia') && $_SERVER['DOING_AJAX'] != '/wp-admin/admin-ajax.php') {
        wp_redirect(home_url()); exit;
      }
    }

    function display_like_share_facebook($url){
     printf('<div class="fb-like" 
            data-href="' . $url . '" 
            data-layout="button_count" 
            data-action="like" 
            data-share= "true"
            data-size= "small"
            data-show-faces="true">
            </div>'
        );
    }

    // add_action('init', 'laurier_custom_rewrite_rule', 10, 0);
    // function laurier_custom_rewrite_rule() {
    //     // if ( is_singular( 'cgqn' ) ) {
    //     // if ( ! is_object( $GLOBALS['wp'] ) ) {
    //     //     return;
    //     // }
    //     // $GLOBALS['wp']->add_query_var( 'thamgia_id' );
    //     add_rewrite_tag('%thamgia_id%', '([^&]+)');
    //     // add_rewrite_tag('%food%', '([^&]+)');
    //     add_rewrite_tag('%variety%', '([^&]+)');
    //     // add_rewrite_rule('co-gai-quyen-nang/thong-tin-cuoc-thi/([0-9]+)/?','index.php?thamgia_id=12','top');
    //     add_rewrite_rule('^shopping/([^/]*)/?','index.php?page_id=4461001&thamgia_id=$matches[1]','top');
    //     // add_rewrite_rule('co-gai-quyen-nang/thong-tin-cuoc-thi/([a-z]+)?', 'index.php?thamgia_id=$matches[1]', 'top' );
    //     // }
    // }  
    function custom_rewrite_rule() {
        add_rewrite_tag('%thamgia_id%', '([^&]+)');
        // add_rewrite_tag('%variety%', '([^&]+)');
        // add_rewrite_rule('^co-gai-quyen-nang/thong-tin-cuoc-thi/([^/]*)/?','index.php?post_type=cgqn&p=7373288&thamgia_id=$matches[1]','top');
        $trang_ca_nhan = get_page_by_path( 'bai-du-thi-ca-nhan', OBJECT, 'cgqn' );
        add_rewrite_rule('^co-gai-quyen-nang/bai-du-thi-ca-nhan/([^/]*)/?','index.php?post_type=cgqn&p='. $trang_ca_nhan->ID .'&thamgia_id=$matches[1]','top');
    } 
    add_action('init', 'custom_rewrite_rule', 10, 0);

    add_action( 'user_register', 'custom_meta_registration_create', 20, 1 );
    function custom_meta_registration_create( $user_id ) {

        // if ( get_user_meta( $user_id, 'fullname', true ); ) {
        //     update_user_meta($user_id, 'fullname', $_POST['fullname']);
        // }else{
        $cur_user = get_userdata($user_id);
        if ( $cur_user ) {
            update_user_meta( $user_id, 'nickname',  $cur_user->display_name );
            update_user_meta( $user_id, 'date_modify',  $cur_user->user_registered );
        }
        unset( $cur_user );
        // }

    }

    function add_award_into_list($award, $field_meta){
        $winner     = $award[$field_meta];
        $img_tpl    = get_user_meta($winner['ID'], 'user_img_tpl', true );
        $url        = site_url('/co-gai-quyen-nang/bai-du-thi-ca-nhan/') . $winner['ID'];
        ?>
        <li>
            <?php if ( $img_tpl ): ?>
                <a href="<?php echo $url; ?>"><img src="<?php echo $img_tpl; ?>" alt="<?php echo $winner['nickname']; ?>"/></a>
            <?php else: ?>
                <a href="<?php echo $url; ?>"><img src="<?php echo URL_IMAGE; ?>/gallery-1.jpg" alt="<?php echo $winner['nickname']; ?>"/></a>
            <?php endif ?>
            <div class="infor-temp">
                <h3 class="name"><a href="<?php echo $url; ?>"><?php echo $winner['nickname']; ?></a></h3>
                <div class="num">
                    <?php display_like_share_facebook(site_url('/co-gai-quyen-nang/bai-du-thi-ca-nhan/') . $winner['ID']); ?>
                </div>
            </div>
        </li>
        <?php
    }
    
    /*==================================================================
    =============================  Ajax ===== ==========================
    ====================================================================*/
	
	add_action('init', 'myStartSession', 1);
    add_action('wp_logout', 'myEndSession');
    add_action('wp_login', 'myEndSession');

    function myStartSession() {
        if(!session_id()) {
            session_start();
        }
    }

    function myEndSession() {
        session_destroy ();
    } 

    /* ajax Upload Laurier Form */
    add_action( 'wp_ajax_uploadLaurierInfo'                              , 'uploadLaurierInfo');
    add_action( 'wp_ajax_nopriv_uploadLaurierInfo'                       , 'uploadLaurierInfo');

    function uploadLaurierInfo() { 
        $img3D = isset($_REQUEST['image']) ? $_REQUEST['image'] :"";
        $arrResult = array();
        if(!is_user_logged_in()) {
            $arrResult['code'] = 0;
            $arrResult['url'] = "?step=1";
            exit;
        } 
        if(empty($img3D)) { 
            $arrResult['code'] = 0;
            $arrResult['url'] = "?step=2";
            exit;
        } else {
            
            //Save base_64 file  
            $upload_dir = wp_upload_dir();  
            $tmp_img_file = uniqid() . '.png';
            $output_file = $upload_dir['path'].'/' . $tmp_img_file; 
            $output_file_url = $upload_dir['url']. "/" . $tmp_img_file;
            $data = str_replace('data:image/png;base64,', '', $img3D);
            $data = str_replace(' ', '+', $data);
            $data = base64_decode($data);
            $success = file_put_contents($output_file, $data); 

            // The ID of the post this attachment is for.
            // 0 Unattached file
            $parent_post_id = 0;
            $attachment = array(
                'guid'           => $upload_dir['url'] . '/' . basename( $output_file ), 
                'post_mime_type' => 'image/png',
                'post_title'     => preg_replace( '/\.[^.]+$/', '', basename( $output_file ) ),
                'post_content'   => '',
                'post_status'    => 'inherit'
            );

            // Insert the attachment.
            $attach_id = wp_insert_attachment( $attachment, $output_file, $parent_post_id );
            // Generate the metadata for the attachment, and update the database record.
            $attach_data = wp_generate_attachment_metadata( $attach_id, $output_file );
            wp_update_attachment_metadata( $attach_id, $attach_data );

            $output_file_url = wp_get_attachment_url( $attach_id  );
            unset($_SESSION['user_img_tpl']);  
            $_SESSION['user_img_tpl'] = $output_file_url; 

            $current_user = wp_get_current_user();
            update_user_meta( $current_user->ID, 'user_img_ori', $_SESSION['file_url'] );
            update_user_meta( $current_user->ID, 'user_img_tpl', $output_file_url  );
            
            $redirect = get_permalink() . "?step=3";
            $arrResult['code'] = 1;
            $arrResult['url'] = $redirect; 
        }  
        echo json_encode($arrResult);
        die();
    }


    /* ajax Upload Laurier Form */
    add_action( 'wp_ajax_uploadLaurierImage'                              , 'uploadLaurierImage');
    add_action( 'wp_ajax_nopriv_uploadLaurierImage'                       , 'uploadLaurierImage');


        function uploadLaurierImage() { 
            print_r($_REQUEST); exit;
        $img3D = isset($_REQUEST['image']) ? $_REQUEST['image'] :"";
        $arrResult = array();
        if(!is_user_logged_in()) {
            $arrResult['code'] = 0;
            $arrResult['url'] = "?step=1";
            exit;
        } 
        if(empty($img3D)) { 
            $arrResult['code'] = 0;
            $arrResult['url'] = "?step=2";
            exit;
        } else {
            
            //Save base_64 file  
            $upload_dir = wp_upload_dir();  
            $tmp_img_file = uniqid() . '.png';
            $output_file = $upload_dir['path'].'/' . $tmp_img_file; 
            $output_file_url = $upload_dir['url']. "/" . $tmp_img_file;
            $data = str_replace('data:image/png;base64,', '', $img3D);
            $data = str_replace(' ', '+', $data);
            $data = base64_decode($data);
            $success = file_put_contents($output_file, $data); 

            // The ID of the post this attachment is for.
            // 0 Unattached file
            $parent_post_id = 0;
            $attachment = array(
                'guid'           => $upload_dir['url'] . '/' . basename( $output_file ), 
                'post_mime_type' => 'image/png',
                'post_title'     => preg_replace( '/\.[^.]+$/', '', basename( $output_file ) ),
                'post_content'   => '',
                'post_status'    => 'inherit'
            );

            // Insert the attachment.
            $attach_id = wp_insert_attachment( $attachment, $output_file, $parent_post_id );
            // Generate the metadata for the attachment, and update the database record.
            $attach_data = wp_generate_attachment_metadata( $attach_id, $output_file );
            wp_update_attachment_metadata( $attach_id, $attach_data );

            $output_file_url = wp_get_attachment_url( $attach_id  );
            unset($_SESSION['user_img_tpl']);  
            $_SESSION['user_img_tpl'] = $output_file_url; 

            $current_user = wp_get_current_user();
            update_user_meta( $current_user->ID, 'user_img_ori', $_SESSION['file_url'] );
            update_user_meta( $current_user->ID, 'user_img_tpl', $output_file_url  );
            
            $redirect = get_permalink() . "?step=3";
            $arrResult['code'] = 1;
            $arrResult['url'] = $redirect; 
        }  
        echo json_encode($arrResult);
        die();
    }

     /* ajax Upload Laurier Form */
    add_action( 'wp_ajax_uploadLaurierFinal'                              , 'uploadLaurierFinal');
    add_action( 'wp_ajax_nopriv_uploadLaurierFinal'                       , 'uploadLaurierFinal');

    function uploadLaurierFinal() { 
        $name       = isset($_REQUEST['step3-name'])    ? $_REQUEST['step3-name']   :   "";
        $phone      = isset($_REQUEST['step3-phone'])   ? $_REQUEST['step3-phone']  :   ""; 
        $cmnd       = isset($_REQUEST['step3-cmnd'])    ? $_REQUEST['step3-cmnd']   :   "";
        $arrResult  = array(); 

        if(!is_user_logged_in()) {
            $arrResult['code']  = 0;
            $arrResult['url']   = "?step=1";
            echo json_encode($arrResult);
            exit;
        }
        if(empty($name) || empty($phone)) { 
            $arrResult['code']  = 0;
            $arrResult['url']   = "?step=3";
            echo json_encode($arrResult);
            exit();
        } else {
            //Get current user WP
            $current_user = wp_get_current_user();
            update_user_meta( $current_user->ID, 'nickname'     , $name     );
            update_user_meta( $current_user->ID, 'phone'        , $phone    );
            update_user_meta( $current_user->ID, 'cmnd'         , $cmnd     );
            update_user_meta( $current_user->ID, 'description'  , $_SESSION['style']    );
            update_user_meta( $current_user->ID, 'job'          , $_SESSION['job']      );
            update_user_meta( $current_user->ID, 'date_modify'  , current_time( 'mysql' ) );
            $redirect = get_permalink() . "?step=4";
            $arrResult['code'] = 1;
            $arrResult['url'] = $redirect; 
        }  
        echo json_encode($arrResult);
        die();
    }


function yoast_title( $str ) {
    if(is_singular( 'cgqn' )) {
        return "Cuộc thi Cô Gái Quyền Năng";    
    }
    return $str;
}
add_filter( 'wpseo_title', 'yoast_title', 10, 1 );

function yoast_desc( $str ) {
    if(is_singular( 'cgqn' )) {
        return "Bạn là cô gái hiện đại, tỏa sáng bởi chính cá tính riêng của mình? Hãy chia sẻ quan điểm sống và truyền cảm hứng cho những cô gái khác!";
    }
  return $str;
}

add_filter( 'wpseo_metadesc', 'yoast_desc', 10, 1 );

function yoast_img( $url ) { 
    if(is_singular( 'cgqn' )) {
        global $current_user;
        //$id         =   get_query_var('thamgia_id') ? get_query_var('thamgia_id') : ''; 
        $id = 0;
        $uri_custom = $_SERVER['REQUEST_URI'];
        $uri_custom = (substr($uri_custom, -1) == '/' ) ? trim($uri_custom, "/") : $uri_custom;
        $end        = end( explode('/', $uri_custom) );
        if(is_numeric($end)) { 
            $id = $end; 
        }
        $id         =   ( is_user_logged_in() && empty($id) ) ? $current_user->ID : $id;
        $img_tpl    =   get_user_meta( $id , 'user_img_tpl', true );
        if(!empty($img_tpl))   
            return $img_tpl; 
    }
    return $url;
}
add_filter( 'wpseo_opengraph_image', 'yoast_img', 10, 1 );

function add_meta_tags() {
    global $post;
    if(is_singular( 'cgqn' )) {
        global $current_user;
        //$id         =   get_query_var('thamgia_id') ? get_query_var('thamgia_id') : ''; 
        $id = 0;
        $uri_custom = $_SERVER['REQUEST_URI'];
        $uri_custom = (substr($uri_custom, -1) == '/' ) ? trim($uri_custom, "/") : $uri_custom;
        $end        = end( explode('/', $uri_custom) );
        if(is_numeric($end)) { 
            $id = $end; 
        }
        $id         =   ( is_user_logged_in() && empty($id) ) ? $current_user->ID : $id;
        $img_tpl    =   get_user_meta( $id , 'user_img_tpl', true );
        if(!empty($img_tpl)) {
            echo '<meta name="og:url" content="' . get_permalink() . "/" . $id .  '" />' . "\n";
            echo '<meta name="og:image" content="' . $img_tpl . '" />' . "\n";
        }
    }
}
add_action( 'wp_head', 'add_meta_tags' , 2 );

add_filter( 'wpseo_canonical', '__return_false' );

add_action('wp_ajax_syncTotal', 'syncTotal');
add_action('wp_ajax_nopriv_syncTotal', 'syncTotal');
function syncTotal() {
    if(isset($_REQUEST['id'])) {
        $id = intval($_REQUEST['id']);
    } else {
        $arrResult['code'] = 0; 
        echo json_encode($arrResult);
        die();
    }
    
    $url = "http://herworldvietnam.vn/co-gai-quyen-nang/bai-du-thi-ca-nhan/". $id; 
    $access_token = '341626452666074|fc3ef924d2cd0fa64a7db4a367e8ae3f';
    $response = wp_remote_get('https://graph.facebook.com/v2.8/?id=' . urlencode( $url ) . '&access_token=' . $access_token );
    if( is_array($response) ) {
        $body = json_decode( $response['body'] );
        $count = intval( $body->share->share_count );
        $arrResult['code'] = 1; 
        $arrResult['msg'] = $count; 
        echo json_encode($arrResult);
    } else {
        $arrResult['code'] = 0; 
        $arrResult['msg'] = "FB Error"; 
        echo json_encode($arrResult); 
    }
    exit;
}



    /* ajax Upload Laurier Form */
    add_action( 'wp_ajax_update_user_info'                              , 'update_user_info');
    add_action( 'wp_ajax_nopriv_update_user_info'                       , 'update_user_info');

    function update_user_info() { 
        $img3D      = isset($_REQUEST['image']) ? $_REQUEST['image'] :"";
        $id         = isset($_REQUEST['id']) ? $_REQUEST['id'] :"";
        $arrResult  = array();
        if(!is_user_logged_in()) {
            $arrResult['code'] = 0;
            $arrResult['url'] = site_url(). "view-laurier.php?id=" .$id;
            exit;
        } 
        if(empty($img3D)) { 
            $arrResult['code'] = 0;
            $arrResult['url'] = site_url(). "view-laurier.php?id=" .$id;
            exit;
        } else {
            
            //Save base_64 file  
            $upload_dir = wp_upload_dir();  
            $tmp_img_file = uniqid() . '.png';
            $output_file = $upload_dir['path'].'/' . $tmp_img_file; 
            $output_file_url = $upload_dir['url']. "/" . $tmp_img_file;
            $data = str_replace('data:image/png;base64,', '', $img3D);
            $data = str_replace(' ', '+', $data);
            $data = base64_decode($data);
            $success = file_put_contents($output_file, $data); 

            // The ID of the post this attachment is for.
            // 0 Unattached file
            $parent_post_id = 0;
            $attachment = array(
                'guid'           => $upload_dir['url'] . '/' . basename( $output_file ), 
                'post_mime_type' => 'image/png',
                'post_title'     => preg_replace( '/\.[^.]+$/', '', basename( $output_file ) ),
                'post_content'   => '',
                'post_status'    => 'inherit'
            );

            // Insert the attachment.
            $attach_id = wp_insert_attachment( $attachment, $output_file, $parent_post_id );
            // Generate the metadata for the attachment, and update the database record.
            $attach_data = wp_generate_attachment_metadata( $attach_id, $output_file );
            wp_update_attachment_metadata( $attach_id, $attach_data );

            $output_file_url = wp_get_attachment_url( $attach_id  );
            unset($_SESSION['user_img_tpl']);  
            $_SESSION['user_img_tpl'] = $output_file_url; 

            $output_file_url = str_replace("backend.herworldvietnam", "herworldvietnam" , $output_file_url);

            update_user_meta( $id, 'user_img_tpl', $output_file_url  );
            
            $redirect = site_url(). "/view-laurier.php?id=" .$id;
            $arrResult['code'] = 1;
            $arrResult['url'] = $redirect; 
        }  
        echo json_encode($arrResult);
        die();
    }