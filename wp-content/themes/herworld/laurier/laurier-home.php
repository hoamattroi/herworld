<?php
global $user_ID;
$cmnd = ( is_user_logged_in() ) ? get_user_meta( $user_ID, 'cmnd', true ) : '';
?>


<?php if ( is_user_logged_in() ) { ?>

	<?php if ( empty($cmnd) ){ ?>
		<?php get_template_part('laurier/laurier-share'); ?>
	<?php }else{ ?>

		<div class="gbox">
			<h1 class="title-general">ĐI TÌM GƯƠNG MẶT </h1>
			<img src="<?php echo URL_IMAGE; ?>/co-gai-quyen-nang.png" class="text-title" alt="Cô gái quyền năng">

			<div class="gbox-desc show-desktop">
	            <p>
				Bạn là cô gái hiện đại, độc lập, tự chủ trong cuộc sống.<br/>
				Bạn tự tin và sẵn sàng theo đuổi mục tiêu lớn của bản thân.<br/>
				Bạn tỏa sáng bởi chính điểm mạnh và cá tính riêng của mình.
				</p>
				<p>
						Hãy chia sẻ quan điểm sống của bạn<br/>
						và truyền cảm hứng cho những cô gái khác nhé!
				</p>
	        </div>
	        <div class="gbox-desc show-mobile">
	            <p>
				Bạn là cô gái hiện đại, độc lập, tự chủ trong cuộc sống. 
				Bạn tự tin và sẵn sàng theo đuổi mục tiêu lớn của bản thân. 
				Bạn tỏa sáng bởi chính điểm mạnh và cá tính riêng của mình.
				</p>
				<p>
					Hãy chia sẻ quan điểm sống của bạn
					và truyền cảm hứng cho những cô gái khác nhé!
				</p>
	        </div>
			<a href="<?php echo site_url( '/co-gai-quyen-nang/tham-gia' ); ?>" class="gbtn-share gbtn"><strong class="visiable">Chia sẻ</strong></a>
		</div>
	
	<?php } ?>

<?php }else{ ?>

	<div class="gbox">
		<h1 class="title-general">ĐI TÌM GƯƠNG MẶT </h1>
		<img src="<?php echo URL_IMAGE; ?>/co-gai-quyen-nang.png" class="text-title" alt="Cô gái quyền năng">

			<div class="gbox-desc show-desktop">
	            <p>
				Bạn là cô gái hiện đại, độc lập, tự chủ trong cuộc sống.<br/>
				Bạn tự tin và sẵn sàng theo đuổi mục tiêu lớn của bản thân.<br/>
				Bạn tỏa sáng bởi chính điểm mạnh và cá tính riêng của mình.
				</p>
				<p>
						Hãy chia sẻ quan điểm sống của bạn<br/>
						và truyền cảm hứng cho những cô gái khác nhé!
				</p>
	        </div>
	        <div class="gbox-desc show-mobile">
	            <p>
				Bạn là cô gái hiện đại, độc lập, tự chủ trong cuộc sống. 
				Bạn tự tin và sẵn sàng theo đuổi mục tiêu lớn của bản thân. 
				Bạn tỏa sáng bởi chính điểm mạnh và cá tính riêng của mình.
				</p>
				<p>
					Hãy chia sẻ quan điểm sống của bạn
					và truyền cảm hứng cho những cô gái khác nhé!
				</p>
	        </div>
	        <a href="javascript:void(0);" onClick="FbAll.facebookLogin();" class="gbtn-share gbtn"><strong class="visiable">Chia sẻ</strong></a>
	        <div class="hidden"><?php do_shortcode('[facebookall_login]'); ?></div>
	</div><!--/gbox -->

<?php } ?>