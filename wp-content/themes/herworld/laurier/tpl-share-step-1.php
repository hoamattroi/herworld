<?php 
	global $current_user;
	$job = get_user_meta( $current_user->ID, 'job', true );

  	$path_file 	= get_user_meta( $current_user->ID,'user_img_ori', true);
  	$base64 	= '';
  	if ( $path_file ) {
	  	$upload_dir = wp_upload_dir(); 
	  	$path_file 	= str_replace($upload_dir['url'], $upload_dir['path'], $path_file);
	  	$path_file 	= str_replace(' ', '+', $path_file);
		$type 		= pathinfo($path_file, PATHINFO_EXTENSION);
		$data 		= file_get_contents($path_file);
		$base64 	= 'data:image/' . $type . ';base64,' . base64_encode($data);
  	}

  	$btn_upload = ( $path_file ) ? 'style="display:none"' : '';
  	$show_img 	= ( $path_file ) ? '' : 'style="display:none"';
?>
<form id="form-upload-avatar" action="<?php the_permalink(); ?>" method="post" enctype="multipart/form-data">
  <input type="hidden" name="step" value="1">
  <div class="box01 over-header">
	<div class="box01-heading">
		<h1 class="title-general small">ĐI TÌM GƯƠNG MẶT</h1>
		<img src="<?php echo URL_IMAGE; ?>/co-gai-quyen-nang-small.png" class="text-title" alt="Cô gái quyền năng">
	</div>
	<div class="box01-body">
	  <div class="profilePic">
		<div class="avatar">
		  <a href="javascript:;" class="link-browser" id="btn-upload-avatar" <?php echo $btn_upload; ?> ><span class="alignmiddle">Tải ảnh<br>của bạn</span></a>
		  <img id="img-preview" src="<?php echo $base64; ?>" alt="<?php echo $current_user->nickname; ?>" <?php echo $show_img; ?>/>
		  <input type='file' name="fileToUpload" id="imgInp" style="display:none"/>
		</div>
		<div class="profileBtn">
			<input type="button" id="btn-change-img" value="" class="gbtn" <?php echo $show_img; ?>></button><br>
			<span id="error-message"></span>
		</div>
	  </div><!-- /profilePic -->
	  <div class="signup-form">
		<dl class="form">
		  <dd>
			<label class="form-label">Tên của bạn</label>
			<input onfocus="this.placeholder = ''" onblur="this.placeholder = 'Nhập họ & tên'" type="text" name="step1-name" placeholder="Nhập họ & tên" value="<?php echo $current_user->nickname; ?>" required/>
		  </dd>
		</dl>
		<dl class="form">
		  <dd>
			<label class="form-label">Công việc hiện tại</label>
			<input onfocus="this.placeholder = ''" onblur="this.placeholder = 'Nhập công việc'" type="text"  name="step1-job" placeholder="Nhập công việc" value="<?php echo $job; ?>" required/>
		  </dd>
		</dl>
		<dl class="form description">
		  <dd>
			<label class="form-label">Quan điểm sống của tôi</label>
			<textarea onfocus="this.placeholder = ''" onblur="this.placeholder = 'Chia sẻ quan điểm sống'" spellcheck="false"  name="step1-style" maxlength="125" placeholder="Chia sẻ quan điểm sống" required><?php echo $current_user->description; ?></textarea>
		  </dd>
		</dl>
		<button class="gbtn-continue gbtn" type="submit" name="step1-submit"><span class="text-none">Tiếp tục</span></button>
	  </div><!-- /signup-form -->
	</div>             
	  
  </div><!-- /.box01 -->
</form>