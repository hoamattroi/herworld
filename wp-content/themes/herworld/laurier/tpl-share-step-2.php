
<form id="form-edit-avatar" action="" method="post">
	<div class="box01 over-header">
	  <div class="box01-heading">
		<h1 class="title-general small">ĐI TÌM GƯƠNG MẶT </h1>
		<img src="<?php echo URL_IMAGE; ?>/co-gai-quyen-nang-small.png" alt="Cô gái quyền năng" class="text-title">
	  </div>
	  <div class="box01-body">
		<div class="share-template">
		  <div class="choose">
	          <div class="show-template show01">
	            <img src="<?php echo URL_IMAGE; ?>/front/logo.png" alt="" class="img-responsive img-logo">
	            <div class="sentence-temp">
	              <h2><?php echo $_SESSION['style']; ?></h2>
	              <p class="author"><?php echo $_SESSION['name']; ?> <br class="sp">- <?php echo $_SESSION['job']; ?></p>
	            </div>
	            <figure class="imgUpload">
	              <img class="img-responsive" src="<?php echo $_SESSION['file_url'];?>" alt="">
	            </figure>
	          </div><!-- /show-template -->
	      </div>
		  <div class="instock">
			<h2>Chọn mẫu trang trí</h2>
			<ul>
			  <li class="active"><a href="show01" href="javascript:;" data-src="<?php echo URL_IMAGE; ?>/template/template-01-show.png"><img src="<?php echo URL_IMAGE; ?>/temp-1.jpg" class="img-reponsive" alt="" /></a></li>
			  <li><a  href="show02" href="javascript:;" data-src="<?php echo URL_IMAGE; ?>/template/template-02-show.png"><img src="<?php echo URL_IMAGE; ?>/temp-2.jpg" alt="" class="img-reponsive" /></a></li>
			  <li><a  href="show03" href="javascript:;" data-src="<?php echo URL_IMAGE; ?>/template/template-03-show.png"><img src="<?php echo URL_IMAGE; ?>/temp-3.jpg" alt="" class="img-reponsive" /></a></li>
			  <li><a  href="show04" href="javascript:;" data-src="<?php echo URL_IMAGE; ?>/template/template-04-show.png"><img src="<?php echo URL_IMAGE; ?>/temp-4.jpg" alt="" class="img-reponsive" /></a></li>
			</ul>
		  </div>
		</div>
		<div class="button-footer">
		  <button class="gbtn gbtn-continue"></button>
		</div>
	  </div>              
	</div><!-- /.box01 -->
</form>