<?php


$award_tab_id = ( isset($_REQUEST['trang']) ) ? $_REQUEST['trang'] - 1 : '';

?>
<div class="box01 over-header">
    <div class="box01-heading">
        <h1 class="title-general small">ĐI TÌM GƯƠNG MẶT </h1>
        <img src="<?php echo URL_IMAGE; ?>/co-gai-quyen-nang-small.png" class="text-title" alt="Cô gái quyền năng">
    </div>
    <div class="box01-body">
    <?php 
        if ( have_rows('zini_awards') ) {
            $awards     = get_field('zini_awards', get_the_ID());

            // loop through the rows of data
            foreach ($awards as $key => $award) {
                if ( $key == $award_tab_id) {
                    // display a sub field value
                    $label      = $award['zini_award_label'];
                    $winner     = $award['zini_win1'];
                    $img_tpl    = get_user_meta($winner['ID'], 'user_img_tpl', true );
                    $url        = site_url('/co-gai-quyen-nang/bai-du-thi-ca-nhan/') . $winner['ID'];
                    ?>
                    <div class="number-1">
                        <div class="temp">
                            <?php if ( $img_tpl ): ?>
                                <a href="<?php echo $url; ?>"><img src="<?php echo $img_tpl; ?>" alt="<?php echo $winner['nickname']; ?>"/></a>
                            <?php else: ?>
                                <a href="<?php echo $url; ?>"><img src="<?php echo URL_IMAGE; ?>/gallery-1.jpg" alt="<?php echo $winner['nickname']; ?>"/></a>
                            <?php endif ?>
                            <div class="infor-temp">
                                <h3 class="name"><a href="<?php echo $url; ?>"><?php echo $winner['nickname']; ?></a></h3>
                                <div class="num">
                                    <?php display_like_share_facebook($url); ?>
                                </div>
                            </div>
                        </div>
                        <div class="text">
                            <h3 class="title-prize">GIẢI NHẤT <?php echo $label; ?></h3>
                            <a href="<?php echo $url; ?>" class="name"><?php echo $winner['nickname']; ?></a>
                            <p><?php echo $winner['user_description']?></p>
                        </div>
                    </div>
                    <?php if ( !empty($award['zini_win2']) || !empty($award['zini_win3']) || !empty($award['zini_win4']) ) { ?>
                        <h3 class="title-prize">GIẢI NHÌ <?php echo $label; ?></h3>
                        <ul class="gallery">
                            <?php add_award_into_list( $award, 'zini_win2'); ?>
                            <?php add_award_into_list( $award, 'zini_win3'); ?>
                            <?php add_award_into_list( $award, 'zini_win4'); ?>
                        </ul>
                    <?php
                    }
                }
            }

            printf('<div class="pagging bg"><strong>Danh sách thắng giải</strong>');
            // loop through the rows of data
            foreach ($awards as $key => $award) {
                $class_current = ( $key == $award_tab_id ) ? 'current' : '';
                $label  = $award['zini_award_label'];
                ?>
                    <span>|</span>
                    <a href="?trang=<?php echo $key +1; ?>" class="<?php echo $class_current; ?>"><?php echo $label; ?></a>
                <?php
            }
            printf('</div>');
        }
    ?>
        
    </div>
</div><!-- /.box01 -->