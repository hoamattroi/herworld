<?php



?>

<div class="gbox">
    <h1 class="title-general">ĐI TÌM GƯƠNG MẶT</h1>
    <img src="<?php echo URL_IMAGE; ?>/co-gai-quyen-nang.png" class="text-title" alt="Cô gái quyền năng">
    <div class="gbox-desc">
        <div class="text-prize">
            <img src="<?php echo URL_IMAGE; ?>/prize-banner.png" class="img-responsive visible-xs"/>
            <strong class="uppercase"><?php the_title(); ?></strong><br/>
            <?php the_content() ?>
        </div>
    </div>
</div><!--/gbox -->
