<?php get_header(); ?>

<div id="content">
	<div class="container">
		<div id="main" class="clearfix" role="main">

			<div class="article-pre">
				<?php if ( function_exists('yoast_breadcrumb') ) {
					yoast_breadcrumb('<p class="breadcrumbs">','</p>');
				} ?>
			</div>
			<div class="page-wrapper">
				<div class="category-content-wrapper">
					<h1 class="category-title category-danh-sach-binh-chon barred-heading">
						<span><?php single_cat_title(); ?></span>
					</h1>
					<?php 
						// Fashion & Design 946283
						// Health & Beauty 946339
						// Music 946579
						// Film 947014
						// Business  947245
						$arrParrentCat = array(946283, 946339, 946579, 947014,  947245);
						foreach($arrParrentCat as $parentCatId) {
							$parentCatObj =  get_category($parentCatId);
							if(!empty($parentCatObj)) {
								echo "<h2 class='category-title barred-heading'><span>".$parentCatObj->name."</span></h2>";
								$categories = get_categories( array( 'child_of' => $parentCatId ));
								foreach ( $categories as $category ) { 
									$topPosts = array (
										'cat'                    => $category->term_id,
										'posts_per_page'         => '3',
										'hide_empty'             => false
									);
									$topPostsQuery = new WP_Query( $topPosts );
									while ($topPostsQuery->have_posts()) : $topPostsQuery->the_post();
										$idForm 	= get_field("form_id", get_the_ID());
										$all_total = 0;
										if(!empty($idForm)) {
											$all_count  	= RGFormsModel::get_form_counts( $idForm );
											if(!empty($all_count)) {
												$all_total 	= $all_count['total'];
											}
										}
								?>
									<article id="post-<?php the_ID(); ?>" <?php post_class('clearfix'); ?> role="article">
										<header class="article-header">
											<a href="<?php the_permalink() ?>"><?php the_post_thumbnail('square-360'); ?></a>
											<h2 class="h2">
												<a href="<?php the_permalink() ?>" rel="bookmark" title="<?php the_title_attribute(); ?>">
													<?php 
														$shortTitle = get_field('short_title', get_the_ID());
														echo $shortTitle;
													?>
												</a>
											</h2>
											<?php 
/*
												if($all_total != null && $all_total > 0) {
													?>
														<p class="love-voting-text"> <span class="love-voting"></span><?php echo $all_total; ?></p>
													<?php
												} else { ?>
													<p class="love-voting-text"> <span class="love-voting"></span>0</p>
											<?php
												}
*/
											?>
											<p class="byline vcard">
												<?php 
												//printf(__('hạng mục %1$s.', 'harpersbazaar'), get_the_category_list(', ')); 
												printf(__('<strong>%1$s</strong>.', 'harpersbazaar'), $category->name); 
													
												?>
											</p>
										</header> <!-- end article header -->
										<footer class="article-footer">
										</footer> <!-- end article footer -->
									</article> <!-- end article -->
							<?php 
									endwhile;
								//echo "<div style='clear:both;'></div>";
								}
							}
						}	  
						?>
				</div> <!-- .category-content-wrapper -->

				<?php get_sidebar(); ?>

			</div> <!-- end .page-wrapper -->

			 

		</div> <!-- end #main -->
	</div> <!-- end .container -->
</div> <!-- end #content -->
<section class="tags moremore">
	<h2 class="more-heading"><span><?php _e("Xem thêm...", "harpersbazaar"); ?></span></h2>
	<ul class=more_tags><?php 
		$curr_cat = get_cat_id( single_cat_title("",false) );
		top_tags($curr_cat); 
		?></ul>
</section>
<?php get_footer(); ?>
