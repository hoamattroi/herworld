			<?php get_template_part("parts/bazaar-tv"); ?>
<!-- Popup Ads -->
            <div id="popup-giua-man-hinh">
                <div class="popUpBannerBox ">
                    <div class="popUpBannerInner">
                        <div class="popUpBannerContent">
                            <p><a href="#" class="closeButton">x</a></p>
                            <a href="#"><img src="<?php bloginfo('template_url'); ?>/images/bg_popup.jpg" width="100%" style="border-radius: 30px;" class="img-responsive"></a>
                        </div>
                    </div>
                </div>
            </div>

			<footer class="footer" role="contentinfo">
				<div id="border-footer" class="clearfix">
				
					<div id="inner-footer" class="container clearfix">
						<div id="inner_footer_row">
						<div id="issue_frontcover">
							<img src="<?php if(get_field('magazine_cover', 'option')){ echo getImgServerUrl(get_field('magazine_cover', 'option')); } ?>" alt="<?php bloginfo( 'name' ) ?>" title="<?php bloginfo( 'name' ) ?>" />
						</div>
						<div id="inner_footer_wrapper">
							<div id="magazine_title">
								<div id="magazine_desc">
									<h4><?php _e('Tạp chí', 'harpersbazaar'); ?></h4>
									<h3 itemprop="publisher"><?php _e('Her World việt nam', 'harpersbazaar'); ?></h3>
								</div>
								<div id="past_issues">
									<!-- div class="footer_button">
										<a href="#footerbutton"><?php _e('Xem các số báo trước', 'harpersbazaar'); ?></a>
									</div -->
								</div>
							</div>
							<!-- <nav role="navigation"> -->
								<?php bones_footer_links(); ?>
							<!-- </nav> -->
							<div id="social_interaction">
								<div id="integration_wrapper">
									<p><p>
									<h3<br/></h3>
									
								</div>
								<div id="social_wrapper">
									<div>
										<a target="_blank" class="social-icon social-icon-facebook" href="https://www.facebook.com/herworldvn">Facebook</a>
										<a target="_blank" class="social-icon social-icon-youtube" href="https://www.youtube.com/user/HerWorldVN">YouTube</a>
									</div> 
									<p><?php _e("CÔNG TY TNHH TRUYỀN THÔNG HOA MẶT TRỜI<br />
	&copy; Copyright Her World Việt Nam, All rights reserved<br />
	Số 190 Nguyễn Văn Hưởng, P. Thảo Điền, Q. 2, TP.HCM<br />
	(+84 8) 3519 2301", "harpersbazaar"); ?><br/>Phiên bản thử nghiệm Beta</p>
	<a href="http://www.dmca.com/Protection/Status.aspx?ID=6f725660-e52e-4afe-ac9e-1acc6211415c" title="DMCA.com Protection Status" class="dmca-badge"> <img src="//images.dmca.com/Badges/DMCA_logo-green150w.png?ID=6f725660-e52e-4afe-ac9e-1acc6211415c" alt="DMCA.com Protection Status"></a> <script src="//images.dmca.com/Badges/DMCABadgeHelper.min.js"> </script>
								</div>
							</div>
						</div>
						</div>
					</div> <!-- end #inner-footer -->
				</div>
			</footer> <!-- end footer -->
			<div id="back_to_top_button"><a href="#" class="back-to-top">&nbsp;</a></div>
		</div> <!-- end #container -->

		<!-- all js scripts are loaded in library/bones.php -->
		<?php wp_footer(); ?>

		<script type="text/javascript">

			function setCookie(name,value,days) {
                var expires = "";
                if (days) {
                    var date = new Date();
                    date.setTime(date.getTime() + (days*24*60*60*1000));
                    expires = "; expires=" + date.toUTCString();
                }
                document.cookie = name + "=" + (value || "")  + expires + "; path=/";
            }
            function getCookie(name) {
                var nameEQ = name + "=";
                var ca = document.cookie.split(';');
                for(var i=0;i < ca.length;i++) {
                    var c = ca[i];
                    while (c.charAt(0)==' ') c = c.substring(1,c.length);
                    if (c.indexOf(nameEQ) == 0) return c.substring(nameEQ.length,c.length);
                }
                return null;
            }
            function eraseCookie(name) {
                document.cookie = name+'=; Max-Age=-99999999;';
            }

		
		$(window).bind("load", function() { 
			var cookiePopup = getCookie("show_popup");
			if (cookiePopup == null) {
				setTimeout(function(){ 
				$('.popUpBannerBox').fadeIn(); 	 	
				$('.popUpBannerContent').addClass('animated swing'); }, 1000);

			setTimeout(function(){ 
				$('.popUpBannerBox').fadeOut(); 
				$('.popUpBannerContent').removeClass('animated swing');
				$('.banner-left').fadeIn();
				$('.banner-right').fadeIn();
				$('.banner-left').addClass('animated zoomOutLeft');
				$('.banner-right').addClass('animated zoomOutRight'); }, 4000);
			
				setCookie('show_popup', 1, 0.001);
			}else{
				setTimeout(function(){ 
				$('.popUpBannerBox').fadeOut(); 
				$('.popUpBannerContent').removeClass('animated swing');
				$('.banner-left').fadeIn();
				$('.banner-right').fadeIn();
				$('.banner-left').addClass('animated zoomOutLeft');
				$('.banner-right').addClass('animated zoomOutRight'); }, 1000);
			}
			

			$('.popUpBannerContent').click(function(e) {
				$('.popUpBannerBox').show();
				e.stopPropagation();
			 });

			$('body, .closeButton').click(function() {
				$('.popUpBannerBox').fadeOut();
				$('.banner-left').fadeIn();
				$('.banner-right').fadeIn();
			});

		});
		
		//	pin banner
		var effectSticky = function (elm, width, height, config) {

        
        var parentSelector =  'footer';
        var offset_top =  10;
        var offset_bottom =  0;
        var parent = [];        
        parent = window.document.getElementsByClassName(parentSelector); // parent CLASS      

        if (!parent || parent.length < 1) {
            return;
        }
        parent = parent[0];

        var el_rect = elm.i.getBoundingClientRect();
        var height_sticky = parent.getBoundingClientRect().bottom - el_rect.top;
        var top_bottom = height_sticky - el_rect.height;
        var css_position = '';
        $(elm.d).css('width',$(elm.d).outerWidth()+"px");
        var setSticky = function () {
            var ele_top = elm.i.getBoundingClientRect().top, p_bottom = parent.getBoundingClientRect().bottom;
            if (ele_top < offset_top && p_bottom + offset_bottom > el_rect.height + offset_top) {
                if (css_position != 'fixed') {
                    elm.d.style.position = 'fixed';
                    elm.d.style.top = offset_top + "px";
                }
                css_position = 'fixed';
                
            } else {
                if (css_position != 'absolute') {
                    elm.d.style.position = 'absolute';
                    if (ele_top < offset_top) {
                        top_bottom = p_bottom - ele_top - el_rect.height;
                        elm.d.style.top = Math.round(top_bottom) + "px";
                    } else {
                        elm.d.style.top = "0px";
                    }
                }
                css_position = 'absolute';
            }
        };
        if (top_bottom > 1) {
            setSticky();
            $(window).scroll(function(){setSticky()});
        }

		};
		effectSticky({i:$('.box-banner-right')[0],d:$('.banner-right')[0]});

		effectSticky({i:$('.box-banner-left')[0],d:$('.banner-left')[0]});
    </script>
		<!--<script type="text/javascript" async src="//assets.pinterest.com/js/pinit.js"></script>-->
		<!-- Google Code for Remarketing Tag -->
		<!--------------------------------------------------
		Remarketing tags may not be associated with personally identifiable information or placed on pages related to sensitive categories. See more information and instructions on how to setup the tag on: http://google.com/ads/remarketingsetup
		--------------------------------------------------->
		<script type="text/javascript">
		/* <![CDATA[ */
		var google_conversion_id = 878273564;
		var google_custom_params = window.google_tag_params;
		var google_remarketing_only = true;
		/* ]]> */
		</script>
		<script type="text/javascript" src="//www.googleadservices.com/pagead/conversion.js">
		</script>


		<noscript>
		<div style="display:inline;">
		<img height="1" width="1" style="border-style:none;" alt="" src="//googleads.g.doubleclick.net/pagead/viewthroughconversion/878273564/?value=0&amp;guid=ON&amp;script=0"/>
		</div>
		</noscript>
	</body>

</html> <!-- end page. what a ride! -->