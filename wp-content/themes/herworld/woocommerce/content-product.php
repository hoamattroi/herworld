<?php
/**
 * The template for displaying product content within loops.
 *
 * Override this template by copying it to yourtheme/woocommerce/content-product.php
 *
 * @author 		WooThemes
 * @package 	WooCommerce/Templates
 * @version     1.6.4
 */

if ( ! defined( 'ABSPATH' ) ) exit; // Exit if accessed directly

global $product, $woocommerce_loop;

// Store loop count we're currently on
if ( empty( $woocommerce_loop['loop'] ) )
	$woocommerce_loop['loop'] = 0;

// Store column count for displaying the grid
if ( empty( $woocommerce_loop['columns'] ) )
	$woocommerce_loop['columns'] = apply_filters( 'loop_shop_columns', 4 );

// Ensure visibility
if ( ! $product || ! $product->is_visible() )
	return;

// Increase loop count
$woocommerce_loop['loop']++;

// Extra post classes
$classes = array();
if ( 0 == ( $woocommerce_loop['loop'] - 1 ) % $woocommerce_loop['columns'] || 1 == $woocommerce_loop['columns'] )
	$classes[] = 'first';
if ( 0 == $woocommerce_loop['loop'] % $woocommerce_loop['columns'] )
	$classes[] = 'last';
?>


<article id="post-<?php echo get_the_ID(); ?>" <?php post_class('clearfix'); ?> role="article">

    <header class="article-header">
        <a href="<?php the_permalink() ?>" title="<?php the_title_attribute(); ?>">
            <?php the_post_thumbnail('square-360', array('class' => 'category product review')); ?>
        </a>

        <h2 class="h2"><a href="<?php the_permalink() ?>" rel="bookmark" title="<?php the_title_attribute(); ?>"><?php the_title(); ?></a></h2>
        <p class="byline vcard">
            <?php 
            $cat_count = sizeof( get_the_terms( $post->ID, 'product_cat' ) );
            echo $product->get_categories( ', ', 'trong' . _n( '', '', $cat_count, 'woocommerce' ) . ' ', '' ); ?>
        </p>
        <?php woocommerce_get_template( 'loop/price.php' ); ?>
    </header> <!-- end article header -->

    <footer class="article-footer"></footer> <!-- end article footer -->

</article> <!-- end article --> 