<?php
/**
 * Single Product title
 *
 * @author 		WooThemes
 * @package 	WooCommerce/Templates
 * @version     1.6.4
 */

if ( ! defined( 'ABSPATH' ) ) exit; // Exit if accessed directly

global $post, $product;

$cat_count = sizeof( get_the_terms( $post->ID, 'product_cat' ) );
?>
<div class="post-categories">
	<?php echo $product->get_categories( ', ', '' . _n( '', '', $cat_count, 'woocommerce' ) . ' ', '' ); ?>
</div>
<h1 itemprop="name" class="product_title entry-title"><?php the_title(); ?></h1>