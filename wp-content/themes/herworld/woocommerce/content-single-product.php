<?php
/**
 * The template for displaying product content in the single-product.php template
 *
 * Override this template by copying it to yourtheme/woocommerce/content-single-product.php
 *
 * @author 		WooThemes
 * @package 	WooCommerce/Templates
 * @version     1.6.4
 */

if ( ! defined( 'ABSPATH' ) ) exit; // Exit if accessed directly
?>

<?php
	/**
	 * woocommerce_before_single_product hook
	 *
	 * @hooked wc_print_notices - 10
	 */
	 do_action( 'woocommerce_before_single_product' );

	 if ( post_password_required() ) {
	 	echo get_the_password_form();
	 	return;
	 }
?>

<div itemscope itemtype="<?php echo woocommerce_get_product_schema(); ?>" id="product-<?php the_ID(); ?>" <?php post_class(); ?>>
	<header class="article-header clearfix">
		<?php
			/**
			 * woocommerce_before_single_product_summary hook
			 *
			 * @hooked woocommerce_show_product_sale_flash - 10
			 * @hooked woocommerce_show_product_images - 20
			 */
			do_action( 'woocommerce_before_single_product_summary' );
		?>

		<div class="summary entry-summary">

			<?php
				/**
				 * woocommerce_single_product_summary hook
				 *
				 * @hooked woocommerce_template_single_title - 5
				 * @hooked woocommerce_template_single_rating - 10
				 * @hooked woocommerce_template_single_price - 10
				 * @hooked woocommerce_template_single_excerpt - 20
				 * @hooked woocommerce_template_single_add_to_cart - 30
				 * @hooked woocommerce_template_single_meta - 40
				 * @hooked woocommerce_template_single_sharing - 50
				 */
				do_action( 'woocommerce_single_product_summary' );
			?>

		</div><!-- .summary -->

		<?php
			/**
			 * woocommerce_after_single_product_summary hook
			 *
			 * @hooked woocommerce_output_product_data_tabs - 10
			 * @hooked woocommerce_upsell_display - 15
			 * @hooked woocommerce_output_related_products - 20
			 */
			//do_action( 'woocommerce_after_single_product_summary' );
		?>

		<meta itemprop="url" content="<?php the_permalink(); ?>" />
	</header>

	<section class="entry-content clearfix">
		<?php the_content(); ?>
		<div class="fb-like" data-width="400" data-layout="button_count" data-action="like" data-show-faces="false" data-share="false"></div>
	</section>

	<div class="inline-wrapper">
		<div class="inline-sharing">
			<h4><?php _e('Chia sẻ trên', 'harpersbazaar'); ?></h4>
			<div class="inline-icon-wrap">
				<a class="social-icon social-icon-facebook-dark" href="https://www.facebook.com/sharer/sharer.php?u=<?php the_permalink(); ?>" target="_blank"><?php _e('Share on Facebook','harpersbazaar'); ?></a>
			</div>
			<!-- div class="inline-icon-wrap">
				<a class="social-icon social-icon-zing-dark" name="zm_share" type="text" title="Chia sẻ lên Zing Me"><?php _e('Share on Zing Me','harpersbazaar'); ?></a>
			</div -->
			<div class="inline-icon-wrap">
				<a class="social-icon social-icon-gplus-dark" href="https://plus.google.com/share?url=<?php the_permalink(); ?>" target="_blank"><?php _e('Share on Google Plus','harpersbazaar'); ?></a>
			</div>
		</div>
	</div>		
										
	<?php related_posts(); ?>

	<section class="article-interactivity">
		<?php $permalink = get_permalink(); ?>
		<div class="article-interactivity-wrapper">
			<div class="share-wrapper">
				<div class="share-facebook">
					<a href="http://www.facebook.com/sharer/sharer.php?s=100&amp;p[url]=<?php echo $permalink; ?>&amp;p[title]=<?php the_title(); ?>&amp;p[summary]=<?php echo strip_tags(get_the_excerpt()); ?>"><?php echo getFacebookShares($permalink); ?> <?php _e('Chia sẻ FB', 'harpersbazaar'); ?></a>
				</div>
				<div class="share-gplus"><a href="https://plus.google.com/share?url=<?php echo $permalink; ?>"><?php echo getGplusShares($permalink); ?> <?php _e('Chia sẻ G+', 'harpersbazaar'); ?></a></div>
				<div class="share-comment"><a href="#article-comments"><?php _e('Bình luận', 'harpersbazaar'); ?></a></div>
			</div>
		</div>
		
		<!-- div class="share-zing"><a href="#share-zing"> <?php _e('shares', 'harpersbazaar'); ?></a></div>
		<script src="http://stc.ugc.zdn.vn/link/js/lwb.0.7.js" type="text/javascript"></script -->
	</section>

	<div class="recent_post_wrap">
		<section class="recent-posts">		
		<?php
		$tag_count = get_the_terms( get_the_ID(), 'pa_brand' );
		if($tag_count){
			$list_terms = array();
			foreach ($tag_count as $value) {
				$list_terms[] = $value->slug;
			}
			$my_query = new WP_Query(array(
			    'posts_per_page'   => 4,
			    'post__not_in'     => array(get_the_ID()),
			    'post_type' => 'product',
			    'orderby' => 'title',
			    'order' => 'ASC',
		     	'tax_query' => array(
			  		array(
					   'taxonomy' => 'pa_brand',
					   'field' => 'slug',
					   'terms' => $list_terms
			 		)
				)
		    ));
			if( $my_query->have_posts() ) {
				echo '<h3>'. __('Sản phẩm cùng thương hiệu:', 'harpersbazaar').'</h3>';
				while ($my_query->have_posts()) : $my_query->the_post(); ?>
					<article>
							<div><a href="<?php the_permalink() ?>" rel="bookmark" title="<?php the_title_attribute(); ?>"><?php echo get_the_post_thumbnail(get_the_id(), 'square-360'); ?></a></div>
							<a href="<?php the_permalink() ?>" rel="bookmark" title="<?php the_title_attribute(); ?>"><?php the_title(); ?></a>
					</article>
				<?php endwhile;
		 	}
			wp_reset_query();  // Restore global post data stomped by the_post(). 
		}
		?>		
		</section>
	</div>

	<footer class="article-footer">
		<?php if(has_tag()){ ?>
			<h3><?php _e('Từ khóa cho bạn', 'harpersbazaar'); ?></h3>
			<?php the_tags('<ul class="tags" itemprop="keywords"><li>','</li><li>','</li></ul>'); ?>
		<?php } ?>
	</footer> <!-- end article footer -->

	<div id="article-comments">
		<h3><?php _e("Bình luận", 'harpersbazaar'); ?></h3>
		<?php comments_template(); ?>
		
		<!--<div class="fb-comments" data-href="<?php the_permalink();?>" data-numposts="5" data-colorscheme="light"></div>
		<style>.fb-comments, .fb-comments iframe[style], .fb-like-box, .fb-like-box iframe[style] {width: 100% !important;}.fb-comments span, .fb-comments iframe span[style], .fb-like-box iframe span[style] {width: 100% !important;}.uiBoxLightblue iframe{display:none !important;}</style>-->
	</div>
	
</div><!-- #product-<?php the_ID(); ?> -->

<?php do_action( 'woocommerce_after_single_product' ); ?>
