<?php
/**
 * The base configurations of the WordPress.
 *
 * This file has the following configurations: MySQL settings, Table Prefix,
 * Secret Keys, WordPress Language, and ABSPATH. You can find more information
 * by visiting {@link http://codex.wordpress.org/Editing_wp-config.php Editing
 * wp-config.php} Codex page. You can get the MySQL settings from your web host.
 *
 * This file is used by the wp-config.php creation script during the
 * installation. You don't have to use the web site, you can just copy this file
 * to "wp-config.php" and fill in the values.
 *
 * @package WordPress
 */

// ** MySQL settings - You can get this info from your web host ** //
/** The name of the database for WordPress */
//define('DB_NAME', 'sfm_harpers-herworld-multi');
define('DB_NAME', 'sfm_herworld.v2.dev');

/** MySQL database username */
define('DB_USER', 'root');

/** MySQL database password */
define('DB_PASSWORD', 'th1nhh49');
//define('DB_PASSWORD', 'sfm@n0thing');

/** MySQL hostname */
#define('DB_HOST', 'localhost');
// define('DB_HOST', '192.168.50.203');
define('DB_HOST', '192.168.50.22');

/** Database Charset to use in creating database tables. */
define('DB_CHARSET', 'utf8');

/** The Database Collate type. Don't change this if in doubt. */
define('DB_COLLATE', '');

define('WP_HOME', 'http://herworld.v2.dev.hoamattroi.com/');
define('WP_SITEURL', 'http://herworld.v2.dev.hoamattroi.com/');

/**#@+
 * Authentication Unique Keys and Salts.
 *
 * Change these to different unique phrases!
 * You can generate these using the {@link https://api.wordpress.org/secret-key/1.1/salt/ WordPress.org secret-key service}
 * You can change these at any point in time to invalidate all existing cookies. This will force all users to have to log in again.
 *
 * @since 2.6.0
 */
define('AUTH_KEY',         'K.n8~i9je!=4@,8A_:M|?bq4CNc9eP:SofY>:Ju7{UJ,7[Z`Gy|@A1Q>cd TWrFB');
define('SECURE_AUTH_KEY',  '@HJD+21sm^GrVi*YSa~A5+vwL-,1^?XGd(~7.La%YYBE[!|Tcq`X2xnMHW|S|ngL');
define('LOGGED_IN_KEY',    '+<#lCzdG}75{0&4AK2- ZCrRv8a.gzl4rlB_G3M(poPBF,;[BsjShHF1Wh)+hN*-');
define('NONCE_KEY',        '&0YWEk}lA2B|BL(Fq<~]q.By{4W:*.V|n|9p`({()J64wy6k0Pa++{Ut-;^bcm#O');
define('AUTH_SALT',        '$%JI&@dnyQe9OWt3M$Qs3*A,2z}P0F/+ln!>#:UpHiR7G;K+`qEHivI|qm&b^c%H');
define('SECURE_AUTH_SALT', '3JV-XD3=j-{]FJ@+63M+7mN4k|iC?IJbMoNPGuEyg)_4Txt`X0z+-%1c;iHlQ0$+');
define('LOGGED_IN_SALT',   'e{p%]1`C}|(>w(m84n7S_(*X_:UJZ!-_ll7ktz*7m);A##}ZPm^-y=Q($H&4SI_Y');
define('NONCE_SALT',       ',$oVtuO/{Ny1{NTU~26q(MfXnK*p|T}PHg6Fb~0j<l)eV[!c?z9 ydoqlD(5B1mD');

/**#@-*/

/**
 * WordPress Database Table prefix.
 *
 * You can have multiple installations in one database if you give each a unique
 * prefix. Only numbers, letters, and underscores please!
 */
$table_prefix  = 'sfm_';

/**
 * WordPress Localized Language, defaults to English.
 *
 * Change this to localize WordPress. A corresponding MO file for the chosen
 * language must be installed to wp-content/languages. For example, install
 * de_DE.mo to wp-content/languages and set WPLANG to 'de_DE' to enable German
 * language support.
 */
define('WPLANG', '');

/**
 * For developers: WordPress debugging mode.
 *
 * Change this to true to enable the display of notices during development.
 * It is strongly recommended that plugin and theme developers use WP_DEBUG
 * in their development environments.
 */
define('WP_DEBUG', false);

/* That's all, stop editing! Happy blogging. */

/** Absolute path to the WordPress directory. */
if ( !defined('ABSPATH') )
	define('ABSPATH', dirname(__FILE__) . '/');

/** Sets up WordPress vars and included files. */
require_once(ABSPATH . 'wp-settings.php');
